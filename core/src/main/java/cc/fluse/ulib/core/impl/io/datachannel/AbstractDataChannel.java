package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.DataChannel;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.Channels;

/**
 * Abstract base class for {@link DataChannel}s.
 *
 * @implSpec Implementations must override {@link #isOpen()} and {@link #close()}.
 */
public abstract class AbstractDataChannel extends AbstractReadableDataChannel implements DataChannel {

    private final DataOutputStream dout;

    protected AbstractDataChannel() {
        this.dout = new DataOutputStream(Channels.newOutputStream(this));
    }

    @Override
    @MustBeInvokedByOverriders
    public void close() throws IOException {
        if (!bouncer.canPass()) return;
        super.close();
        dout.flush();
    }

    @Override
    public @NotNull DataChannel channel() throws IOException {
        bouncer.pass();
        return IOUtil.isolate(this);
    }

    @Override
    public @NotNull OutputStream streamWrite() throws IOException {
        bouncer.pass();
        return IOUtil.isolate(dout);
    }

    // delegate DataOutput


    @Override
    public void write(int b) throws IOException {
        bouncer.pass();
        dout.write(b);
    }

    @Override
    public void write(@NotNull byte[] b) throws IOException {
        bouncer.pass();
        dout.write(b);
    }

    @Override
    public void write(@NotNull byte[] b, int off, int len) throws IOException {
        bouncer.pass();
        dout.write(b, off, len);
    }

    @Override
    public void writeBoolean(boolean v) throws IOException {
        dout.writeBoolean(v);
    }

    @Override
    public void writeByte(int v) throws IOException {
        dout.writeByte(v);
    }

    @Override
    public void writeShort(int v) throws IOException {
        dout.writeShort(v);
    }

    @Override
    public void writeChar(int v) throws IOException {
        dout.writeChar(v);
    }

    @Override
    public void writeInt(int v) throws IOException {
        dout.writeInt(v);
    }

    @Override
    public void writeLong(long v) throws IOException {
        dout.writeLong(v);
    }

    @Override
    public void writeFloat(float v) throws IOException {
        dout.writeFloat(v);
    }

    @Override
    public void writeDouble(double v) throws IOException {
        dout.writeDouble(v);
    }

    @Override
    public void writeBytes(@NotNull String s) throws IOException {
        dout.writeBytes(s);
    }

    @Override
    public void writeChars(@NotNull String s) throws IOException {
        dout.writeChars(s);
    }

    @Override
    public void writeUTF(@NotNull String s) throws IOException {
        dout.writeUTF(s);
    }
}
