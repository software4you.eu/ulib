package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.io.datachannel.WritableDataChannelDelegate;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.VarDataSink;
import cc.fluse.ulib.core.io.Writable;
import org.jetbrains.annotations.NotNull;

import java.io.DataOutput;
import java.io.IOException;
import java.nio.channels.WritableByteChannel;

/**
 * A {@link WritableByteChannel} that also implements {@link DataOutput} and {@link Writable}.
 * <p>
 * Calls to write methods on this channel will be forwarded to the underlying channel, and calls open channel methods on
 * {@link Writable} will always return the same underlying channel.
 */
public interface WritableDataChannel extends WritableByteChannel, VarDataSink, Writable {

    /**
     * Wraps a {@link WritableByteChannel} in a {@link WritableDataChannel}. If the channel is already a
     * {@link WritableDataChannel}, it is returned as-is. Otherwise, a new {@link WritableDataChannel} is created that
     * delegates to the given channel.
     * <p>
     * The returned object is not guaranteed to be thread-safe.
     *
     * @param channel the channel to wrap
     * @return a {@link WritableDataChannel} that delegates to the given channel
     */
    static @NotNull WritableDataChannel wrap(@NotNull WritableByteChannel channel) {
        return channel instanceof WritableDataChannel dch ? dch : new WritableDataChannelDelegate(channel);
    }


    /**
     * Returns a new isolated channel that writes to this channel.
     *
     * @return the channel
     * @see IOUtil#isolate(WritableByteChannel)
     */
    @Override
    @NotNull
    default WritableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    @Override
    @NotNull
    default WritableDataChannel channelWrite() throws IOException {
        return channel();
    }
}
