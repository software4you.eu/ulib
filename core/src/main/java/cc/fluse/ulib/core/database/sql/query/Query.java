package cc.fluse.ulib.core.database.sql.query;

/**
 * A query that may be limited with additional conditions.
 */
public interface Query extends QueryStart, QueryEndpoint {
}
