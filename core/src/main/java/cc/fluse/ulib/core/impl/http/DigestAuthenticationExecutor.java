package cc.fluse.ulib.core.impl.http;

import cc.fluse.ulib.core.http.HttpRequestExecutor;
import cc.fluse.ulib.core.http.HttpUtil;
import cc.fluse.ulib.core.impl.Concurrent;
import cc.fluse.ulib.core.util.*;
import lombok.Synchronized;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.http.*;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static java.util.Objects.requireNonNull;

/**
 * An executor implementing HTTP Digest Access Authentication as defined by <a
 * href="https://www.rfc-editor.org/rfc/rfc7616">RFC 7616</a>.
 */
public class DigestAuthenticationExecutor implements HttpRequestExecutor {

    private final SecureRandom rand = new SecureRandom();
    private final HttpClient client;
    private final String username;
    private final String password;

    private int requestCounter;
    private ServerAuthData serverAuth;

    /**
     * @param client   the http client that should be used
     * @param username the username
     * @param password the password
     */
    public DigestAuthenticationExecutor(@NotNull HttpClient client, @NotNull String username, @NotNull String password) {
        this.client = client;
        this.username = username;
        this.password = password;
    }

    public <T> @NotNull CompletableFuture<HttpResponse<T>> sendAsync(@NotNull HttpRequest request, HttpResponse.@NotNull BodyHandler<T> responseHandler) {
        return Concurrent.run(() -> handle(request, responseHandler));
    }

    @Synchronized
    private <T> HttpResponse<T> handle(@NotNull HttpRequest request, HttpResponse.@NotNull BodyHandler<T> responseHandler) {
        try {
            // do initial request
            var response = this.client.send(requestCounter > 0 ? withDigestHeader(request) : request, responseHandler);

            // check if new request should be made
            if (!checkAuthResult(response)) {
                return response;
            }

            // send new request
            return client.send(withDigestHeader(request), responseHandler);
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks if a http response requests a new digest authentication. Saves server auth data if applicable.
     *
     * @param response the server response to process
     * @return {@code true} if a new digest authentication is necessary, {@code false} if the response does not request
     * another authentication
     */
    private boolean checkAuthResult(@NotNull HttpResponse<?> response) {
        Collection<Map<String, String>> authList;
        if (response.statusCode() != 401
            || (authList = parseHeaderMap(response.headers(), "WWW-authenticate", "Digest ")) == null
            || authList.isEmpty())
        // no digest auth necessary
        {
            return false;
        }

        // server requested new digest authentication
        var auth = authList.iterator().next();

        // put new auth data
        var algo = Optional.ofNullable(auth.get("algorithm")).orElse("MD5");
        var digestAlgo = algo.endsWith("-sess") ? algo.substring(algo.length() - 5) : algo;
        var digest = Expect.compute(MessageDigest::getInstance, digestAlgo)
                           .orElseRethrowRE();
        var realm = requireNonNull(auth.get("realm"), "realm");
        var nonce = requireNonNull(auth.get("nonce"), "nonce");
        var qop = requireNonNull(auth.get("qop"), "qop");
        var opaque = auth.get("opaque");

        this.serverAuth = new ServerAuthData(algo, digest, realm, nonce, qop, opaque);

        // notify new auth data
        return true;
    }

    /**
     * Copies the http request and adds the authorization header
     *
     * @param request the request to copy
     * @return the newly built request
     */
    private HttpRequest withDigestHeader(HttpRequest request) {

        var builder = HttpRequest.newBuilder()
                                 .uri(request.uri())
                                 .expectContinue(request.expectContinue());

        // set headers
        request.headers().map().forEach((key, values) -> values.forEach(value -> builder.header(key, value)));

        // set timeout
        request.timeout().ifPresent(builder::timeout);

        // set method
        request.bodyPublisher().ifPresentOrElse(bp -> {
            builder.method(request.method(), bp);
        }, () -> {
            switch (request.method()) {
                case "GET" -> builder.GET();
                case "DELETE" -> builder.DELETE();
            }
        });

        // add digest header
        builder.header("Authorization", buildDigestHeader(request));

        return builder.build();
    }

    /**
     * Builds a proper digest authorization header based on a certain http request and the last provided server auth
     * data.
     *
     * @param request the request to generate the header data for
     * @return the header
     */
    private String buildDigestHeader(HttpRequest request) {
        if (Objects.isNull(serverAuth)) {
            throw new IllegalStateException();
        }

        String cnonce = generateClientNonce();

        // hash(username:realm:password)
        String hash1 = serverAuth.hash("%s:%s:%s".formatted(username, serverAuth.realm, password));
        if (serverAuth.algorithm.endsWith("-sess")) {
            // hash(hash(username:realm:password):nonce:cnonce)
            hash1 = serverAuth.hash(hash1 + ":%s:%s".formatted(serverAuth.nonce, cnonce));
        }

        String hash2 = switch (serverAuth.qop) {
            // hash(method:digestURI)
            case "", "auth", "auth,auth-int" ->
                    serverAuth.hash("%s:%s".formatted(request.method(), request.uri().getPath()));

            // hash(method:digestURI:hash(entityBody))
            case "auth-int" -> serverAuth.hash("%s:%s:%s".formatted(request.method(), request.uri().getPath(),
                                                                    serverAuth.hash(new String(HttpUtil.obtainRequestBody(request).orElseThrow()))));
            default -> throw new IllegalStateException();
        };

        String response = switch (serverAuth.qop) {
            case "auth", "auth-int", "auth,auth-int" -> serverAuth.hash("%s:%s:%08x:%s:%s:%s".formatted(
                    hash1, serverAuth.nonce, requestCounter, cnonce, serverAuth.qop, hash2
            ));
            default -> serverAuth.hash("%s:%s:%s".formatted(hash1, serverAuth.nonce, hash2));
        };


        var joiner = new StringJoiner(",", "Digest ", "")
                .add("username=\"%s\"".formatted(username))
                .add("nonce=\"%s\"".formatted(serverAuth.nonce))
                .add("uri=\"%s\"".formatted(request.uri().getPath()))
                .add((serverAuth.qop.contains(",") ? "qop=\"%s\"" : "qop=%s").formatted(serverAuth.qop))
                .add("nc=%08x".formatted(requestCounter))
                .add("cnonce=\"%s\"".formatted(cnonce))
                .add("response=\"%s\"".formatted(response));

        return (serverAuth.opaque == null ? joiner : joiner
                .add("opaque=\"%s\"".formatted(serverAuth.opaque))
        ).toString();
    }

    /**
     * Securely generates a client nonce and increments the request counter
     *
     * @return the newly generated nonce
     */
    private String generateClientNonce() {
        requestCounter++;
        byte[] bytes = new byte[4];
        rand.nextBytes(bytes);
        return Conversions.toHex(bytes, false);
    }


    // - helpers -


    public static Collection<Map<String, String>> parseHeaderMap(HttpHeaders headers, String key, String cutOff) {
        return headers.map().entrySet().stream()
                      .filter(en -> en.getKey().equalsIgnoreCase(key))
                      .findFirst()
                      .map(Map.Entry::getValue)
                      .map(li -> li.stream()
                                   .map(str -> str.startsWith(cutOff) ? str.substring(cutOff.length()) : null)
                                   .filter(Objects::nonNull)
                                   .map(str -> str.replaceAll("[\\r\\n]", ""))
                                   .map(str -> {
                                       try {
                                           return parseSimpleKeyValueMap(str);
                                       } catch (IOException e) {
                                           throw new RuntimeException(e);
                                       }
                                   })
                                   .toList()
                      )
                      .orElse(null);
    }

    private static Map<String, String> parseSimpleKeyValueMap(String str) throws IOException {
        Map<String, String> map = new HashMap<>();

        for (int li = 0, i; (i = str.indexOf('=', li)) != -1; li = i) {
            String key = str.substring(li, i);


            boolean wrapped = str.charAt(i + 1) == '"';

            // value begins, search for end
            var end = str.indexOf(wrapped ? "\"," : ",", i + 2);
            if (end == -1) {
                end = str.length();
            }

            // obtain value
            String value = str.substring(i + (wrapped ? 2 : 1), end);

            // shift index to end plus 1 or 2 (because of '",')
            i = end + (wrapped ? 2 : 1);


            map.put(key, value);
        }

        return map;
    }

    /**
     * Represents the last server authentication request data.
     */
    private record ServerAuthData(@NotNull String algorithm,
                                  @NotNull MessageDigest digest,
                                  @NotNull String realm,
                                  @NotNull String nonce,
                                  @NotNull String qop,
                                  @Nullable String opaque) {
        private String hash(String input) {
            digest.reset();
            return Expect.compute(() -> HashUtil.computeHex(new ByteArrayInputStream(input.getBytes()), digest))
                         .orElseThrow();
        }
    }

    @Override
    public String toString() {
        return "DigestAuthenticationExecutor{" +
               "client=" + client +
               '}';
    }
}
