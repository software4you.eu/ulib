package cc.fluse.ulib.core.impl;

import cc.fluse.ulib.core.configuration.ConfigFile;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.impl.init.Init;
import cc.fluse.ulib.core.impl.inject.AccessibleObjectTransformer;
import cc.fluse.ulib.core.impl.inject.InjectionManager;
import cc.fluse.ulib.core.impl.inject.PropertiesLock;
import cc.fluse.ulib.core.impl.patch.Patch;
import cc.fluse.ulib.core.impl.reflect.ReflectSupport;
import cc.fluse.ulib.core.reflect.Param;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Conversions;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.Synchronized;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static cc.fluse.ulib.core.file.FSUtil.mkDirs;

public final class Internal {

    private static final boolean DEBUG = false;

    // - config -
    @Getter
    private static final Path dataDir, cacheDir, localMavenDir;
    @Getter
    private static final boolean unsafeOperations, forceSync, allowMavenChecksumMismatch, allowDirectBuffers;
    @Getter
    private static final int defaultBufferCapacity;
    @Getter
    private static final Collection<Patch> disabledPatches;

    // - misc -
    private static final Set<Thread> sudoThreads = ConcurrentHashMap.newKeySet();
    @Getter
    private static Instrumentation instrumentation;

    static {
        try {
            mkDirs(dataDir = Path.of(System.getProperty("ulib.directory.main", ".ulib")));

            final var config = ConfigFile.versionedYaml(dataDir.resolve("config.yml"),
                    Objects.requireNonNull(Internal.class.getResource("/META-INF/coreconfig.yml"), "Bundled configuration not found!"),
                    KeyPath.of("config-version"));

            final var preferProp = !config.bool("override-command-line").orElse(false);


            mkDirs(cacheDir = Path.of(config.propGet("directories.cache", "ulib.directory.cache", preferProp)
                    .orElseGet(() -> String.format("%s%scache", dataDir, File.separator))));
            mkDirs(localMavenDir = Path.of(config.propGet("directories.libraries", "ulib.directory.libraries", preferProp)
                    .orElseGet(() -> String.format("%s%slibraries", dataDir, File.separator))));

            // collect disabled patches
            disabledPatches = (preferProp && !System.getProperty("ulib.disabled-patches", "").isBlank()
                    ? Arrays.stream(System.getProperty("ulib.disabled-patches").strip().split(","))
                    : config.stringList("disabled-patches").stream().flatMap(List::stream)
            ).map(str -> {
                        try {
                            return Patch.valueOf(str.toUpperCase());
                        } catch (IllegalArgumentException e) {
                            System.err.println("(ulib) Cannot parse disabled patch: " + str);
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toUnmodifiableSet());

            forceSync = DEBUG || config.propGet("force-synchronous-work", "ulib.forcesync", preferProp, Conversions::tryBoolean).orElse(false);
            unsafeOperations = config.propGet("unsafe-operations", "ulib.unsafe_operations", preferProp).orElse("deny").equalsIgnoreCase("allow");
            defaultBufferCapacity = config.propGet("buffers.default-capacity", "ulib.buffers.defaultcapacity", preferProp, Conversions::tryInt).orElse(1024);
            allowMavenChecksumMismatch = config.propGet("maven-resolver.allow-checksum-mismatch", "ulib.maven-resolver.allow-checksum-mismatch", preferProp, Conversions::tryBoolean).orElse(false);
            allowDirectBuffers = !DEBUG && !config.propGet("buffers.allow-direct", "ulib.buffers.allowdirect", preferProp).orElse("true").equals("false"); // not using Conversions::tryBoolean because we want to anything but "false" to be true
        } catch (IOException e) {
            throw new RuntimeException("Initialization failed", e);
        }
    }


    // - Init methods -

    /**
     * Init point of the library. Invoked by the loader.
     */
    @Synchronized
    public static void agentInit(Instrumentation instrumentation) {
        if (ReflectUtil.getCallerClass() != Init.class) {
            throw new Error();
        }
        if (Internal.instrumentation != null) {
            throw new IllegalStateException();
        }
        Internal.instrumentation = Objects.requireNonNull(instrumentation);

        AccessibleObjectTransformer.acquirePrivileges();
        ReflectUtil.doPrivileged(Internal::widenModuleAccess);
        PropertiesLock.lockSystemProperties(AccessibleObjectTransformer.SUDO_KEY, InjectionManager.HOOKING_KEY, InjectionManager.PROXY_KEY);

        // apply patches that are not disabled
        Patch.apply(p -> !disabledPatches.contains(p));
    }

    @SneakyThrows
    private static void widenModuleAccess() {
        var method = Module.class.getDeclaredMethod("implAddReadsAllUnnamed");
        ReflectSupport.accessible(method);

        var modules = AccessibleObjectTransformer.class.getModule().getLayer().modules().stream()
                                                       .filter(m -> m.getName().startsWith("ulib."))
                                                       .toArray(Module[]::new);

        for (Module module : modules) {
            method.invoke(module);
        }
    }

    // - object creation -

    public static ByteBuffer bufalloc() {
        return bufalloc(defaultBufferCapacity);
    }

    public static ByteBuffer bufalloc(int capacity) {
        return allowDirectBuffers ? ByteBuffer.allocateDirect(capacity) : ByteBuffer.allocate(capacity);
    }

    // - utility methods for the library -

    public static boolean isUlibClass(Class<?> cl) {
        Module m;
        if (!(m = cl.getModule()).isNamed()) {
            return false; // ulib classes are all in named modules
        }

        // check if the module is a genuine ulib module
        if (!m.getName().startsWith("ulib.")
            || m != Internal.class.getModule().getLayer().findModule(m.getName()).orElse(null)) {
            return false;
        }

        // name check should be unnecessary, buh eh
        return cl.getName().startsWith("cc.fluse.ulib.");
    }

    /**
     * <b>WARNING</b> This method does not check if the class itself is a ulib class.
     * <p>
     * A class is considered an implementation class if it is not opened or exported.
     */
    public static boolean isImplClass(Class<?> cl) {
        var m = cl.getModule();
        var pack = cl.getPackageName();
        return !m.isExported(pack) && !m.isOpen(pack);
    }

    // sudo

    public static boolean isSudoThread() {
        return sudoThreads.contains(Thread.currentThread());
    }

    public static <T, X extends Exception> T sudo(Func<T, X> task) throws X {
        if (ReflectUtil.getCallerClass() != ReflectUtil.class) throw new Error();
        if (isSudoThread()) return task.execute();

        final var thr = Thread.currentThread();
        sudoThreads.add(thr);
        try {
            return task.execute();
        } finally {
            sudoThreads.remove(thr);
        }
    }

    public static void makeAccessibleTo(Module other) {
        var noAccessYet = Internal.class.getModule().getLayer().modules().stream()
                                        // grab ulib modules
                                        .filter(m -> m.getName().startsWith("ulib."))
                                        // filter out the modules `other` can already read
                                        .filter(api -> !other.canRead(api))
                                        .toList();
        if (noAccessYet.isEmpty()) {
            return;
        }

        try {
            ReflectUtil.doPrivileged(() -> {
                for (Module api : noAccessYet) {
                    ReflectUtil.icall(other, "implAddReads()", Param.single(Module.class, api));
                }
            });
        } catch (ReflectiveOperationException e) {
            throw new UndefinedStateError(e);
        }
    }
}
