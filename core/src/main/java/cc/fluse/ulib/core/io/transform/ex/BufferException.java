package cc.fluse.ulib.core.io.transform.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.experimental.StandardException;

import java.io.IOException;

/**
 * An exception indicating something's wrong with the internal buffer of a certain transform.
 */
@StandardException
@BypassAnnotationEnforcement
public final class BufferException extends IOException {
}
