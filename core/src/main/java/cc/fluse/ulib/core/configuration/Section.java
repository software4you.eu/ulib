package cc.fluse.ulib.core.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;

/**
 * Representation of a section in a map structured document.
 */
public interface Section {
    /**
     * Returns the root of this section.
     * <p>
     * The root is the highest available parent section.
     *
     * @return the root
     */
    @NotNull
    Section getRoot();

    /**
     * Determines whether this section is the root.
     *
     * @return {@code true}, if this section is the root
     */
    boolean isRoot();

    /**
     * Harmonizes this section to the given one.
     * <p>
     * Each value present in the base be set to the if it's not present in this section. The base will not be modified.
     * <p>
     * You might want to purge the section after a strict convergence.
     *
     * @param base   the section to converge/harmonize to
     * @param strict if values not present in the base should be removed correspondingly
     * @param deep   if the converging should also be applied recursively to subsections
     * @see #purge(boolean)
     */
    void converge(@NotNull Section base, boolean strict, boolean deep);

    /**
     * Removes empty all subsections.
     *
     * @param deep if subsections should be purged as well (recursively)
     */
    void purge(boolean deep);

    /**
     * Searches the configuration for a specific key and obtains its value.
     *
     * @param path the key path
     * @return an optional wrapping the value, or empty optional if {@code path} not found or the value couldn't get
     * converted
     */
    @NotNull Optional<Object> get(@NotNull KeyPath path);

    /**
     * Reads a value and attempts to cast it to the given type.
     *
     * @param clazz the type class
     * @param path  the key path
     * @param <T>   the type
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @throws NumberFormatException    if {@code <T>} is a number type but the read value cannot be parsed to it
     * @throws IllegalArgumentException if read value is not of type {@code <T>}
     * @see String#format(String, Object...)
     */
    @NotNull <T> Optional<T> get(@NotNull Class<T> clazz, @NotNull KeyPath path);

    /**
     * Collects all keys holding a non-subsection value from this section.
     *
     * @param deep if keys from lower sections should be collected as well (flattened)
     * @return a collection with all the keys
     */
    @NotNull
    Collection<KeyPath> getKeys(boolean deep);

    /**
     * Collects all non-subsection values from this section.
     *
     * @param deep if values from lower sections should be collected as well (flattened)
     * @return a key-value map
     *
     * @see #graphUnpack(Function)
     * @see #getKeys(boolean)
     */
    @NotNull
    Map<KeyPath, Object> getValues(boolean deep);

    /**
     * Collects all this sections contents into a map recursively. The map presents an immutable view of the current
     * state of this section. However, changes in the section will <b>not</b> be visible in the map.
     * <p>
     * Objects will not get automatically serialized, it can be done with a mapper.
     *
     * @param mapper a function that may map certain elements (i.e. unpacking them); mapping them to {@code null} will
     *               cause the elements not to be present in the resulting map
     * @return the map
     *
     * @see #getValues(boolean)
     */
    @NotNull
    Map<String, Object> graphUnpack(@Nullable Function<@NotNull Object, @Nullable Object> mapper);

    /**
     * Determines if this section is empty, meaning is does not hold any values or other sections.
     *
     * @return {@code true} if this section is empty, {@code false otherwise}
     */
    boolean isEmpty();

    /**
     * Sets a key to a specific value in the configuration.
     * <p>
     * A {@code null} value removes the key from the configuration.
     * <p>
     * Any previous associated value will be overwritten.
     *
     * @param path  the key path
     * @param value the value
     * @throws IllegalStateException if the section cannot hold keyed values. This is the case if the section was
     *                               directly loaded with not-keyed data.
     */
    void set(@NotNull KeyPath path, @Nullable Object value);

    /**
     * Determines whether a certain key has a value assigned to it (cannot be a section).
     *
     * @param path the key path
     * @return {@code true}, if the {@code path} holds a value
     */
    boolean isSet(@NotNull KeyPath path);

    /**
     * Determines whether a certain key has a value assigned to it (can be a section).
     *
     * @param path the key path
     * @return {@code true}, if the {@code path} is known
     */
    boolean contains(@NotNull KeyPath path);

    /**
     * Searches this section for a subsection with a specific key.
     *
     * @param path the key path
     * @return an optional wrapping the sub, or an empty optional if {@code path} not found
     */
    @NotNull
    Optional<? extends Section> getSubsection(@NotNull KeyPath path);

    /**
     * Creates a new subsection at the given key.
     * <p>
     * Any previous associated value will be overwritten.
     *
     * @param path the key path
     * @return the newly created section
     */
    @NotNull
    Section createSubsection(@NotNull KeyPath path);

    /**
     * Collects all subsections of this section.
     *
     * @param deep if the subsections should also get collected (recursively)
     * @return a collection containing all subsections.
     */
    @NotNull
    Collection<? extends Section> getSubsections(boolean deep);

    /**
     * Determines whether a certain key holds a subsection.
     * <p>
     * This method will also return {@code false} if the {@code path} is not set or if it holds another value than a
     * section.
     *
     * @param path the key path
     * @return {@code true}, if the {@code path} holds a subsection
     */
    boolean isSubsection(@NotNull KeyPath path);

    /**
     * Gets a section from the specified path, creating it when necessary.
     * <p>
     * When creating the new section, any previous associated value will be overwritten.
     *
     * @param path the key path
     * @return the subsection
     */
    @NotNull
    Section subAndCreate(@NotNull KeyPath path);


    /**
     * Clears all data from this section.
     */
    void clear();
}
