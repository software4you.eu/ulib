package cc.fluse.ulib.core.impl.reflect;

import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ClsRef implements cc.fluse.ulib.core.reflect.ref.ClsRef {

    private static final String CNP = "[^\\n\\s.,:;!\"`´'§%&\\/()=?{\\[\\]}\\\\+*~’\\-\\|<>^°]";
    private static final String CP = "^" + CNP + "+(\\/" + CNP + "+)*$";

    @NotNull
    public static String getInternalName(@NotNull Class<?> clazz) {
        return clazz.getName().replace('.', '/');

    }

    private final String name, internalName;
    private volatile Class<?> clazz;

    /**
     * @param internalName jvm class name, e.g. "java/lang/String"
     */
    public ClsRef(@NotNull String internalName) {
        if (!internalName.matches(CP)) throw new IllegalArgumentException("not a valid jvm class name");
        this.internalName = internalName;
        this.name = internalName.replace("/", ".");
        this.clazz = null;
    }

    public ClsRef(@NotNull Class<?> clazz) {
        this.internalName = getInternalName(clazz);
        this.name = clazz.getName();
        this.clazz = clazz;
    }

    @Override
    public @NotNull String getName() {
        return name;
    }

    public String getInternalName() {
        return internalName;
    }

    @Override
    @NotNull
    public synchronized Expect<Class<?>, ClassNotFoundException> tryLoad() {
        if (clazz != null) return Expect.of(clazz);
        var e = cc.fluse.ulib.core.reflect.ref.ClsRef.super.tryLoad();
        if (e.isPresent()) this.clazz = e.getValue();
        return e;
    }

    @Nullable
    public Class<?> cls() {
        return clazz;
    }

    @Override
    public int hashCode() {
        return internalName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.getClass() == getClass() && ((ClsRef) obj).internalName.equals(internalName);
    }
}
