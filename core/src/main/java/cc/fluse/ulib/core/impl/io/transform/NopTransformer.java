package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.io.transform.AbstractTransformer;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;

public final class NopTransformer extends AbstractTransformer {

    @Override
    protected void implWrite(@NotNull ByteBuffer src) throws InputFormatException {
        super.writeResult(src);
    }

    @Override
    protected void implComplete() throws IOTransformException {
        // nop
    }

    @Override
    protected void implClose() {
        // nop
    }
}
