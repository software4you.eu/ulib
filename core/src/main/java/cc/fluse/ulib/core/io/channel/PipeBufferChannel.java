package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.ReadWrite;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.Objects;

import static cc.fluse.ulib.core.util.Semantics.nofail;

/**
 * An output stream backed by a byte buffer. The internal buffer is dynamically enlarged, if necessary (unlike
 * {@link RingBufferChannel}). This class acts like a pipeline, where the written bytes are buffered in the internal
 * buffer, and can be read from the internal buffer. Read bytes by {@link #read(ByteBuffer)} are discarded from the
 * internal buffer.
 *
 * @see ByteBufferReadChannel
 */
public class PipeBufferChannel extends OutputStream implements DataChannel, ReadWrite {

    private final ChBouncer bouncer = Bouncer.ch();
    private volatile ByteBuffer buf;

    public PipeBufferChannel(@Nullable ByteBuffer src, int initialCapacity) {
        if (initialCapacity <= 0) {
            throw new IllegalArgumentException("Capacity must be greater than 0 (%d)".formatted(initialCapacity));
        }
        this.buf = Internal.bufalloc(initialCapacity);

        if (src == null) return;
        // fill initial bytes
        nofail(ClosedChannelException.class, () -> write(src));
    }

    public PipeBufferChannel(int initialCapacity) {
        this(null, initialCapacity);
    }

    public PipeBufferChannel(@Nullable ByteBuffer src) {
        this(src, Internal.getDefaultBufferCapacity());
    }

    public PipeBufferChannel() {
        this(null);
    }

    /**
     * Ensures new bytes can be written.
     *
     * @param len the amount of bytes
     */
    private synchronized void ensure(int len) throws ClosedChannelException {
        bouncer.pass();
        if (buf.remaining() >= len) return;

        // enlarge
        var times = len > buf.capacity() ? (int) Math.ceil(len / (double) buf.capacity()) : 1;
        this.buf = Internal.bufalloc(buf.capacity() + buf.capacity() * times) // more cap
                           .put(buf.flip()); // copy
    }

    /**
     * Clears the internal buffer. Any previous directly obtained buffer object(s) will transition to undefined
     * behavior.
     */
    public synchronized void clear() throws ClosedChannelException {
        bouncer.pass();
        buf.clear();
    }


    /**
     * Deletes the first {@code n} bytes stored in the internal buffer and shifts any remaining bytes forward to the
     * head. Any previous directly obtained buffer object(s) will transition to undefined behavior.
     *
     * @param n the amount of bytes to purge from the buffer's head
     */
    public synchronized void purge(int n) throws ClosedChannelException {
        bouncer.pass();
        if (n < 0) throw new IndexOutOfBoundsException(n);
        if (n == 0 || n > buf.position()) return;

        var sl = buf.slice(n, buf.position() - n);
        buf.position(0);
        buf.put(sl);
    }

    public synchronized boolean isOpen() {
        return bouncer.canPass();
    }

    /**
     * Closes this channel and releases any system resources associated with it. If the channel is already closed then
     * invoking this method has no effect.
     */
    @Override
    public synchronized void close() {
        if (!isOpen()) return;
        bouncer.block();
        buf.clear();
        buf = null;
    }

    /**
     * Obtains the written content directly.
     *
     * @return a read-only buffer with the previously written content
     * @implNote The returned buffer will have an <b>undefined behavior</b>, after {@link #clear()} or
     * {@link #purge(int)} is called.
     */
    @NotNull
    public synchronized ByteBuffer obtain() throws ClosedChannelException {
        bouncer.pass();
        return buf.asReadOnlyBuffer().flip();
    }

    /**
     * Copies and obtains the written content.
     *
     * @return a read-only buffer with the previously written content
     */
    @NotNull
    public synchronized ByteBuffer obtainCopy() throws ClosedChannelException {
        bouncer.pass();
        var copy = ByteBuffer.allocate(buf.position());
        copy.put(obtain());
        return copy.asReadOnlyBuffer();
    }

    /**
     * Writes bytes into a destination buffer until no more bytes can be written, either because the destination buffer
     * has no more free space or no more bytes are available.
     * <p>
     * Afterwards this object is {@link #purge(int) purged}.
     *
     * @param dst the destination buffer
     * @return the amount of bytes written into the destination buffer
     */
    @Override
    public synchronized int read(@NotNull ByteBuffer dst) throws ClosedChannelException {
        if (!dst.hasRemaining()) return 0;
        var src = obtain();
        if (!src.hasRemaining()) return -1;

        var slice = src.slice(0, Math.min(dst.remaining(), src.remaining()));
        dst.put(slice);
        int written = slice.position();
        purge(written);
        return written;
    }

    /**
     * @return {@code true} if the internal buffer holds any data, {@code false} otherwise
     */
    public synchronized boolean holds() throws ClosedChannelException {
        bouncer.pass();
        return buf.position() > 0;
    }

    @Override
    public synchronized int write(@NotNull ByteBuffer src) throws ClosedChannelException {
        ensure(src.remaining());
        var pos = buf.position();
        buf.put(src);
        return buf.position() - pos;
    }

    @Override
    public synchronized void write(byte @NotNull [] b) throws ClosedChannelException {
        ensure(b.length);
        buf.put(b);
    }

    @Override
    public synchronized void write(byte @NotNull [] b, int off, int len) throws ClosedChannelException {
        Objects.checkFromIndexSize(off, len, b.length);
        ensure(len);
        buf.put(b, off, len);
    }

    @Override
    public synchronized void write(int b) throws ClosedChannelException {
        ensure(1);
        buf.put((byte) b);
    }

    @Override
    public @NotNull DataChannel channel() throws ClosedChannelException {
        bouncer.pass();
        return IOUtil.isolate((DataChannel) this);
    }
}
