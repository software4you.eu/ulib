package cc.fluse.ulib.core.impl.configuration;

import cc.fluse.ulib.core.configuration.*;
import cc.fluse.ulib.core.function.BiParamTask;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.ReadWrite;
import cc.fluse.ulib.core.io.Readable;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;

public class FileBackedConfiguration extends DelegationConf implements ConfigFile {

    /**
     * @see ConfigFile#of(Path, URL, KeyPath, ParamFunc)
     */
    public static <T extends ReloadableConfiguration & DumpableConfiguration, X extends Exception, X2 extends Exception, X3 extends Exception>
    FileBackedConfiguration versioned(
        ReadWrite resource, Readable bundled,
        ParamFunc<? super @NotNull T, @NotNull Integer, @NotNull X> versionExtractor,
        BiParamTask<? super T, ? super Integer, X3> versionSetter,
        ParamFunc<? super @NotNull Reader, @NotNull T, @NotNull X2> initializer
    ) throws X, X2, X3, IOException {

        // try to load the configuration from the file
        ReadableByteChannel ch;
        try {
            ch = resource.channelRead();
        } catch (IOException e) {
            // file cannot be read, so we create it
            try (var src = bundled.channelRead(); var dst = resource.channelWrite()) {
                IOUtil.transfer(src, dst);
            }
            return new FileBackedConfiguration(resource, initializer);
        }

        // file exists, so we load it
        // IOUtil#op will close the channel
        var config = IOUtil.op(() -> Channels.newReader(ch, Charset.defaultCharset()), initializer, true);
        var bundledConfig = IOUtil.op(() -> Channels.newReader(bundled.channelRead(), Charset.defaultCharset()), initializer, true);

        // check if upgrade is necessary
        int bundledVersion;
        if ((bundledVersion = versionExtractor.execute(bundledConfig)) <= versionExtractor.execute(config)) {
            // bundled version is older or equal to the loaded version, so we can use the loaded version
            return new FileBackedConfiguration(resource, config);
        }

        // bundled version is newer, so we upgrade
        versionSetter.execute(config, bundledVersion);
        config.converge(bundledConfig, true, true);
        config.purge(true);

        // save the upgraded configuration
        var cf = new FileBackedConfiguration(resource, config);
        cf.save();
        return cf;
    }


    private final ReadWrite resource;

    public <T extends ReloadableConfiguration & DumpableConfiguration, X extends Exception>
    FileBackedConfiguration(@NotNull ReadWrite resource, @NotNull ParamFunc<? super @NotNull Reader, @NotNull T, X> initializer) throws X, IOException {
        super(IOUtil.op(() -> Channels.newReader(resource.channelRead(), Charset.defaultCharset()), initializer, true));
        this.resource = resource;
    }

    public <T extends ReloadableConfiguration & DumpableConfiguration> FileBackedConfiguration(@NotNull ReadWrite resource, T config) {
        super(config);
        this.resource = resource;
    }

    @Override
    public @NotNull Configuration getBacking() {
        return config;
    }

    @Override
    public @NotNull ReadWrite getBackingResource() {
        return resource;
    }

    public void reload() throws IOException {
        try (var ch = resource.channelRead()) {
            ((ReloadableConfiguration) config).reload(ch);
        }
    }

    public void save() throws IOException {
        try (var ch = resource.channelWrite()) {
            ((DumpableConfiguration) config).dump(ch);
        }
    }

}
