package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.SeekableDataChannel;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.channels.SeekableByteChannel;

public class SeekableDataChannelDelegate extends DataChannelDelegate implements SeekableDataChannel {
    private final SeekableByteChannel channel;

    /**
     * Creates a new {@link SeekableDataChannel} that reads from the given {@link SeekableByteChannel}.
     *
     * @param channel The channel to read from.
     */
    public SeekableDataChannelDelegate(@NotNull SeekableByteChannel channel) {
        super(channel);
        this.channel = channel;
    }

    @Override
    public @NotNull SeekableDataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    // delegate SeekableByteChannel

    @Override
    public long position() throws IOException {
        return channel.position();
    }

    @Override
    public @NotNull SeekableDataChannel position(long newPosition) throws IOException {
        channel.position(newPosition);
        return this;
    }

    @Override
    public long size() throws IOException {
        return channel.size();
    }

    @Override
    public @NotNull SeekableDataChannel truncate(long size) throws IOException {
        channel.truncate(size);
        return this;
    }
}
