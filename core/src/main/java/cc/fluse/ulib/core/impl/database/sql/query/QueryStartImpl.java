package cc.fluse.ulib.core.impl.database.sql.query;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.database.sql.query.QueryStart;
import cc.fluse.ulib.core.database.sql.query.Where;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import org.jetbrains.annotations.NotNull;

public final class QueryStartImpl implements QueryStart {
    private final Metadata meta;

    public QueryStartImpl(AbstractSqlDatabase sql, Table table, String operand) {
        this.meta = new Metadata(sql, new StringBuilder(String.format("%s %s", operand, sql.quote(table.getName(), true))));
    }

    @Override
    public @NotNull ConditionImpl<Where> where(@NotNull Column<?> column) {
        return where(column.getName());
    }

    @Override
    public @NotNull ConditionImpl<Where> where(@NotNull String column) {
        return new ConditionImpl<>(meta, column, WhereImpl::new);
    }

    @Override
    public @NotNull WhereImpl whereRaw(@NotNull String condition) {
        return new WhereImpl(meta, condition);
    }
}
