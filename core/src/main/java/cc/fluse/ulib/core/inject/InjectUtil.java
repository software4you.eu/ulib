package cc.fluse.ulib.core.inject;

import cc.fluse.ulib.core.impl.inject.ClassLoaderDelegationHook;
import cc.fluse.ulib.core.impl.inject.InjectionManager;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.LazyValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Unmodifiable;

import java.lang.instrument.UnmodifiableClassException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

/**
 * Hook injection utility.
 *
 * @see HookInjection
 */
public final class InjectUtil {
    private static final LazyValue<Spec> DEFAULT_SPEC = LazyValue.immutable(() -> createHookingSpec(null, null, (Integer[]) null));

    @NotNull
    public static Spec defaultHookingSpec() {
        return DEFAULT_SPEC.getIfAvailable().orElseThrow();
    }

    @NotNull
    public static Spec createHookingSpec(@Nullable HookPoint point, @Nullable String target, @Nullable Integer... n) {
        Map<String, Object> vals = new HashMap<>(3, 1f);

        if (point != null)
            vals.put("point", point);

        if (target != null)
            vals.put("target", target);

        if (n != null && n.length > 0) {
            // convert into primitive array
            int[] array = new int[n.length];
            for (int i = 0; i < n.length; i++) {
                var $n = n[i];
                if ($n != null)
                    array[i] = $n;
            }
            vals.put("n", array);
        }

        return ReflectUtil.instantiateAnnotation(Spec.class, vals);
    }

    @NotNull
    public static Spec createHookingSpec(@Nullable HookPoint point) {
        return createHookingSpec(point, null, (Integer[]) null);
    }

    /**
     * Performs all pending injection configurations.
     * <b>Note</b>: Injection configurations for classes not yet loaded will fail.
     *
     * @return pairs of the jvm target class representations and the respective transform results
     */
    @Unmodifiable
    @NotNull
    public static Map<String, Optional<? extends Exception>> performPendingInjections() {
        return performPendingInjections((jvmCN, cl) -> true);
    }

    /**
     * Performs pending injection configurations, whose reference passes the predicate.
     * <b>Note</b>: Injection configurations for classes not yet loaded will fail.
     *
     * @param filter the filter to determine which injection configurations to perform, will be called with the JVM
     *               Class name and (if present) the class instance itself
     * @return pairs of the jvm target class representations and the respective transform results
     */
    @Unmodifiable
    @NotNull
    public static Map<String, Optional<? extends Exception>> performPendingInjections(@NotNull BiPredicate<@NotNull String, @Nullable Class<?>> filter) {
        try {
            return InjectionManager.getInstance().transformPending(ref -> filter.test(ref.getInternalName(), ref.cls()));
        } catch (UnmodifiableClassException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Obtains all pending injection configuration references and their respective future objects
     * (which indicate when the injection is completed).
     *
     * @return pairs of the jvm target class representations and the respective future object
     */
    @Unmodifiable
    @NotNull
    public static Map<String, CompletableFuture<Void>> getPendingInjections() {
        return getPendingInjections((jvmCN, cl) -> true);
    }

    /**
     * Obtains the pending injection configuration references and their respective future objects
     * (which indicate when the injection is completed), whose references passes the predicate.
     *
     * @param filter the filter to determine which injection configurations to obtain,
     *               will be called with the JVM Class name and (if present) the class instance itself
     * @return pairs of the jvm target class representations and the respective future object
     */
    @Unmodifiable
    @NotNull
    public static Map<String, CompletableFuture<Void>> getPendingInjections(@NotNull BiPredicate<@NotNull String, @Nullable Class<?>> filter) {
        return InjectionManager.getInstance().getPending(ref -> filter.test(ref.getInternalName(), ref.cls()));
    }

    /**
     * @see #injectLoaderDelegation(ClassLoaderDelegation, Predicate, BiPredicate, Class)
     */
    public static void injectLoaderDelegation(@NotNull ClassLoaderDelegation delegation, @NotNull ClassLoader target)
            throws Exception {
        injectLoaderDelegation(delegation, (requester, request) -> true, target);
    }

    /**
     * @see #injectLoaderDelegation(ClassLoaderDelegation, Predicate, BiPredicate, Class)
     */
    public static void injectLoaderDelegation(@NotNull ClassLoaderDelegation delegation,
                                              @NotNull BiPredicate<Class<?>, String> filter,
                                              @NotNull ClassLoader target)
            throws Exception {
        injectLoaderDelegation(delegation, cl -> cl == target, filter, target.getClass());
    }

    /**
     * Injects a hook into the given target class loader that will be called on any class loading request.
     * <p>
     * Whenever (any instance of) the target class loader receives a request to load or find a certain class,
     * the request is forwarded to the given delegation container instead, provided the request passes the filters.
     * If the delegation returns a {@code null} value, the request will be handled by the original class loader as usual.
     * If the delegation returns a {@code non-null} value, the usual computation of the class loader will be aborted
     * and the hook-computed value will be returned instead.
     * <p>
     * The underlying implementation has built-in protection against recursion. Without recursion detection &amp; protection,
     * an infinite recursion (would eventually throw a {@link StackOverflowError}) would occur if the injection target
     * is one of the delegation's parents: A regular class loader will always first delegate a request to its parent.
     * If the parent happens to be the injection target, it will then (because of the injected hook) delegate the request
     * to the class loader delegation which will, again, first delegate the request to its parent. É voilà, recursion.
     *
     * @param delegation    the delegation that requests will be forwarded to
     * @param filterLoader  the predicate that determines if a particular class loader instance should forward a request
     * @param filterRequest the predicate that determines if a particular class loading request should be forwarded;
     *                      the first argument is the class that is initially requesting the class that is to be loaded,
     *                      the second argument is the fully qualified name of the class that is to be loaded
     * @param target        the target class that will be injected
     * @see ClassLoaderDelegation#ClassLoaderDelegation(ClassLoader)
     */
    public static void injectLoaderDelegation(@NotNull ClassLoaderDelegation delegation,
                                              @NotNull Predicate<ClassLoader> filterLoader,
                                              @NotNull BiPredicate<Class<?>, String> filterRequest,
                                              @NotNull Class<? extends ClassLoader> target)
            throws Exception {
        new ClassLoaderDelegationHook(target, delegation.additional.getAndSet(null),
                delegation, filterLoader, filterRequest).inject();
    }

    // - jars -

    /**
     * Performs an injection into the given target class loader, that will load classes from the given jars.
     *
     * @see #injectLoaderDelegation(ClassLoaderDelegation, Predicate, BiPredicate, Class)
     */
    public static void tweakLoader(@NotNull Collection<Path> jars, @NotNull ClassLoader target)
            throws Exception {
        tweakLoader(jars, (requester, request) -> true, target);
    }

    /**
     * Performs an injection into the given target class loader, that will load classes from the given jars.
     *
     * @see #injectLoaderDelegation(ClassLoaderDelegation, Predicate, BiPredicate, Class)
     */
    public static void tweakLoader(@NotNull Collection<Path> jars,
                                   @NotNull BiPredicate<Class<?>, String> filter,
                                   @NotNull ClassLoader target)
            throws Exception {
        tweakLoader(jars, cl -> cl == target, filter, target.getClass());
    }

    /**
     * Performs an injection into the given target class loader, that will load classes from the given jars.
     *
     * @see #injectLoaderDelegation(ClassLoaderDelegation, Predicate, BiPredicate, Class)
     */
    public static void tweakLoader(@NotNull Collection<Path> jars,
                                   @NotNull Predicate<ClassLoader> filterLoader,
                                   @NotNull BiPredicate<Class<?>, String> filterRequest,
                                   @NotNull Class<? extends ClassLoader> target) throws Exception {
        injectLoaderDelegation(new ClassLoaderDelegation(new URLClassLoader(
                jars.stream()
                        .map(p -> Expect.compute(() -> p.toUri().toURL()).orElseRethrowRE())
                        .toArray(URL[]::new)
        ), true), filterLoader, filterRequest, target);
    }

}
