package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.io.transform.AbstractTransformer;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import cc.fluse.ulib.core.io.transform.ex.LackOfDataException;
import org.jetbrains.annotations.NotNull;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

import static cc.fluse.ulib.core.util.Semantics.nofail;

public final class CipherTransformer extends AbstractTransformer {

    private final Cipher cipher;

    public CipherTransformer(@NotNull Cipher cipher) {
        this.cipher = cipher;
    }

    @Override
    public boolean holdsData() throws ClosedChannelException {
        return super.holdsData() || !isCompleted();
    }

    @Override
    protected void implWrite(@NotNull ByteBuffer src) throws InputFormatException {
        var buf = Internal.bufalloc(cipher.getOutputSize(src.remaining()));
        try {
            nofail(ShortBufferException.class, () -> cipher.update(src, buf));
        } catch (Exception e) {
            throw new InputFormatException(e);
        }
        writeResult(buf.flip());
    }

    @Override
    protected void implComplete() throws IOTransformException {
        ByteBuffer buf;
        try {
            buf = ByteBuffer.wrap(cipher.doFinal());
        } catch (IllegalBlockSizeException e) {
            throw new LackOfDataException(e);
        } catch (BadPaddingException e) {
            throw new InputFormatException(e);
        }
        writeResult(buf);
    }
}
