package cc.fluse.ulib.core.impl.inject;

class HookException extends RuntimeException {
    HookException(Throwable cause) {
        super(cause);
    }
}
