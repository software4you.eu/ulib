package cc.fluse.ulib.core.configuration;

import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Represents a generic configuration that whose data can be dumped to a stream.
 */
public interface DumpableConfiguration extends Configuration {
    /**
     * Serializes the data and dumps it to a writer.
     *
     * @param writer the writer to write to
     */
    void dump(@NotNull Writer writer) throws IOException;

    /**
     * Serializes the data and dumps it to an output stream.
     *
     * @param out the stream to write to
     */
    default void dump(@NotNull OutputStream out) throws IOException {
        var wr = new OutputStreamWriter(out);
        dump(wr);
        wr.flush();
    }

    default void dump(@NotNull WritableByteChannel ch) throws IOException {
        var wr = Channels.newWriter(ch, Charset.defaultCharset());
        dump(wr);
        wr.flush();
    }

    /**
     * Serializes the data and dumps it to a file.
     *
     * @param path the path to write to
     */
    default void dumpTo(@NotNull Path path) throws IOException {
        try (var writer = Files.newBufferedWriter(path)) {
            dump(writer);
        }
    }

    /**
     * Serializes the data to a byte sequence.
     *
     * @return a read only byte buffer
     */
    @NotNull
    default ByteBuffer dump() {
        var bout = new ByteArrayOutputStream();
        Expect.compute(() -> dump(bout)).rethrowRE();
        return ByteBuffer.wrap(bout.toByteArray()).asReadOnlyBuffer();
    }

    /**
     * Serializes the data to a string.
     *
     * @return the string
     */
    @NotNull
    default String dumpString() {
        var bout = new ByteArrayOutputStream();
        Expect.compute(() -> dump(bout)).rethrowRE();
        return bout.toString();
    }
}
