package cc.fluse.ulib.core.impl.database.sql.sqlite;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.SQLiteDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase.DBImpl;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;
import lombok.Getter;
import lombok.SneakyThrows;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Properties;

@DBImpl(names = {"sqlite", "sqlite3"},
        driverCoords = "{{maven.sqlite}}",
        protocol = "jdbc:sqlite:")
public final class SQLiteDatabaseImpl extends AbstractSqlDatabase implements SQLiteDatabase {

    @Getter
    private final Path path;

    @SneakyThrows
    public SQLiteDatabaseImpl(Connection connection) {
        super(connection);
        path = determine(connection.getMetaData().getURL());
    }

    public SQLiteDatabaseImpl(String url, Properties info) {
        super(url, info, 0); // disable pooling
        path = determine(url);
    }

    /**
     * Parses the file path from a jdbc url.
     *
     * @param url The url to parse
     * @return The file path
     */
    private static Path determine(String url) {
        if (!url.startsWith("jdbc:sqlite:")) {
            throw new IllegalArgumentException(String.format("Unknown protocol: %s", url));
        }
        return Paths.get(url.substring(12));
    }

    @Override
    protected AbstractSqlTable newTable(String name, Collection<Column<?>> columns) {
        return new SQLiteTable(this, name, columns);
    }

    @Override
    protected ResultSet getTablesInfo(DatabaseMetaData meta) throws SQLException {
        return meta.getTables(null, null, null, new String[]{"TABLES", "TABLE"});
    }
}
