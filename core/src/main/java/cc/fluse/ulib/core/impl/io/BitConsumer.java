package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public final class BitConsumer<X extends Exception> implements ParamTask<Boolean, X> {

    private final ParamTask<? super Integer, X> consumer;
    private int word;
    private int cursor;

    public BitConsumer(@NotNull ParamTask<? super Integer, X> byteConsumer) {
        this.consumer = byteConsumer;
    }

    public synchronized void putNextBit(boolean set) throws X {
        if (set) {
            word |= 1 << cursor;
        }

        if (++cursor > 7) {
            flush();
        }
    }

    public synchronized void flush() throws X {
        if (cursor > 0) {
            consumer.execute(Byte.toUnsignedInt((byte) word));
            word = 0;
            cursor = 0;
        }
    }

    @Override
    @BypassAnnotationEnforcement
    public void execute(Boolean aBoolean) throws X {
        putNextBit(Objects.requireNonNull(aBoolean));
    }
}
