package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;

/**
 * An object able to hold 4 values.
 *
 * @param <T> the type of the first value
 * @param <U> the type of the second value
 * @param <V> the type of the third value
 * @param <W> the type of the fourth value
 */
@BypassAnnotationEnforcement
public interface Quadruple<T, U, V, W> extends Triple<T, U, V> {
    W getFourth();
}
