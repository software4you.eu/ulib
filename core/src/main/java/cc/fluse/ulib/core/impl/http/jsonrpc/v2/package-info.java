/**
 * Classes implementing the <a href="https://www.jsonrpc.org/specification">JsonRPC version 2 standard</a>.
 */
package cc.fluse.ulib.core.impl.http.jsonrpc.v2;