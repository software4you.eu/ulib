package cc.fluse.ulib.core.cli;

import org.jetbrains.annotations.NotNull;

/**
 * Representation of an CLI option.
 */
public interface CliOption {

    /**
     * @return the option's long name
     */
    @NotNull
    String getName();

    /**
     * @return the option's description
     */
    @NotNull
    String getDescription();

    /**
     * @return a clone of the short names array
     */
    char @NotNull [] getShortNames();

    /**
     * @return the option's argument properties
     */
    @NotNull
    ArgProps getArgProps();

    /**
     * Sets (overwrites!) the required options that have to be present in order for this option to get accepted.
     * <p>
     * Only one of the supplied option arrays has to be present.
     *
     * @param anyOf an array of option arrays
     */
    void requires(@NotNull CliOption @NotNull [] @NotNull [] anyOf);

    default void requires(@NotNull CliOption @NotNull ... allOf) {
        requires(new CliOption[][]{allOf});
    }

    /**
     * Sets this option object to be incompatible with another option.
     *
     * @param others the other options
     */
    void incompatible(@NotNull CliOption @NotNull ... others);

}
