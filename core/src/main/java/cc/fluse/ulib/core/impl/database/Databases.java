package cc.fluse.ulib.core.impl.database;

import cc.fluse.ulib.core.configuration.Configuration;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.database.Database.Protocol;
import cc.fluse.ulib.core.database.FileDatabase;
import cc.fluse.ulib.core.database.RemoteDatabase;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.util.Expect;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.InvocationTargetException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.*;

public final class Databases {

    // - protocols -
    private static class ProtocolImpl<T extends Database> implements Protocol<T> {

        private final Class<? extends T> impl;
        private final AbstractSqlDatabase.DBImpl meta;
        @Getter(AccessLevel.PRIVATE)
        private final Collection<String> names;

        private ProtocolImpl(Class<? extends T> impl) {
            this.impl = impl;
            this.meta = UndefinedStateError.ensureNotNull(impl.getAnnotation(AbstractSqlDatabase.DBImpl.class), "Missing metadata");
            this.names = List.of(meta.names());
        }

        private Class<? extends T> type() {
            return impl;
        }

        @Override
        public @NotNull String protocol() {
            return meta.protocol();
        }

        @Override
        public String toString() {
            return protocol();
        }

        public boolean supportsConnectionPooling() {
            return meta.supportsConnectionPooling();
        }
    }

    private static final Collection<ProtocolImpl<?>> protocols = new LinkedList<>();

    public static <T extends Database> Protocol<T> protocol(Class<? extends T> impl) {
        var p = new ProtocolImpl<T>(impl);
        protocols.add(p);
        return p;
    }

    // - functions -

    /**
     * @see Database#prepare(Configuration)
     */
    public static Database prepare(@NotNull Configuration configuration) {
        var typeStr = configuration.string("type").orElseThrow(() -> new IllegalArgumentException("Missing database type."));

        // handle url
        if (typeStr.equalsIgnoreCase("url")) {
            var url = configuration.string("url").orElseThrow(() -> new IllegalArgumentException("Missing database url."));
            var properties = configuration.getSubsection(KeyPath.of("properties"))
                    .map(sec -> sec.getValues(false))
                    .map(map -> {
                        var props = new Properties();
                        map.forEach((k, v) -> props.setProperty(k.toLegacyString(), v.toString()));
                        return props;
                    })
                    .orElse(new Properties());
            return prepare(url, properties, configuration.int32("max-pool-size").orElse(0));
        }

        // figure out protocol
        var protocol = protocols.stream()
                .filter(p -> p.names.stream().anyMatch(typeStr::equalsIgnoreCase))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown database type: %s", typeStr)));

        // check if type is file database
        if (FileDatabase.class.isAssignableFrom(protocol.type())) {
            // get file database configuration
            var path = configuration.string("path").orElseThrow(() -> new IllegalArgumentException("Missing database path."));
            //noinspection unchecked
            return prepare((Protocol<? extends FileDatabase>) protocol, Path.of(path));
        }

        if (!(RemoteDatabase.class.isAssignableFrom(protocol.type()))) {
            throw new IllegalArgumentException("Database type is not supported.");
        }

        // get remote database configuration
        //noinspection unchecked
        return prepare((Protocol<? extends RemoteDatabase>) protocol,
                configuration.int32("max-pool-size").orElse(0),
                configuration.string("host").orElseThrow(() -> new IllegalArgumentException("Missing database host.")),
                configuration.int32("port").orElseThrow(() -> new IllegalArgumentException("Missing database port.")),
                configuration.string("database").orElseThrow(() -> new IllegalArgumentException("Missing database name.")),
                configuration.string("username").orElseThrow(() -> new IllegalArgumentException("Missing database username.")),
                configuration.string("password").orElseThrow(() -> new IllegalArgumentException("Missing database password."))
        );
    }

    public static Database prepare(String url, Properties info, int maxPoolSize) {
        return protocols.stream()
                .filter(p -> url.startsWith(p.protocol()))
                .map(protocol -> {
                    try {
                        return protocol.supportsConnectionPooling() ?
                                protocol.type().getDeclaredConstructor(String.class, Properties.class, int.class).newInstance(url, info, maxPoolSize)
                                : protocol.type().getDeclaredConstructor(String.class, Properties.class).newInstance(url, info);
                    } catch (IllegalAccessException e) {
                        throw new UndefinedStateError(e);
                    } catch (InvocationTargetException e) {
                        if (e.getCause() instanceof RuntimeException re) throw re;
                        throw new RuntimeException(e.getCause());
                    } catch (InstantiationException e) {
                        throw new RuntimeException(e);
                    } catch (NoSuchMethodException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown protocol: %s", url)));
    }

    public static SqlDatabase wrap(Connection connection) {
        if (Expect.compute(connection::isClosed).orElse(true)) {
            throw new IllegalStateException("Connection is closed.");
        }
        var url = Expect.compute(() -> connection.getMetaData().getURL()).orElseRethrowRE();


        return protocols.stream()
                .filter(p -> url.startsWith(p.protocol()))
                .map(ProtocolImpl::type)
                .map(cl -> {
                    try {
                        //noinspection unchecked
                        return ((Class<? extends SqlDatabase>) cl)
                                .getDeclaredConstructor(Connection.class).newInstance(connection);
                    } catch (IllegalAccessException e) {
                        throw new UndefinedStateError(e);
                    } catch (InvocationTargetException e) {
                        if (e.getCause() instanceof RuntimeException re) throw re;
                        throw new RuntimeException(e.getCause());
                    } catch (InstantiationException e) {
                        throw new RuntimeException(e);
                    } catch (NoSuchMethodException e) {
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unknown protocol: %s", url)));
    }

    public static <T extends FileDatabase> T prepare(Protocol<T> protocol, Path path) {
        // TODO: url encode path?
        //noinspection unchecked
        return (T) prepare(protocol.protocol() + path.toString(), new Properties(), 0);
    }

    @SneakyThrows
    public static <T extends RemoteDatabase> T prepare(Protocol<T> protocol, int maxPoolSize,
                                                       String host, int port, String database, String user, String password,
                                                       String... parameters) {
        var params = new StringJoiner("&", "?", "");
        params.setEmptyValue("");
        for (String parameter : parameters) {
            params.add(parameter);
        }
        var url = String.format("%s%s:%d/%s%s",
                protocol.protocol(),
                URLEncoder.encode(host, StandardCharsets.UTF_8),
                port,
                URLEncoder.encode(database, StandardCharsets.UTF_8),
                params
        );
        var info = new Properties();
        if (user != null) {
            info.put("user", user);
        }
        if (password != null) {
            info.put("password", password);
        }
        //noinspection unchecked
        return (T) prepare(url, info, maxPoolSize);
    }
}
