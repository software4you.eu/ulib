package cc.fluse.ulib.core.file;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;

public class MissingParentException extends HierarchyException {

    @Getter
    @NotNull
    private final Path target;

    public MissingParentException(@NotNull Path target) {
        this(target, null);
    }

    public MissingParentException(@NotNull Path target, @Nullable Throwable cause) {
        super("The target's parent is inaccessible or does not exist: " + target, cause);
        this.target = target;
    }

}
