package cc.fluse.ulib.core.util;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.impl.Concurrent;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class provides a way to execute a task or function for a certain resource in a multi-threaded manner while
 * preventing infinite recursion.
 * <p>
 * This class is meant to be utilized by classes that frequently do recursive calls to themselves or other classes.
 */
public class ThreadedRecursionAccess {

    private final Map<Object, Object> $locks = Concurrent.newThreadsafeMap();
    private final Map<Object, Collection<Thread>> accessingThreads = Concurrent.newThreadsafeMap();

    /**
     * Executes the given task for the given resource identifier. This method ensures that the same thread can only
     * access the same resource identifier once at a time. If the same thread tries to access the same resource
     * identifier again, this method will return {@code false} and the task will not be executed, thus preventing
     * infinite recursion.
     *
     * @param identifier identifier of resource
     * @param task       task to execute
     * @param <X>        type of exception thrown by task
     * @return {@code true} if task was executed, {@code false} if task was not executed because the identifier is
     * already being accessed by the same thread
     * @throws X exception thrown by task
     */
    public <X extends Exception> boolean access(@NotNull Object identifier, @NotNull Task<X> task) throws X {
        // get or create lock for identifier
        Object $lock;
        boolean lockCreated = false;
        synchronized ($locks) {
            if (!$locks.containsKey(identifier)) {
                $locks.put(identifier, $lock = new Object[0]);
                lockCreated = true;
            } else {
                $lock = $locks.get(identifier);
            }
        }

        // check if thread is already accessing identifier
        var thr = Thread.currentThread();
        Collection<Thread> threads;
        synchronized ($lock) {
            threads = accessingThreads.computeIfAbsent(identifier, k -> Concurrent.newThreadsafeCollection());
            if (threads.contains(thr)) return false; // thr is already accessing identifier, -> recursion detected
            // thread is not accessing identifier, continue
            threads.add(thr); // indicate that thread is accessing identifier
        }

        // execute task
        try {
            task.execute();
        } finally {
            // remove thread from accessing identifier
            synchronized ($lock) {
                threads.remove(thr); // indicate that thread is no longer accessing identifier
                if (threads.isEmpty()) {
                    accessingThreads.remove(identifier);
                }

                // remove lock if it was created
                if (lockCreated) {
                    synchronized ($locks) {
                        $locks.remove(identifier);
                    }
                }
            }
        }

        return true;
    }

    /**
     * Executes the given function for the given resource identifier. This method ensures that the same thread can only
     * access the same resource identifier once at a time. If the same thread tries to access the same resource
     * identifier again, this method will return {@code null} and the function will not be executed, thus preventing
     * infinite recursion. If the function was executed, the return value of the function will be returned wrapped in an
     * {@link AtomicReference}. It is guaranteed that the return value will be only {@code null} if the function was not
     * executed because the identifier is already being accessed by the same thread.
     *
     * @param identifier identifier of resource
     * @param func       function to execute
     * @param <T>        type of return value of function
     * @param <X>        type of exception thrown by function
     * @return return value of function wrapped in an {@link AtomicReference} if function was executed, {@code null} if
     * function was not executed because the identifier is already being accessed by the same thread
     * @throws X exception thrown by function
     */
    public <T, X extends Exception> @Nullable AtomicReference<T> access(@NotNull Object identifier, @NotNull Func<T, X> func) throws X {
        var ref = new AtomicReference<T>();
        if (!access(identifier, () -> ref.set(func.execute()))) return null;
        return ref;
    }

}
