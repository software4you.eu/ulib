package cc.fluse.ulib.core.database.sql.orm;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.Table;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import org.jetbrains.annotations.NotNull;

/**
 * Represents an entity in a database.
 */
@BypassAnnotationEnforcement
public interface Entity {

    /**
     * Returns the table this entity belongs to.
     *
     * @return the table
     */
    @NotNull Table getTable();

    /**
     * Returns the value of a column. Deserializes the value if necessary.
     *
     * @param column The column.
     * @return The value of the column, may be {@code null}, depending on whether the column allows {@code null} values.
     */
    Object get(@NotNull Column<?> column) throws Serializer.SerializerException;

    /**
     * Returns the value of a column. Deserializes the value if necessary.
     *
     * @param columnName The name of the column.
     * @return The value of the column, may be {@code null}, depending on whether the column allows {@code null} values.
     * @throws IllegalArgumentException If the column does not exist.
     */

    default Object get(@NotNull String columnName) throws Serializer.SerializerException {
        return get(getTable().getColumn(columnName).orElseThrow(() -> new IllegalArgumentException("Column not found: " + columnName)));
    }

    /**
     * Returns the value of a column. Does not deserialize the value.
     *
     * @param column The column.
     * @return The value of the column, may be {@code null}, depending on whether the column allows {@code null} values.
     */
    <T> T getDirect(@NotNull Column<T> column);

    /**
     * Returns the value of a column. Does not deserialize the value.
     *
     * @param columnName The name of the column.
     * @return The value of the column, may be {@code null}, depending on whether the column allows {@code null} values.
     * @throws IllegalArgumentException If the column does not exist.
     */
    default Object getDirect(@NotNull String columnName) {
        return getDirect(getTable().getColumn(columnName).orElseThrow(() -> new IllegalArgumentException("Column not found: " + columnName)));
    }

    /**
     * Sets the value of a column. Serializes the value if necessary.
     *
     * @param column The column.
     * @param value  The value to set.
     * @throws IllegalArgumentException If the value is not of the correct type and cannot be serialized.
     */
    void set(@NotNull Column<?> column, Object value) throws Serializer.SerializerException;

    /**
     * Sets the value of a column. Serializes the value if necessary.
     *
     * @param columnName The name of the column.
     * @param value      The value to set.
     * @throws IllegalArgumentException If the column does not exist or the value is not of the correct type and cannot
     *                                  be serialized.
     */
    default void set(@NotNull String columnName, Object value) throws Serializer.SerializerException {
        set(getTable().getColumn(columnName).orElseThrow(() -> new IllegalArgumentException("Column not found: " + columnName)), value);
    }

    /**
     * Sets the value of a column. Does not serialize the value.
     *
     * @param column The column.
     * @param value  The value to set.
     * @param <T>    The type of the value.
     * @throws IllegalArgumentException If the value is not of the correct type.
     */
    <T> void setDirect(@NotNull Column<T> column, T value);

    /**
     * Sets the value of a column. Does not serialize the value.
     *
     * @param columnName The name of the column.
     * @param value      The value to set.
     * @throws IllegalArgumentException If the column does not exist or the value is not of the correct type.
     */
    default void setDirect(@NotNull String columnName, Object value) {
        //noinspection unchecked
        setDirect((Column<Object>) getTable().getColumn(columnName).orElseThrow(() -> new IllegalArgumentException("Column not found: " + columnName)), value);
    }

    /**
     * Deletes this entity from the database.
     */
    void delete();

    /**
     * Waits for all lazily pushed updates to be submitted to the database.
     * <p>
     * <b>Note:</b> Another thread may submit updates to this entity, while this thread is waiting, increasing the
     * waiting time.
     *
     * @throws InterruptedException If the thread is interrupted while waiting.
     */
    void waitFor() throws InterruptedException;

}
