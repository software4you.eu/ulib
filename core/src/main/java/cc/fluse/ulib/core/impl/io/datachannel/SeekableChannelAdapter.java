package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.IOUtil;
import org.jetbrains.annotations.Nullable;

import java.io.Closeable;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.NonWritableChannelException;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.util.Objects;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.*;

public class SeekableChannelAdapter implements SeekableByteChannel {

    private final Bouncer.ChBouncer bouncer = Bouncer.ch();
    private final ReadableByteChannel channel;
    private final int memoryLimit;
    private final boolean allowFileMode;

    private boolean memory = true; // true if data is stored in memory, false if data is stored in tempFile
    private @Nullable ByteBuffer buffer; // in-memory buffer
    private int bufferDataSize = 0; // size of data in buffer
    private @Nullable SeekableByteChannel fileBuffer; // will be initialized when in-memory cache exceeds memoryLimit
    private long position;

    /**
     * Creates a new {@link SeekableChannelAdapter} that uses an in-memory buffer with the given memory limit. If the
     * memory limit is exceeded, this channel adapter will switch to a file buffer. If the memory limit is 0 or
     * negative, this channel adapter will be initialized in file buffer mode.
     *
     * @param channel       the channel to wrap
     * @param memoryLimit   the memory limit in bytes
     * @param allowFileMode whether this channel adapter is allowed to switch to file buffer mode
     * @throws IOException if an I/O error occurs
     */
    public SeekableChannelAdapter(ReadableByteChannel channel, int memoryLimit, boolean allowFileMode)
    throws IOException {
        this.channel = channel;
        this.allowFileMode = allowFileMode;

        if ((this.memoryLimit = memoryLimit) > 0) {
            this.buffer = Internal.bufalloc(Math.min(Internal.getDefaultBufferCapacity(), this.memoryLimit));
            return;
        }

        // memory limit is 0 or negative, immediately switch to file buffer
        switchToFBuffer();
    }

    /**
     * Enlarges the in-memory buffer if possible. If the in-memory buffer cannot be enlarged due to memory limit being
     * exceeded, this channel adapter will switch to a file buffer. If the enlargement is successful, this method
     * returns {@code true}. If the in-memory buffer cannot be enlarged and this channel adapter switches to a file
     * buffer, this method returns {@code false}. While switching to a file buffer, no new data is read from the source
     * channel.
     * <p>
     * If this method succeeds the buffer will be enlarged to at least the current position plus one byte.
     *
     * @param req the number of requested bytes to enlarge the buffer by
     * @return {@code true} if the in-memory buffer was successfully enlarged, {@code false} if this channel adapter
     * switched to a file buffer
     *
     * @throws IOException if an I/O error occurs
     */
    private boolean enlargeIMBuffer(long req) throws IOException {
        if (!memory) throw new IllegalStateException("Cannot enlarge buffer when not in memory mode");
        UndefinedStateError.ensureNotNull(buffer);
        if (position < buffer.capacity()) return true; // buffer already large enough

        // check if memory limit is would be exceeded
        if (position + 1 >= memoryLimit) {
            // memory limit is exceeded
            switchToFBuffer();
            return false; // indicate switch to file channel
        }

        // memory limit is will not be exceeded
        // enlarge buffer to position plus requested bytes or memory limit, but at least one byte
        var newBuffer = Internal.bufalloc(Math.min((int) Math.min(position + req, Integer.MAX_VALUE), memoryLimit));
        buffer.position(0);
        buffer.limit(bufferDataSize);
        newBuffer.put(buffer);
        buffer = newBuffer;
        return true; // indicate that buffer was enlarged
    }

    /**
     * Fills the in-memory buffer with new data from the channel. If the in-memory buffer cannot be filled due to memory
     * limit being exceeded, this channel adapter will switch to a file buffer. If the buffer was successfully filled,
     * this method returns {@code true}. If this channel adapter switches to a file buffer, this method returns
     * {@code false}. While switching to a file buffer, no new data is read from the source channel.
     * <p>
     * If this method succeeds, the buffer will contain at least one additional byte at the current position.
     *
     * @return {@code true} if the buffer was successfully filled, {@code false} if this channel adapter switched to
     * file channel
     *
     * @throws IOException if an I/O error occurs
     */
    private boolean fillIMBuffer(int req) throws IOException {
        if (!memory) throw new IllegalStateException("Cannot fill buffer when not in memory mode");
        UndefinedStateError.ensureNotNull(buffer);
        if (position < bufferDataSize) return true; // buffer already contains data at position

        // TODO: ensure current position can be read after this method returns

        // calculate number of bytes to read up to requested bytes or memory limit, but at least one byte
        var fill = Math.max(position + 1 - bufferDataSize, Math.min(memoryLimit - bufferDataSize, (int) position + req - bufferDataSize));

        // enlarge buffer if necessary and possible, switch to file channel if necessary
        if (bufferDataSize + fill > buffer.capacity() && !enlargeIMBuffer(fill)) {
            return false; // indicate switch to file channel
        }

        // transfer data from channel
        buffer.position(bufferDataSize);
        for (long limit = bufferDataSize + fill; bufferDataSize < limit; ) {
            int read = channel.read(buffer);
            if (read == -1) return true; // EOF
            bufferDataSize += read;
        }
        return true; // indicate that buffer was filled
    }

    /**
     * Switches this channel adapter to a file buffer. If this channel adapter is already using a file buffer, this
     * method throws an {@link IllegalStateException}.
     * <p>
     * Switching to a file buffer will flush the in-memory buffer to a temporary file. The in-memory buffer will be
     * cleared subsequently.
     *
     * @throws IOException           if an I/O error occurs
     * @throws IllegalStateException if this channel adapter is already using a file buffer
     */
    private void switchToFBuffer() throws IOException {
        if (!memory) throw new IllegalStateException("Already using file buffer");
        if (!allowFileMode) throw new BufferOverflowException();

        // create temp file
        fileBuffer = Files.newByteChannel(Files.createTempFile(null, null), READ, WRITE, CREATE, DELETE_ON_CLOSE);

        if (buffer != null) {
            // flush buffer to file
            if (bufferDataSize > 0) {
                buffer.position(0);
                buffer.limit(bufferDataSize);
                fileBuffer.write(buffer);
            }

            // clear buffer
            buffer.clear();
            buffer = null;
        }
        memory = false;
    }

    @Override
    public synchronized int read(ByteBuffer dst) throws IOException {
        bouncer.pass();
        if (!dst.hasRemaining()) return 0;

        memory_read:
        if (memory) {
            UndefinedStateError.ensureNotNull(buffer);

            // check if position to seek to is in buffer
            if (position >= bufferDataSize) {
                // position is not in buffer
                int prevSize = bufferDataSize;
                if (!fillIMBuffer(dst.remaining())) break memory_read; // switch to file buffer
                if (prevSize == bufferDataSize) return -1; // EOF
            }

            // seek to position in buffer and read data
            buffer.position((int) position);
            buffer.limit(Math.min(bufferDataSize, (int) position + dst.remaining()));

            // semantics ensure that buffer and dst have at least one byte remaining
            do {
                dst.put(buffer);
            } while (dst.hasRemaining() && buffer.hasRemaining());
            int read = buffer.position() - (int) position;
            if (read == 0) {
                throw new UndefinedStateError("In-memory buffer returned EOF at position %d, even though it should not".formatted(position));
            }
            position += read;
            buffer.limit(buffer.capacity());
            return read;
        }

        UndefinedStateError.ensureNotNull(fileBuffer);

        // read from file buffer
        // check if position to seek to is in buffer
        if (position >= fileBuffer.size()) {
            // position is not in buffer
            // transfer data from channel to buffer
            fileBuffer.position(fileBuffer.size());
            var prevSize = fileBuffer.size();
            IOUtil.transfer(channel, fileBuffer, position - fileBuffer.size() + dst.remaining());
            if (prevSize == fileBuffer.size()) return -1; // EOF
        }

        // seek to position in buffer and read data
        fileBuffer.position(position);
        int read = fileBuffer.read(dst);
        if (read == -1) {
            throw new UndefinedStateError("Temp file channel returned EOF at position %d, even though it should not".formatted(position));
        }
        position += read;
        return read;
    }

    @Override
    public synchronized long position() throws IOException {
        bouncer.pass();
        return position;
    }

    @Override
    public synchronized SeekableByteChannel position(long newPosition) throws IOException {
        bouncer.pass();
        this.position = newPosition;
        return this;
    }

    @Override
    public int write(ByteBuffer src) throws IOException {
        throw new NonWritableChannelException();
    }

    @Override
    public SeekableByteChannel truncate(long size) {
        throw new NonWritableChannelException();
    }

    @Override
    public synchronized long size() throws IOException {
        bouncer.pass();

        if (memory) {
            UndefinedStateError.ensureNotNull(buffer);
            return bufferDataSize;
        }

        UndefinedStateError.ensureNotNull(fileBuffer);
        return fileBuffer.size();
    }

    @Override
    public boolean isOpen() {
        return bouncer.canPass(); // TODO and channel is open etc
    }

    @Override
    public synchronized void close() throws IOException {
        bouncer.pass();
        bouncer.close();
        if (buffer != null) buffer.clear();
        IOUtil.closeIO(Stream.<Closeable>of(channel, fileBuffer).filter(Objects::nonNull).iterator());
    }
}
