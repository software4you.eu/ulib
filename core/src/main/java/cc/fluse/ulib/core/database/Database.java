package cc.fluse.ulib.core.database;

import cc.fluse.ulib.core.configuration.Configuration;
import cc.fluse.ulib.core.database.sql.MySQLDatabase;
import cc.fluse.ulib.core.database.sql.PostgreSQLDatabase;
import cc.fluse.ulib.core.database.sql.SQLiteDatabase;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.impl.database.Databases;
import cc.fluse.ulib.core.impl.database.sql.mysql.MySQLDatabaseImpl;
import cc.fluse.ulib.core.impl.database.sql.postgres.PostgreSQLDatabaseImpl;
import cc.fluse.ulib.core.impl.database.sql.sqlite.SQLiteDatabaseImpl;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Very basic functions all databases share.
 */
public interface Database extends AutoCloseable {

    /**
     * Represents a database connection protocol.
     *
     * @param <T> the database type
     */
    interface Protocol<T extends Database> {
        @NotNull Protocol<MySQLDatabase> MySQL = Databases.protocol(MySQLDatabaseImpl.class);
        @NotNull Protocol<MySQLDatabase> MariaDB = Databases.protocol(MySQLDatabaseImpl.class);
        @NotNull Protocol<PostgreSQLDatabase> PostgreSQL = Databases.protocol(PostgreSQLDatabaseImpl.class);
        @NotNull Protocol<SQLiteDatabase> SQLite = Databases.protocol(SQLiteDatabaseImpl.class);

        /**
         * @return the protocol prefix
         */
        @NotNull String protocol();

        /**
         * @return whether the protocol supports connection pooling
         */
        boolean supportsConnectionPooling();
    }

    /**
     * Wraps an already existing sql connection.
     *
     * @param connection the connection to be wrapped
     * @return the instance
     * @throws IllegalArgumentException when no suitable wrapper for the used protocol is found.
     */
    @NotNull
    static SqlDatabase wrap(@NotNull Connection connection) {
        return Databases.wrap(connection);
    }

    /**
     * Prepares a database from a configuration.
     * <p>
     * Configuration layout (yaml format in this example, may be any supported format):
     * <pre>
     *  type: value # i.e. 'url', 'mysql', 'postgresql', 'sqlite'
     *
     *  # only applicable for url
     *  url: value # i.e. 'jdbc:mysql://localhost:3306/my_database' # protocol decides the type
     *  properties:
     *      user: value # i.e. 'root'
     *      password: value # i.e. 'password'
     *
     *  # only applicable for file databases (i.e. sqlite)
     *  path: value # i.e. './path/to/database.db'
     *
     *  # only applicable for remote databases (i.e. mysql, postgresql)
     *  host: value # i.e. 'localhost'
     *  port: value # i.e. '3306'
     *  database: value # i.e. 'my_database'
     *  username: value # i.e. 'root'
     *  password: value # i.e. 'password'
     *
     *  # optional
     *  max-pool-size: value # i.e. '4'
     *
     * </pre>
     *
     * @param configuration the configuration
     * @return the database
     */
    static @NotNull Database prepare(@NotNull Configuration configuration) {
        return Databases.prepare(configuration);
    }

    /**
     * Prepares a database connection with optionally connection pooling.
     *
     * @param url         the database url
     * @param info        the connection info
     * @param maxPoolSize the maximum pool size, {@code 0} to disable connection pooling, {@code -1} for no limit.
     *                    Ignored if the protocol does not support connection pooling.
     * @return the instance
     * @throws IllegalArgumentException when no suitable wrapper for the used protocol is found.
     */
    @NotNull
    static Database prepare(@NotNull String url, @NotNull Properties info, int maxPoolSize) {
        return Databases.prepare(url, info, maxPoolSize);
    }

    /**
     * Prepares a file database connection.
     *
     * @param protocol the protocol
     * @param path     path to the database file
     * @return the instance
     */
    @NotNull
    static <T extends FileDatabase> T prepare(@NotNull Protocol<T> protocol, @NotNull Path path) {
        return Databases.prepare(protocol, path);
    }

    /**
     * Prepares a remote database with optionally connection pooling.
     *
     * @param protocol    the protocol
     * @param maxPoolSize the maximum pool size, {@code 0} to disable connection pooling, {@code -1} for no limit.
     * @param host        the host
     * @param port        the port (usually 3306)
     * @param database    the database
     * @param user        the user
     * @param password    the password
     * @param parameters  optional URL parameters (e.g. "param1=val1", "param2=val2"). Will <b>not</b> be encoded.
     * @return the instance
     * @see DriverManager#getConnection(String, String, String)
     * @see URLEncoder#encode(String, String)
     */
    @NotNull
    static <T extends RemoteDatabase> T prepare(@NotNull Protocol<T> protocol, int maxPoolSize,
                                                @NotNull String host, int port, @NotNull String database, @NotNull String user, @NotNull String password, @NotNull String... parameters) {
        return Databases.prepare(protocol, maxPoolSize, host, port, database, user, password, parameters);
    }

    /**
     * Creates a new Database and connects to it (with optionally connection pooling).
     *
     * @param url         the database url
     * @param info        the connection arguments, usually at least a {@code user} and a {@code password}
     * @param maxPoolSize the maximum pool size, {@code 0} to disable connection pooling, {@code -1} for no limit.
     *                    Ignored if the protocol does not support connection pooling.
     * @return the instance
     * @throws IllegalArgumentException when no suitable wrapper for the used protocol is found.
     * @throws IOException              If the connection could not be established.
     * @see DriverManager#getConnection(String, Properties)
     */
    @NotNull
    static Database connect(@NotNull String url, @NotNull Properties info, int maxPoolSize) throws IllegalArgumentException, IOException {
        var db = prepare(url, info, maxPoolSize);
        db.connect();
        return db;
    }

    /**
     * Creates a new file database connection and wraps it.
     *
     * @param path path to the SQLite file
     * @return the instance
     * @throws IOException If the connection could not be established.
     * @see DriverManager#getConnection(String)
     */
    @NotNull
    static <T extends FileDatabase> T connect(@NotNull Protocol<T> protocol, @NotNull Path path) throws IOException {
        var db = prepare(protocol, path);
        db.connect();
        return db;
    }

    /**
     * Creates a new remote database and connects to it (with optionally connection pooling).
     *
     * @param protocol    the protocol
     * @param maxPoolSize the maximum pool size, {@code 0} to disable connection pooling, {@code -1} for no limit.
     * @param host        the host
     * @param port        the port (usually 3306)
     * @param database    the database
     * @param user        the user
     * @param password    the password
     * @param parameters  optional URL parameters (e.g. "param1=val1", "param2=val2"). Will <b>not</b> be encoded.
     * @return the instance
     * @throws IOException If the connection could not be established.
     * @see DriverManager#getConnection(String, String, String)
     * @see URLEncoder#encode(String, String)
     */
    @NotNull
    static <T extends RemoteDatabase> T connect(@NotNull Protocol<T> protocol, int maxPoolSize,
                                                @NotNull String host, int port, @NotNull String database, @NotNull String user, @NotNull String password, @NotNull String... parameters) throws IOException {
        var db = prepare(protocol, maxPoolSize, host, port, database, user, password, parameters);
        db.connect();
        return db;
    }

    /**
     * Checks if a connection to the database exists.
     *
     * @return {@code true} if the database is connected, {@code false} otherwise.
     */
    boolean isConnected();

    /**
     * Attempts to create a connection to the database.
     *
     * @throws IllegalStateException when attempting to re-create a already existing connection.
     * @throws IOException           when a connection could not be established.
     */
    void connect() throws IOException;

    /**
     * Attempts to close a existing connection to the database.
     *
     * @throws IllegalStateException when a attempting to close a not existing connection.
     */
    void disconnect();

    @Override
    default void close() {
        if (isConnected()) disconnect();
    }
}
