package cc.fluse.ulib.core.impl.database.sql;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

@Getter
public final class ColumnImpl<T> implements Column<T> {
    private final Class<T> type;
    private final String name;
    private final DataType dataType;
    private final boolean notNull;
    private final boolean autoIncrement;
    private final Column.Index index;
    private final long size;
    private final T defaultValue;
    private final T[] acceptable;

    /**
     * {@code type} must be equal to or a supertype of {@code dataType.getClazz()}
     */
    public ColumnImpl(Class<T> type, String name, DataType dataType, boolean notNull, boolean autoIncrement, Index index, long size, T defaultValue, T[] acceptable) {
        if (!type.isAssignableFrom(dataType.getClazz()))
            throw new IllegalArgumentException("Incompatible type %s for data type %s".formatted(type, dataType));
        this.type = type;
        this.name = name;
        this.dataType = dataType;
        this.notNull = notNull;
        this.autoIncrement = autoIncrement;
        this.index = index;
        this.size = size;
        this.defaultValue = defaultValue;
        this.acceptable = acceptable;
    }

    @NotNull
    public Optional<Index> getIndex() {
        return Optional.ofNullable(index);
    }

    @Override
    public boolean equals(Object o) {
        return ReflectUtil.autoEquals(this, o);
    }

    @Override
    public int hashCode() {
        return ReflectUtil.autoHash(this);
    }
}
