package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.impl.tuple.*;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.*;

/**
 * An immutable ordered list of elements.
 */
@BypassAnnotationEnforcement
public interface Tuple<E> extends Iterable<E> {

    /**
     * Attempts to create a 2-,3- or 4-tuple from a collection. The returned object is either a {@link Pair}, a
     * {@link Triple} or a {@link Quadruple}.
     *
     * @param collection the collection to create the tuple from
     * @return the tuple object
     *
     * @throws IllegalArgumentException if the collection has a size other than 1, 2, 3 or 4
     */
    @NotNull
    static Tuple<Object> asTuple(@NotNull Collection<?> collection) {
        var it = collection.iterator();
        return switch (collection.size()) {
            case 1 -> of(it.next());
            case 2 -> of(it.next(), it.next());
            case 3 -> of(it.next(), it.next(), it.next());
            case 4 -> of(it.next(), it.next(), it.next(), it.next());
            default -> throw new IllegalArgumentException("Cannot create a %d-tuple".formatted(collection.size()));
        };
    }

    /**
     * Creates a new 1-tuple.
     *
     * @param t the first element
     * @return the created object
     */
    @NotNull
    static <T> Value<T> of(T t) {
        return new Ref<>(t);
    }


    /**
     * Creates a new 2-tuple.
     *
     * @param t the first element
     * @param u the second element
     * @return the created object
     */
    @NotNull
    static <T, U> Pair<T, U> of(T t, U u) {
        return new PairImpl<>(t, u);
    }

    /**
     * Creates a new 3-tuple.
     *
     * @param t the first element
     * @param u the second element
     * @param v the third element
     * @return the created object
     */
    @NotNull
    static <T, U, V> Triple<T, U, V> of(T t, U u, V v) {
        return new TripleImpl<>(t, u, v);
    }

    /**
     * Creates a new 4-tuple.
     *
     * @param t the first element
     * @param u the second element
     * @param v the third element
     * @param w the fourth element
     * @return the created object
     */
    @NotNull
    static <T, U, V, W> Quadruple<T, U, V, W> of(T t, U u, V v, W w) {
        return new QuadrupleImpl<>(t, u, v, w);
    }

    /**
     * Creates a stream of this tuple's elements.
     *
     * @return the stream
     */
    @NotNull
    @Contract("-> new")
    default Stream<E> stream() {
        return StreamSupport.stream(spliterator(), false);
    }

    default int size() {
        return (int) stream().count();
    }

    /**
     * Creates an immutable list of this tuple's elements.
     *
     * @return the list
     */
    @Contract("-> new")
    @NotNull
    default List<E> asList() {
        return stream().toList();
    }

    /**
     * Creates an immutable set of this tuple's elements.
     *
     * @return the set
     */
    @Contract("-> new")
    @NotNull
    default Set<E> asSet() {
        return stream().collect(Collectors.toUnmodifiableSet());
    }
}
