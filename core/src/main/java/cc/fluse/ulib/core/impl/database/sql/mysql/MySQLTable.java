package cc.fluse.ulib.core.impl.database.sql.mysql;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;
import lombok.SneakyThrows;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public final class MySQLTable extends AbstractSqlTable {

    public MySQLTable(AbstractSqlDatabase sql, String name, Collection<Column<?>> columns) {
        super(sql, name, columns);
    }

    @SneakyThrows
    @Override
    public boolean exists() {
        try (var e = sql.getConnection();
             var st = e.get().prepareStatement("select count(*) from information_schema.tables where table_schema = database() AND table_name = ?")) {
            st.setString(1, name);
            var res = st.executeQuery();
            return res.next() && res.getInt("count(*)") > 0;
        }
    }

    @Override
    public void create() throws SQLException {
        super.create(
                Collections.emptyMap(),
                Collections.emptyMap(),
                Map.of(
                        "isIndexBeforeAutoIncrement", false
                        // autoIncrementKeyword is already "auto_increment" by default
                ),
                Collections.emptyMap()
        );
    }
}
