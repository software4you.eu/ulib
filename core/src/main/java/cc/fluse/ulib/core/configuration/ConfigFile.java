package cc.fluse.ulib.core.configuration;

import cc.fluse.ulib.core.file.FilesystemResource;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.configuration.FileBackedConfiguration;
import cc.fluse.ulib.core.io.ReadWrite;
import cc.fluse.ulib.core.io.Readable;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.file.Path;
import java.util.Optional;
import java.util.function.Function;

/**
 * A configuration backed by a file.
 */
public interface ConfigFile extends Configuration {

    /**
     * Loads a yaml configuration backed instance from a certain path.
     *
     * @param path the path to load the data from
     * @return the config file instance
     */
    @NotNull
    static ConfigFile yaml(@NotNull Path path) throws IOException {
        return of(path, YamlConfiguration::loadYaml);
    }

    /**
     * Loads a yaml configuration backed instance from a certain path and upgrades it if necessary.
     *
     * @param path       the path to load the data from
     * @param bundled    the bundled configuration
     * @param versionKey the key to extract the version from
     * @return the config file instance
     * @throws IOException if an I/O error occurs
     * @see #of(Path, URL, KeyPath, ParamFunc)
     */
    static @NotNull ConfigFile versionedYaml(@NotNull Path path, @NotNull URL bundled, @NotNull KeyPath versionKey) throws IOException {
        return of(path, bundled, versionKey, YamlConfiguration::loadYaml);
    }

    /**
     * Loads a json configuration backed instance from a certain path.
     *
     * @param path the path to load the data from
     * @return the config file instance
     */
    @NotNull
    static ConfigFile json(@NotNull Path path) throws IOException {
        return of(path, JsonConfiguration::loadJson);
    }

    /**
     * Loads a json configuration backed instance from a certain path and upgrades it if necessary.
     *
     * @param path       the path to load the data from
     * @param bundled    the bundled configuration
     * @param versionKey the key to extract the version from
     * @return the config file instance
     * @throws IOException if an I/O error occurs
     * @see #of(Path, URL, KeyPath, ParamFunc)
     */
    static @NotNull ConfigFile versionedJson(@NotNull Path path, @NotNull URL bundled, @NotNull KeyPath versionKey) throws IOException {
        return of(path, bundled, versionKey, JsonConfiguration::loadJson);
    }

    /**
     * Constructs a custom configuration backed instance.
     *
     * @param path        the path to load the data from
     * @param initializer a constructing function, loading the configuration from the path
     * @return the config file instance
     */
    @NotNull
    static <T extends ReloadableConfiguration & DumpableConfiguration, X extends Exception> ConfigFile
    of(@NotNull Path path, @NotNull ParamFunc<? super @NotNull Reader, @NotNull T, X> initializer) throws X, IOException {
        return new FileBackedConfiguration(FilesystemResource.of(path), initializer);
    }

    /**
     * Constructs a custom configuration backed instance. If the file does not exist, it will be created and filled with
     * the bundled resource. If it does exist, it will be loaded from the file. The version numbers of the bundled and
     * the loaded configuration will be extracted using the given function. If the version number of the bundled
     * configuration is higher than the loaded one, the loaded configuration will be updated to the bundled one: Keys
     * not present in the newer version will be removed, keys present in both versions will be kept, and keys only
     * present in the newer version will be added with their default values.
     *
     * @param path          the configuration file path
     * @param bundled       the bundled configuration resource
     * @param versionNumber a key path to the version number
     * @param initializer   a function initializing the configuration from a reader
     * @param <T>           the configuration type
     * @param <X>           the exception thrown by the version number extractor
     * @param <X2>          the exception thrown by the initializer
     * @return the config file instance
     * @throws X  any exception thrown by the version number extractor
     * @throws X2 any exception thrown by the initializer
     */
    static <T extends ReloadableConfiguration & DumpableConfiguration, X extends Exception, X2 extends Exception> @NotNull ConfigFile
    of(@NotNull Path path, @NotNull URL bundled, @NotNull KeyPath versionNumber,
       @NotNull ParamFunc<? super @NotNull Reader, @NotNull T, X2> initializer) throws X, X2, IOException {
        return FileBackedConfiguration.versioned(FilesystemResource.of(path),
            Readable.of(() -> Channels.newChannel(bundled.openStream())),
            t -> t.int32(versionNumber).orElse(0),
            (t, ver) -> t.set(versionNumber, ver),
            initializer);
    }

    /**
     * Gets a string from the configuration or the system properties.
     *
     * @param key           the configuration key
     * @param propKey       the system property key
     * @param propPreferred whether the system property should be preferred
     * @return the string
     */
    default @NotNull Optional<String> propGet(@NotNull String key, @NotNull String propKey, boolean propPreferred) {
        return (propPreferred ? Optional.ofNullable(System.getProperty(propKey)) : string(key))
            .filter(s -> !s.isBlank())
            .or(() -> propPreferred ? string(key) : Optional.ofNullable(System.getProperty(propKey)))
            .filter(s -> !s.isBlank());
    }

    default <T> @NotNull Optional<T> propGet(@NotNull String key, @NotNull String propKey, boolean propPreferred,
                                             @NotNull Function<? super @NotNull String, @NotNull Expect<T, ?>> mapper) {
        return propGet(key, propKey, propPreferred).flatMap(s -> mapper.apply(s).toOptional());
    }

    /**
     * Gets a string from the configuration or the environment variables.
     *
     * @param key          the configuration key
     * @param envKey       the environment variable key
     * @param envPreferred whether the environment variable should be preferred
     * @return the string
     */
    default @NotNull Optional<String> envGet(@NotNull String key, @NotNull String envKey, boolean envPreferred) {
        return (envPreferred ? Optional.ofNullable(System.getenv(envKey)) : string(key))
            .filter(s -> !s.isBlank())
            .or(() -> envPreferred ? string(key) : Optional.ofNullable(System.getenv(envKey)))
            .filter(s -> !s.isBlank());
    }

    /**
     * @return the backing configuration
     */
    @NotNull
    Configuration getBacking();

    /**
     * @return the backing object
     */
    @NotNull
    ReadWrite getBackingResource();

    /**
     * Saves (dumps) the configuration to the path.
     *
     * @see DumpableConfiguration#dumpTo(Path)
     */
    void save() throws IOException;

    /**
     * Reloads the configuration from the path.
     *
     * @see ReloadableConfiguration#reloadFrom(Path)
     */
    void reload() throws IOException;
}
