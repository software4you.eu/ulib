package cc.fluse.ulib.core.impl.value;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.tuple.Value;
import org.jetbrains.annotations.Nullable;

public class NoSetLazyValue<T> extends LazyValueImpl<T> {
    public NoSetLazyValue(@Nullable Value<? extends @Nullable T> value, @Nullable Func<? extends @Nullable T, ?> fetch, @Nullable ParamTask<? super @Nullable T, ?> push) {
        super(value, fetch, push);
    }

    @Override
    public T set(T val) {
        throw new UnsupportedOperationException();
    }
}
