package cc.fluse.ulib.core.impl.database.sql.orm;

import cc.fluse.ulib.core.database.sql.orm.Serializer;
import cc.fluse.ulib.core.function.ParamFunc;
import lombok.Getter;

public class SimpleSerializer<T, S> implements Serializer<T, S> {

    @Getter
    private final Class<T> javaType;
    @Getter
    private final Class<S> serializedType;

    private final ParamFunc<? super T, ? extends S, ? extends SerializerException> serializer;
    private final ParamFunc<? super S, ? extends T, ? extends SerializerException> deserializer;

    public SimpleSerializer(Class<T> javaType, Class<S> serializedType,
                            ParamFunc<? super T, ? extends S, ? extends SerializerException> serializer,
                            ParamFunc<? super S, ? extends T, ? extends SerializerException> deserializer) {
        this.javaType = javaType;
        this.serializedType = serializedType;
        this.serializer = serializer;
        this.deserializer = deserializer;
    }

    @Override
    public S serialize(T object) throws SerializerException {
        if (object == null) return null;

        if (!javaType.isInstance(object))
            throw new IllegalArgumentException("Object %s is not of type %s".formatted(object, javaType));

        var s = serializer.execute(object);
        if (!serializedType.isInstance(s))
            throw new SerializerException("Serializer returned object %s of type %s, but expected type %s".formatted(s, s.getClass(), serializedType));

        return s;
    }

    @Override
    public T deserialize(S serialized) throws SerializerException {
        if (serialized == null) return null;

        if (!serializedType.isInstance(serialized))
            throw new IllegalArgumentException("Serialized object %s is not of type %s".formatted(serialized, serializedType));

        var t = deserializer.execute(serialized);
        if (!javaType.isInstance(t))
            throw new SerializerException("Deserializer returned object %s of type %s, but expected type %s".formatted(t, t.getClass(), javaType));

        return t;
    }
}
