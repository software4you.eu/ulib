package cc.fluse.ulib.core.reflect.ref;

import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * Method reference
 */
public interface MthRef extends Ref<Method> {

    @NotNull
    ClsRef getDeclaringClass();

    @NotNull
    ClsRef getReturnType();

    @NotNull
    ClsRef @NotNull [] getParameterTypes();

    @Override
    default @NotNull Expect<Method, ?> tryLoad() {
        return getDeclaringClass()
                .tryLoad()
                .map(cls -> cls.getDeclaredMethod(getName(), Arrays.stream(getParameterTypes())
                                                                   .map(ClsRef::tryLoad)
                                                                   .map(Expect::orElseRethrowRE)
                                                                   .toArray(Class[]::new)));
    }
}
