package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Writable;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.nio.charset.Charset;

public class ByteArrayDataOutputStream extends DataOutputStream implements Writable {
    private final ByteArrayOutputStream bout;

    /**
     * @see ByteArrayOutputStream#ByteArrayOutputStream() ByteArrayOutputStream()
     */
    public ByteArrayDataOutputStream() {
        super(new ByteArrayOutputStream());
        this.bout = (ByteArrayOutputStream) super.out;
    }

    /**
     * @see ByteArrayOutputStream#writeTo(OutputStream)
     */
    public void writeTo(@NotNull OutputStream out) throws IOException {
        bout.writeTo(out);
    }

    /**
     * @see ByteArrayOutputStream#reset()
     */
    public void reset() {
        bout.reset();
    }

    /**
     * @see ByteArrayOutputStream#toByteArray()
     */
    public byte[] toByteArray() {
        return bout.toByteArray();
    }

    /**
     * @see ByteArrayOutputStream#toString()
     */
    @NotNull
    public String toString() {
        return bout.toString();
    }

    /**
     * @see ByteArrayOutputStream#toString(String)
     */
    @NotNull
    public String toString(@NotNull String charsetName) throws UnsupportedEncodingException {
        return bout.toString(charsetName);
    }

    /**
     * @see ByteArrayOutputStream#toString(Charset)
     */
    @NotNull
    public String toString(@NotNull Charset charset) {
        return bout.toString(charset);
    }

    @Override
    public @NotNull OutputStream streamWrite() {
        return IOUtil.isolate(this);
    }

    @Override
    public @NotNull WritableByteChannel channel() throws IOException {
        return IOUtil.isolate(Channels.newChannel(this));
    }
}
