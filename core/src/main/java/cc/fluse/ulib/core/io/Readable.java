package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamFunc;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.concurrent.CompletableFuture;

/**
 * Represents something that can be read from.
 *
 * @implSpec Specific characteristics for the methods have to be defined by the implementing class.
 */
public interface Readable {

    /**
     * Creates a new {@link Readable} object wrapping the given channel. Each opened stream or channel is backed by the
     * given channel, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source channel.
     *
     * @param channel the channel to wrap
     * @return a new {@link Readable} object wrapping the given channel
     */
    static @NotNull Readable of(@NotNull ReadableByteChannel channel) {
        return () -> IOUtil.isolate(channel);
    }

    /**
     * Creates a new {@link Readable} object sourcing each opened stream or channel from the given channel supplier.
     *
     * @param channelSupplier the channel supplier
     * @return a new {@link Readable} object
     */
    static @NotNull Readable of(@NotNull Func<? extends @NotNull ReadableByteChannel, IOException> channelSupplier) {
        return channelSupplier::execute;
    }

    /**
     * Creates a new {@link Readable} object wrapping the given stream. Each opened stream or channel is backed by the
     * given stream, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source stream.
     *
     * @param stream the stream to wrap
     * @return a new {@link Readable} object wrapping the given stream
     */
    static @NotNull Readable of(@NotNull InputStream stream) {
        return of(Channels.newChannel(stream));
    }


    /**
     * Opens a stream to read contents from the object. The stream is not closed automatically, closure has to be
     * handled by the caller.
     *
     * @return a stream reading the object's contents
     * @implSpec Specific characteristics for the stream have to be defined by the implementing class.
     */
    @NotNull
    default InputStream streamRead() throws IOException {
        return Channels.newInputStream(channelRead());
    }

    /**
     * Opens a stream to read contents from the object and passes it to the given reader function, which is executed
     * asynchronously. The reader function's result is returned as a future object. The stream is closed automatically.
     *
     * @param reader a function reading the object's contents as a stream and mapping them into an arbitrary object
     * @param <R>    the return type of the reader function
     * @return a future object wrapping the reader function's result
     */
    @NotNull
    default <R> CompletableFuture<R> streamRead(@NotNull ParamFunc<? super @NotNull InputStream, R, ?> reader) {
        try {
            return CompletableFuture.completedFuture(streamRead()).thenApplyAsync(ParamFunc.as(in -> {
                try (in) {
                    return reader.execute(in);
                }
            }));
        } catch (IOException e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    /**
     * Same as {@link #channel()}.
     *
     * @see #channel()
     */
    @NotNull
    default ReadableByteChannel channelRead() throws IOException {
        return channel();
    }

    /**
     * Opens a new channel to read contents from the object. The channel is not closed automatically, closure has to be
     * handled by the caller.
     *
     * @return the channel
     * @implSpec Specific characteristics for the channel have to be defined by the implementing class. It is advised to
     * consider isolating the new channel if the object itself is backed by a channel and not an object that can be
     * opened (i.e. a file)
     */
    @NotNull ReadableByteChannel channel() throws IOException;


    /**
     * Opens a channel to read contents from the object and passes it to the given reader function, which is executed
     * asynchronously. The reader function's result is returned as a future object. The channel is closed
     * automatically.
     *
     * @param reader a function reading the object's contents as a channel and mapping them into an arbitrary object
     * @param <R>    the return type of the reader function
     * @return a future object wrapping the reader function's result
     */
    @NotNull
    default <R> CompletableFuture<R> channelRead(@NotNull ParamFunc<? super @NotNull ReadableByteChannel, R, ?> reader) {
        try {
            return CompletableFuture.completedFuture(channelRead()).thenApplyAsync(ParamFunc.as(ch -> {
                try (ch) {
                    return reader.execute(ch);
                }
            }));
        } catch (IOException e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    /**
     * Reads all contents from the object and returns them as a read-only byte buffer. Whether the buffer is direct or
     * not, is implementation specific.
     *
     * @return a future object wrapping the read-only byte buffer
     */
    @NotNull
    default CompletableFuture<@NotNull ByteBuffer> readAll() {
        return channelRead(IOUtil::readAll);
    }
}
