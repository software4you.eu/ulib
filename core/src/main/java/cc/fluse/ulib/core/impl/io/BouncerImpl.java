package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.io.Bouncer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.nio.channels.ClosedChannelException;
import java.util.function.Supplier;

public class BouncerImpl<X extends Throwable> implements Bouncer<X> {

    public static final class ChBouncerImpl extends BouncerImpl<ClosedChannelException> implements ChBouncer {
        public ChBouncerImpl(@Nullable Object $lock) {
            super(ClosedChannelException::new, $lock);
        }
    }

    public static final class IOBouncerImpl extends BouncerImpl<IOException> implements IOBouncer {
        public IOBouncerImpl(@Nullable Object $lock) {
            this($lock, "closed");
        }

        public IOBouncerImpl(@Nullable Object $lock, @NotNull String msg) {
            super(() -> new IOException(msg), $lock);
        }
    }

    public static final class StateBouncerImpl extends BouncerImpl<IllegalStateException> implements StateBouncer {
        public StateBouncerImpl(@Nullable Object $lock) {
            this($lock, "closed");
        }

        public StateBouncerImpl(@Nullable Object $lock, @NotNull String msg) {
            super(() -> new IllegalStateException(msg), $lock);
        }
    }

    private final Supplier<? extends X> exceptionSupplier;
    private final Object $lock;
    private volatile boolean open = true;

    public BouncerImpl(@NotNull Supplier<? extends @NotNull X> exceptionSupplier, @Nullable Object $lock) {
        this.exceptionSupplier = exceptionSupplier;
        this.$lock = $lock == null ? this : $lock;
    }

    public boolean canPass() {
        synchronized ($lock) {
            return open;
        }
    }

    public void block() {
        synchronized ($lock) {
            open = false;
        }
    }

    @Override
    public void unblock() {
        synchronized ($lock) {
            open = true;
        }
    }

    public <XX extends Throwable> void pass(@NotNull Supplier<? extends XX> exceptionSupplier) throws XX {
        synchronized ($lock) {
            if (!open) throw exceptionSupplier.get();
        }
    }

    public void pass() throws X {
        synchronized ($lock) {
            if (!open) throw exceptionSupplier.get();
        }
    }


    @Override
    public void close() {
        synchronized ($lock) {
            Bouncer.super.close();
        }
    }

    public <T, XX extends Exception> T withCoverage(@NotNull Func<T, ? extends XX> task) throws X, XX {
        synchronized ($lock) {
            pass();
            return task.execute();
        }
    }
}