package cc.fluse.ulib.core.configuration;

import cc.fluse.ulib.core.impl.configuration.yaml.YamlSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;

/**
 * Representation of a YAML-type configuration sub.
 */
public interface YamlConfiguration extends ReloadableConfiguration, DumpableConfiguration, CommentSupportingConfiguration {
    /**
     * Creates a new empty YAML-Typed {@link Configuration}.
     *
     * @return the newly created sub
     */
    @NotNull
    static YamlConfiguration newYaml() {
        return YamlSerializer.getInstance().createNew();
    }

    /**
     * Loads a YAML-Typed {@link Configuration} from a reader.
     *
     * @param reader the YAML document
     * @return the loaded sub
     */
    @NotNull
    static YamlConfiguration loadYaml(@NotNull Reader reader) throws IOException {
        return YamlSerializer.getInstance().deserialize(reader);
    }

    /**
     * Loads a YAML-Typed {@link Configuration} from a stream.
     *
     * @param in the YAML document
     * @return the loaded sub
     */
    @NotNull
    static YamlConfiguration loadYaml(@NotNull InputStream in) throws IOException {
        return loadYaml(new InputStreamReader(in));
    }

    /**
     * Loads a YAML-Typed {@link Configuration} from a certain path.
     *
     * @param path the path to the YAML document
     * @return the loaded sub
     */
    @NotNull
    static YamlConfiguration loadYaml(@NotNull Path path) throws IOException {
        try (var reader = Files.newBufferedReader(path)) {
            return loadYaml(reader);
        }
    }

    @Override
    @NotNull YamlConfiguration getRoot();

    @Override
    @NotNull Optional<? extends YamlConfiguration> getSubsection(@NotNull KeyPath path);

    @Override
    @NotNull Collection<? extends YamlConfiguration> getSubsections(boolean deep);

    @Override
    @NotNull YamlConfiguration createSubsection(@NotNull KeyPath path);

    @Override
    @NotNull YamlConfiguration subAndCreate(@NotNull KeyPath path);
}
