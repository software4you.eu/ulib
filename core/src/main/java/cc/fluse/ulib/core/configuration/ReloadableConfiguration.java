package cc.fluse.ulib.core.configuration;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Represents a generic configuration that can be reloaded with different data.
 */
public interface ReloadableConfiguration extends Configuration {
    /**
     * Clears all data from the configuration and loads in new data read from a reader.
     *
     * @param reader the data
     */
    void reload(@NotNull Reader reader) throws IOException;

    /**
     * Clears all data from the configuration and loads in new data read from a stream.
     *
     * @param in the data stream
     */
    default void reload(@NotNull InputStream in) throws IOException {
        reload(new InputStreamReader(in));
    }

    default void reload(@NotNull ReadableByteChannel ch) throws IOException {
        reload(Channels.newReader(ch, Charset.defaultCharset()));
    }

    /**
     * Clears all data from the configuration and loads in new data read from a path.
     *
     * @param path the path to read the data from
     */
    default void reloadFrom(@NotNull Path path) throws IOException {
        try (var reader = Files.newBufferedReader(path)) {
            reload(reader);
        }
    }
}
