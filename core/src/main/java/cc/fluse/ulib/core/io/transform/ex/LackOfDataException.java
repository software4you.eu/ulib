package cc.fluse.ulib.core.io.transform.ex;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import lombok.experimental.StandardException;

/**
 * An exception indicating that the transform has not yet enough data (bytes) to successfully process the current
 * buffer.
 */
@StandardException
@BypassAnnotationEnforcement
public final class LackOfDataException extends IOTransformException {
}
