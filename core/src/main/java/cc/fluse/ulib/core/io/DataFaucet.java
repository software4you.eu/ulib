package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.Range;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SeekableByteChannel;
import java.util.Objects;


/**
 * A {@link DataInput} with default implementations for all methods.
 */
public interface DataFaucet extends DataInput {

    /**
     * @see java.nio.channels.ReadableByteChannel#read(ByteBuffer)
     */
    int read(@NotNull ByteBuffer buffer) throws IOException;

    /**
     * @see InputStream#read()
     */
    default @Range(from = -1, to = 255) int read() throws IOException {
        try (var e = Pool.BYTE_BUFFER_SINGLE_POOL.obtain()) {
            int c = read(e.get());
            return c < 0 ? -1 : e.get().get(0) & 0xFF;
        }
    }

    /**
     * @see InputStream#read(byte[])
     */
    default int read(byte[] b) throws IOException {
        return read(b, 0, b.length);
    }

    /**
     * @see InputStream#read(byte[], int, int)
     */
    default int read(byte[] b, int off, int len) throws IOException {
        Objects.checkFromIndexSize(off, len, b.length);
        return read(ByteBuffer.wrap(b, off, len));
    }

    @Override
    default void readFully(byte @NotNull [] b) throws IOException {
        readFully(b, 0, b.length);
    }

    @Override
    default void readFully(byte @NotNull [] b, int off, int len) throws IOException {
        Objects.checkFromIndexSize(off, len, b.length);
        var buf = ByteBuffer.wrap(b, off, len);
        int n = 0;
        while (n < len) {
            int c = read(buf);
            if (c < 0) throw new EOFException();
            n += c;
        }
    }

    /**
     * Reads bytes from this channel into the given buffer until the buffer is full. If the channel reaches EOF before
     * the buffer is full, an {@link EOFException} is thrown.
     *
     * @param buf the buffer to read into
     * @throws IOException if an I/O error occurs
     */
    default void readFully(@NotNull ByteBuffer buf) throws IOException {
        while (buf.hasRemaining()) {
            int c = read(buf);
            if (c < 0) throw new EOFException();
        }
    }

    @Override
    default int skipBytes(int n) throws IOException {
        if (n <= 0) return 0;

        // try to skip directly if possible
        if (this instanceof SeekableByteChannel seekable) {
            long pos = seekable.position();
            long size = seekable.size();
            if (pos + n > size) {
                n = (int) (size - pos);
            }
            seekable.position(pos + n);
            return n;
        }

        // otherwise, read and discard
        try (var e = Pool.BYTE_BUFFER_POOL.obtain()) {
            var buf = e.get();

            int skipped = 0;
            while (skipped < n) {
                int c = read(buf.clear().limit(Math.min(n - skipped, buf.capacity())));
                if (c < 0) break;
                skipped += c;
            }
            return skipped;
        }
    }

    @Override
    default boolean readBoolean() throws IOException {
        int i = read();
        if (i < 0) throw new EOFException();
        return i != 0;
    }

    @Override
    default byte readByte() throws IOException {
        int i = read();
        if (i < 0) throw new EOFException();
        return (byte) i;
    }

    @Override
    default @Range(from = 0, to = 255) int readUnsignedByte() throws IOException {
        return Byte.toUnsignedInt(readByte());
    }

    @Override
    default short readShort() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(2));
            if (c < 0) throw new EOFException();
            return e.get().getShort(0);
        }
    }

    @Override
    default int readUnsignedShort() throws IOException {
        return Short.toUnsignedInt(readShort());
    }

    @Override
    default char readChar() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(2));
            if (c < 0) throw new EOFException();
            return e.get().getChar(0);
        }
    }

    @Override
    default int readInt() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(4));
            if (c < 0) throw new EOFException();
            return e.get().getInt(0);
        }
    }

    @Override
    default long readLong() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(8));
            if (c < 0) throw new EOFException();
            return e.get().getLong(0);
        }
    }

    @Override
    default float readFloat() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(4));
            if (c < 0) throw new EOFException();
            return e.get().getFloat(0);
        }
    }

    @Override
    default double readDouble() throws IOException {
        try (var e = Pool.BYTE_BUFFER_8_POOL.obtain()) {
            int c = read(e.get().limit(8));
            if (c < 0) throw new EOFException();
            return e.get().getDouble(0);
        }
    }

    @Override
    @Deprecated
    default @Nullable String readLine() throws IOException {
        var buf = new StringBuilder();
        int c;
        while ((c = read()) != -1 && c != '\n') {
            if (c != '\r') buf.append((char) c);
        }
        return buf.length() == 0 && c == -1 ? null : buf.toString();
    }

    @NotNull
    @Override
    default String readUTF() throws IOException {
        return DataInputStream.readUTF(this);
    }

}
