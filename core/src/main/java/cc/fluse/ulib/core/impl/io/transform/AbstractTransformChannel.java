package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.transform.ByteTransformer;
import cc.fluse.ulib.core.io.transform.TransformerChainChannel;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.util.LinkedList;

import static cc.fluse.ulib.core.util.Semantics.nofail;

public abstract class AbstractTransformChannel implements TransformerChainChannel {

    protected final Bouncer.ChBouncer bouncer = Bouncer.ch();
    protected final Bouncer.StateBouncer completion = Bouncer.is("completed");
    protected final LinkedList<ByteTransformer> chain = new LinkedList<>();

    @Override
    public void addTransformer(@NotNull ByteTransformer transformer) {
        bouncer.pass(() -> new IllegalStateException("closed"));
        completion.pass();
        if (chain.contains(transformer)) return;

        var prev = chain.peekLast();
        chain.add(transformer);

        if (prev instanceof NopTransformer) {
            nofail(() -> process());
            chain.remove(prev);
        }
    }

    @Override
    public void removeTransformer(@NotNull ByteTransformer transformer) {
        bouncer.pass(() -> new IllegalStateException("closed"));
        completion.pass();
        chain.remove(transformer);
        // TODO: close transformer and update next one with result?
    }

    @Override
    public boolean hasTransformer(@NotNull ByteTransformer transformer) {
        return chain.contains(transformer);
    }

    /**
     * Writes data to the head of the chain and processes it.
     *
     * @param src the data to write
     * @return the number of bytes written
     * @throws InputFormatException   if processing fails
     * @throws ClosedChannelException if the channel is closed
     */
    protected int push(@NotNull ByteBuffer src) throws InputFormatException, ClosedChannelException {
        bouncer.pass();
        completion.pass();

        if (chain.isEmpty()) {
            addTransformer(new NopTransformer());
        }

        // write to head of chain
        int i = chain.getFirst().write(src);
        process();
        return i;
    }

    /**
     * Reads data from the tail of the chain.
     *
     * @param dst the buffer to read into
     * @return the number of bytes read
     * @throws ClosedChannelException if the channel is closed
     */
    protected int pull(@NotNull ByteBuffer dst) throws ClosedChannelException {
        bouncer.pass();
        if (chain.isEmpty()) return -1;

        // only propagate EOF if chain is completed
        var r = chain.getLast().read(dst);
        return r != -1 ? r : isCompleted() ? -1 : 0;
    }

    /**
     * Processes the chain, sending the data through it. Does not complete processing.
     *
     * @throws ClosedChannelException if the channel is closed
     * @throws InputFormatException   if processing fails
     */
    protected void process() throws ClosedChannelException, InputFormatException {
        try {
            process(false);
        } catch (IOTransformException e) {
            if (e instanceof InputFormatException ife) throw ife;
            throw new UndefinedStateError(e);
        }
    }

    /**
     * Processes the chain, sending the data through it. Optionally also completes processing.
     *
     * @param complete whether to complete processing after sending data through
     * @throws ClosedChannelException if the channel is closed
     * @throws IOTransformException   if processing fails
     */
    private void process(boolean complete) throws ClosedChannelException, IOTransformException {
        bouncer.pass();
        completion.pass();
        if (chain.isEmpty()) return; // NOP on empty chain or single element

        var it = chain.iterator();
        var prev = it.next();

        // (no alloc if only 1 elem in chain)
        var pbuf = it.hasNext() ? Pool.BYTE_BUFFER_DEFAULT_POOL.obtain() : null;
        var buf = pbuf != null ? pbuf.get() : null;

        try {
            while (it.hasNext()) {
                var elem = it.next();

                if (complete) prev.complete();

                // transfer from previous to current
                while ((prev.read(buf.clear())) > 0) {
                    elem.write(buf.flip());
                }

                // update previous
                prev = elem;
            }
        } finally {
            if (pbuf != null) pbuf.release();
        }

        if (complete) {
            prev.complete(); // complete last
            completion.block();
        }
    }

    /**
     * Completes processing.
     *
     * @throws IOTransformException   if processing fails
     * @throws ClosedChannelException if the channel is closed
     */
    @Override
    public void complete() throws IOTransformException, ClosedChannelException {
        process(true);
    }

    public boolean isCompleted() {
        return !completion.canPass();
    }

    @Override
    public boolean isOpen() {
        return bouncer.canPass();
    }

    /**
     * Closes the chain.
     * <p>
     * <b>Note:</b> This method never throws an exception, despite the declaration.
     */
    @Override
    @MustBeInvokedByOverriders
    public void close() throws IOException {
        if (!bouncer.canPass()) return;
        chain.clear();
        bouncer.block();
    }

    protected boolean holdsData() throws ClosedChannelException {
        bouncer.pass();
        for (var proc : chain) {
            if (proc.holdsData()) return true;
        }
        return false;
    }
}
