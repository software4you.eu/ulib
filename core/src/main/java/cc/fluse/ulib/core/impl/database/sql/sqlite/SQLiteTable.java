package cc.fluse.ulib.core.impl.database.sql.sqlite;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;
import lombok.SneakyThrows;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

final class SQLiteTable extends AbstractSqlTable {
    SQLiteTable(AbstractSqlDatabase sql, String name, Collection<Column<?>> columns) {
        super(sql, name, columns);
    }

    @SneakyThrows
    @Override
    public boolean exists() {
        try (var e = sql.getConnection();
             var st = e.get().prepareStatement("select count(*) from sqlite_master where type = 'table' and name = ?")) {
            st.setString(1, name);
            var res = st.executeQuery();
            return res.next() && res.getInt("count(*)") > 0;
        }
    }

    @Override
    public void create() throws SQLException {
        super.create(
                Collections.emptyMap(),
                Collections.emptyMap(),
                Map.of(
                        // isIndexBeforeAutoIncrement is already true by default
                        "autoIncrementKeyword", "autoincrement"
                ),
                Collections.emptyMap()
        );
    }
}
