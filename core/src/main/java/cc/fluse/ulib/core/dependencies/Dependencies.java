package cc.fluse.ulib.core.dependencies;

import cc.fluse.ulib.core.impl.maven.LocalRepository;
import cc.fluse.ulib.core.inject.InjectUtil;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.tuple.BiMap;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Access point for resolving maven artifacts.
 *
 * @see Repository#of(String)
 */
public final class Dependencies {

    /**
     * Converts maven coords in the format {@code groupId:artifactId:version[:classifier]} to the format
     * {@code groupId:artifactId[:extension[:classifier]]:version}.
     *
     * @param gradleCoords the coords
     * @return the converted coords
     */
    @NotNull
    public static String gradleToMaven(@NotNull String gradleCoords) {
        var c = gradleCoords.split(":");
        return switch (c.length) {
            case 3 -> gradleCoords;
            case 4 -> "%s:%s::%s:%s".formatted(c[0], c[1], c[3], c[2]);
            default -> throw new IllegalArgumentException(
                    "'%s' not in format groupId:artifactId:version[:classifier]".formatted(gradleCoords));
        };
    }

    // - resolve -

    /**
     * Resolves a maven artifact with all its transitive dependencies and downloads them to the local repository.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @param exclusions   The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull String coords, @NotNull Collection<Repository> repositories,
                                                @NotNull Collection<String> exclusions) {
        return LocalRepository.resolve(coords, repositories, exclusions);
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies and downloads them to the local repository.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull String coords, @NotNull Collection<Repository> repositories) {
        return resolve(coords, repositories, Collections.emptyList());
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies and downloads them to the local repository.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @param exclusions The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull String coords, @NotNull Repository repository, @NotNull Collection<String> exclusions) {
        return resolve(coords, toColl(repository), exclusions);
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies and downloads them to the local repository.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull String coords, @NotNull Repository repository) {
        return resolve(coords, toColl(repository));
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @param exclusions   The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull Collection<String> coords, @NotNull Collection<Repository> repositories,
                                                @NotNull Collection<String> exclusions) {
        return coords.stream()
                .flatMap(c -> resolve(c, repositories, exclusions))
                .distinct();
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull Collection<String> coords, @NotNull Collection<Repository> repositories) {
        return resolve(coords, repositories, Collections.emptyList());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @param exclusions The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull Collection<String> coords, @NotNull Repository repository, @NotNull Collection<String> exclusions) {
        return resolve(coords, toColl(repository), exclusions);
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull Collection<String> coords, @NotNull Repository repository) {
        return resolve(coords, toColl(repository));
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coordsRepositoriesExclusionsMap Map: maven coordinates -> Pair: repositories, exclusions. See
     *                                        {@link #resolve(String, Collection, Collection)}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull BiMap<String, Collection<Repository>, Collection<String>> coordsRepositoriesExclusionsMap) {
        return coordsRepositoriesExclusionsMap.stream()
                .flatMap(tri -> resolve(tri.getFirst(), tri.getSecond(), tri.getThird()))
                .distinct();
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies and downloads them to the local repository.
     *
     * @param coordsRepositoriesMap Map: maven coordinates -> repositories. See {@link #resolve(String, Collection)}.
     * @return A stream with the provided jar files.
     */
    public static @NotNull Stream<Path> resolve(@NotNull Map<String, Collection<Repository>> coordsRepositoriesMap) {
        return coordsRepositoriesMap.entrySet().stream()
                .flatMap(en -> resolve(en.getKey(), en.getValue()))
                .distinct();
    }

    // - require -

    /**
     * Resolves a maven artifact with all its transitive dependencies, downloads them to the local repository and tweaks
     * the class loader of the caller with the provided jar files.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @param exclusions   The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(String, Collection, Collection)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull String coords, @NotNull Collection<Repository> repositories, @NotNull Collection<String> exclusions) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repositories, exclusions).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies, downloads them to the local repository and tweaks
     * the class loader of the caller with the provided jar files.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifact in.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(String, Collection)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull String coords, @NotNull Collection<Repository> repositories) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repositories).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies, downloads them to the local repository and tweaks
     * the class loader of the caller with the provided jar files.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @param exclusions The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(String, Repository, Collection)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull String coords, @NotNull Repository repository, @NotNull Collection<String> exclusions) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repository, exclusions).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves a maven artifact with all its transitive dependencies, downloads them to the local repository and tweaks
     * the class loader of the caller with the provided jar files.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifact in.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(String, Repository)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull String coords, @NotNull Repository repository) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repository).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifacts in.
     * @param exclusions   The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @throws Exception If the class loader could not be tweaked.
     */
    public static void require(@NotNull Collection<String> coords, @NotNull Collection<Repository> repositories, @NotNull Collection<String> exclusions) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repositories, exclusions).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coords       The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repositories The repositories to search the artifacts in.
     * @throws Exception If the class loader could not be tweaked.
     */
    public static void require(@NotNull Collection<String> coords, @NotNull Collection<Repository> repositories) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repositories).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifacts in.
     * @param exclusions The exclusions to apply in format {@code groupId[:artifactId[:classifier[:extension]]]}.
     * @throws Exception If the class loader could not be tweaked.
     */
    public static void require(@NotNull Collection<String> coords, @NotNull Repository repository, @NotNull Collection<String> exclusions) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repository, exclusions).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coords     The maven coordinates in format {@code groupId:artifactId[:extension[:classifier]]:version}.
     * @param repository The repository to search the artifacts in.
     * @throws Exception If the class loader could not be tweaked.
     */
    public static void require(@NotNull Collection<String> coords, @NotNull Repository repository) throws Exception {
        InjectUtil.tweakLoader(resolve(coords, repository).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coordsRepositoriesExclusionsMap Map: maven coordinates -> Pair: repositories, exclusions. See
     *                                        {@link #resolve(String, Collection, Collection)}.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(BiMap)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull BiMap<String, Collection<Repository>, Collection<String>> coordsRepositoriesExclusionsMap) throws Exception {
        InjectUtil.tweakLoader(resolve(coordsRepositoriesExclusionsMap).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }

    /**
     * Resolves maven artifacts with all their transitive dependencies, downloads them to the local repository and
     * tweaks the class loader of the caller with the provided jar files.
     *
     * @param coordsRepositoriesMap Map: maven coordinates -> repositories. See {@link #resolve(String, Collection)}.
     * @throws Exception If the class loader could not be tweaked.
     * @see #resolve(Map)
     * @see InjectUtil#tweakLoader(Collection, ClassLoader)
     */
    public static void require(@NotNull Map<String, Collection<Repository>> coordsRepositoriesMap) throws Exception {
        InjectUtil.tweakLoader(resolve(coordsRepositoriesMap).collect(Collectors.toSet()), ReflectUtil.getCallerClass().getClassLoader());
    }


    private static Collection<Repository> toColl(Repository repository) {
        var central = Repository.mavenCentral();
        return central == repository ? Collections.singletonList(repository) : Arrays.asList(central, repository);
    }
}
