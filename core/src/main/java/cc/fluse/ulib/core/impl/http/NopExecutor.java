package cc.fluse.ulib.core.impl.http;

import cc.fluse.ulib.core.http.HttpRequestExecutor;
import org.jetbrains.annotations.NotNull;

import java.net.http.*;
import java.util.concurrent.CompletableFuture;

/**
 * A No-Operation executor that only delegates the request execution to a http client.
 */
public class NopExecutor implements HttpRequestExecutor {

    private final HttpClient client;

    /**
     * @param client the http client that should be used
     */
    public NopExecutor(HttpClient client) {
        this.client = client;
    }

    @Override
    public <T> @NotNull CompletableFuture<HttpResponse<T>> sendAsync(@NotNull HttpRequest request, HttpResponse.@NotNull BodyHandler<T> responseHandler) {
        return client.sendAsync(request, responseHandler);
    }

    @Override
    public String toString() {
        return "NopExecutor{" +
               "client=" + client +
               '}';
    }
}
