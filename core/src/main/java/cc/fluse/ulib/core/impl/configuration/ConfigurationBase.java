package cc.fluse.ulib.core.impl.configuration;

import cc.fluse.ulib.core.common.Keyable;
import cc.fluse.ulib.core.configuration.*;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.util.Conversions;
import cc.fluse.ulib.core.util.Expect;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.AbstractMap.SimpleEntry;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.*;

@SuppressWarnings("unchecked")
public abstract class ConfigurationBase<R extends ConfigurationBase<R>> implements Configuration, Keyable<String> {
    // key -> node (data or sub)
    protected final Map<String, Object> children = new LinkedHashMap<>();

    @Getter
    @NotNull
    private final R root, parent;
    @Getter
    @NotNull
    private final String key; // key of this sub

    // root constructor
    protected ConfigurationBase() {
        this.root = (R) this;
        this.parent = (R) this;
        this.key = "";
    }

    // child constructor
    protected ConfigurationBase(@NotNull R root, @NotNull R parent, @NotNull String key) {
        this.root = root;
        this.parent = parent;
        this.key = key;
    }

    // - direct data access -

    private Object _get(KeyPath path) {
        return get(path).orElse(null);
    }

    @Override
    @NotNull
    public Optional<Object> get(@NotNull KeyPath path) {
        return resolve(Objects.requireNonNull(path, "Path may not be null"))
                // pair: (doc, key)
                .map(pair -> pair.getFirst().children.get(pair.getSecond()));
    }

    @Override
    public @NotNull <T> Optional<T> get(@NotNull Class<T> clazz, @NotNull KeyPath path) {
        return get(path)
                .filter(clazz::isInstance)
                .map(clazz::cast);
    }

    @Override
    public @NotNull <T> Optional<List<T>> list(@NotNull Class<T> clazz, @NotNull KeyPath path) {
        return get(path)
                .map(val -> val.getClass().isArray() ? Arrays.asList((Object[]) val)
                                                     : ((val instanceof List<?> li) ? li : null))
                .filter(li -> li.stream().allMatch(clazz::isInstance))
                .map(li -> (List<T>) li);
    }

    @Override
    public @NotNull Optional<Boolean> bool(@NotNull KeyPath path) {
        return Conversions.tryBoolean(_get(path)).toOptional();
    }

    @Override
    public @NotNull Optional<Float> dec32(@NotNull KeyPath path) {
        return Conversions.tryFloat(_get(path)).toOptional();
    }

    @Override
    public @NotNull Optional<Double> dec64(@NotNull KeyPath path) {
        return Conversions.tryDouble(_get(path)).toOptional();
    }

    @Override
    public @NotNull Optional<Integer> int32(@NotNull KeyPath path) {
        return Conversions.tryInt(_get(path)).toOptional();
    }

    @Override
    public @NotNull Optional<Long> int64(@NotNull KeyPath path) {
        return Conversions.tryLong(_get(path)).toOptional();
    }

    @Override
    public @NotNull Collection<KeyPath> getKeys(boolean deep) {
        Set<KeyPath> keys = new LinkedHashSet<>();

        children.forEach((key, value) -> {

            // do not add a sub as key, only it's values
            if (deep && value instanceof ConfigurationBase<?> doc) {
                var prefix = KeyPath.of(key);

                doc.getKeys(true).stream()
                   .map(prefix::chain)
                   .forEach(keys::add);
            } else {
                keys.add(KeyPath.of(key));
            }

        });

        return keys;
    }

    @Override
    public @NotNull Map<KeyPath, Object> getValues(boolean deep) {
        Map<KeyPath, Object> values = new LinkedHashMap<>();

        children.forEach((key, value) -> {

            // do not add a sub as key, only it's values
            if (deep && value instanceof ConfigurationBase<?> doc) {
                var prefix = KeyPath.of(key);

                doc.getValues(true).forEach((k, v) -> values.put(prefix.chain(k), v));

            } else if (value instanceof Collection<?> coll) {
                var mappedVal = coll.stream()
                                    .map(obj -> obj instanceof Configuration c ? c.getValues(deep) : obj)
                                    .toList();
                values.put(KeyPath.of(key), mappedVal);
            } else {
                values.put(KeyPath.of(key), value);
            }

        });

        return values;
    }

    @Override
    public @NotNull Map<String, Object> graphUnpack(@Nullable Function<@NotNull Object, @Nullable Object> mapper) {
        return children.entrySet().stream()
                       .map(e -> new SimpleEntry<>(e.getKey(), unpackValue(e.getValue(), mapper)))
                       .filter(e -> e.getKey() != null && e.getValue() != null)
                       .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }

    @Nullable
    protected Object unpackValue(@NotNull Object obj, @Nullable Function<@NotNull Object, @Nullable Object> mapper) {

        // unpack collections
        if (obj instanceof Collection<?> c) {
            return c.stream()
                    .map(e -> unpackValue(e, mapper))
                    .filter(Objects::nonNull)
                    .toList();
        }

        // unpack maps
        if (obj instanceof Map<?, ?> m) {
            return m.entrySet().stream()
                    .map(e -> new SimpleEntry<>(e.getKey(), unpackValue(e.getValue(), mapper)))
                    .filter(e -> e.getKey() != null && e.getValue() != null)
                    .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        }

        // unpack configurations
        if (obj instanceof Section s) {
            return s.graphUnpack(mapper);
        }

        // unpack iterables
        if (obj instanceof Iterable<?> iter) {
            return StreamSupport.stream(iter.spliterator(), false)
                                .map(e -> unpackValue(e, mapper))
                                .filter(Objects::nonNull)
                                .toList();
        }

        return mapper != null ? mapper.apply(obj) : obj;
    }

    @Override
    public boolean isEmpty() {
        return children.isEmpty();
    }

    @Override
    public void set(@NotNull KeyPath path, Object value) {
        Pair<R, String> r;
        if (value != null) {
            r = resolveFull(path);
        } else {
            var op = resolve(path);
            if (op.isEmpty()) {
                return;
            }
            r = op.get();
        }

        var doc = r.getFirst();
        var key = r.getSecond();
        var isActuallyNew = !doc.children.containsKey(key);
        if (value == null) {
            doc.children.remove(key);
        } else {
            doc.children.put(key, value);
        }
        doc.placedNewValue(key, value, isActuallyNew);
    }

    // - sub access -

    @Override
    @NotNull
    public Optional<R> getSubsection(@NotNull KeyPath path) {
        return get(path)
                .filter(ConfigurationBase.class::isInstance)
                .map(sub -> Expect.compute(() -> (R) sub).getValue());
    }

    @Override
    public @NotNull R createSubsection(@NotNull KeyPath path) {
        var r = resolveFull(path);
        return ((ConfigurationBase<R>) r.getFirst()).putNewSub(r.getSecond());
    }

    @Override
    public @NotNull R subAndCreate(@NotNull KeyPath path) {
        var opSub = getSubsection(path);
        return opSub.orElseGet(() -> createSubsection(path));
    }

    @Override
    public @NotNull Collection<R> getSubsections(boolean deep) {
        return children.values().stream()
                       .filter(ConfigurationBase.class::isInstance)
                       .map(sub -> (R) sub)
                       .flatMap(sub -> deep ? sub.getSubsections(true).stream() : Stream.of(sub))
                       .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public void converge(@NotNull Section base, boolean strict, boolean deep) {
        // set default values
        base.getValues(deep).forEach((k, v) -> {
            if (contains(k)) {
                return;
            }
            set(k, v);
            convergeSet(base, k, v);
        });

        if (!strict) {
            return;
        }

        // remove non-present values
        getKeys(deep).forEach(k -> {
            if (!base.contains(k)) {
                set(k, null);
            }
        });
    }

    @Override
    public void purge(boolean deep) {
        var st = getSubsections(false).stream();
        (deep ? st.peek(s -> s.purge(true)) : st)
                .filter(Configuration::isEmpty)
                .map(s -> ((Keyable<String>) s).getKey())
                .forEach(key -> {
                    children.remove(key);
                    placedNewValue(key, null, false);
                });

    }

    // - checks -

    @Override
    public boolean isSet(@NotNull KeyPath path) {
        return get(path)
                .map(obj -> !(obj instanceof ConfigurationBase))
                .orElse(false);
    }

    @Override
    public boolean contains(@NotNull KeyPath path) {
        return get(path).isPresent();
    }

    @Override
    public boolean isSubsection(@NotNull KeyPath path) {
        return get(path)
                .map(obj -> obj instanceof ConfigurationBase)
                .orElse(false);
    }

    @Override
    public boolean isRoot() {
        return this == root;
    }


    // - resolvers -

    // resolves the full path into a valid sub and key
    // creates new subs if necessary
    private Pair<R, String> resolveFull(final KeyPath path) {
        return resolve(path, true)
                .orElseThrow(() -> new IllegalStateException("Resolution failure"));
    }

    // attempts to resolve the full path into a valid sub and a key
    // if `createSub` is true, resolution success is guaranteed
    // note that the key is not guaranteed to exist in the sub
    // returns pair: (sub, key)
    protected final Optional<Pair<R, String>> resolve(final KeyPath path, boolean createSub) {

        ConfigurationBase<R> doc = this;
        String lastKey = "";

        var it = path.iterator();
        while (it.hasNext()) {
            String key = it.next();
            if (!it.hasNext()) {
                lastKey = key;
                break;
            }

            if (doc.children.get(key) instanceof ConfigurationBase<?> cb && cb.getClass() == getClass()) {
                doc = (ConfigurationBase<R>) cb;
                // sub does exist, continue with that
                continue;
            }

            // sub does not exist
            if (!createSub) {
                // not allowed to create a new sub or overwrite existing value with one
                // indicate failure
                return Optional.empty();
            }

            // create/overwrite
            doc = doc.putNewSub(key);
        }

        // successfully resolved
        return Optional.of(Tuple.of((R) doc, lastKey));
    }

    // attempts to resolve the full path into a valid sub and a key
    // resolution success is not guaranteed
    // note that the key is not guaranteed to exist in the sub
    // returns pair: (sub, key)
    protected final Optional<Pair<R, String>> resolve(final KeyPath path) {
        return resolve(path, false);
    }

    // - misc -

    private R putNewSub(String key) {
        var sub = constructSub(key);
        children.put(key, sub);
        return sub;
    }

    // - abstraction -

    // constructs a new sub with the specified key
    protected abstract R constructSub(String key);

    // called when a new value has been placed at the specified key, while overwriting any other associated value
    // the key is not resolved
    // param genuinelyNew indicates if a value has been overwritten or is actually 'new'
    protected void placedNewValue(String key, Object value, boolean genuinelyNew) {
        // to be overwritten
    }

    // called when a value is set by a convergence
    protected void convergeSet(Section base, KeyPath path, Object value) {
        // to be overwritten
    }

}
