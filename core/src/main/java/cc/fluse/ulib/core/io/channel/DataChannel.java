package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.impl.io.datachannel.DataChannelDelegate;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.ReadWrite;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.channels.ByteChannel;

/**
 * A {@link ByteChannel} that also implements {@link ReadableDataChannel} and {@link WritableDataChannel}.
 * <p>
 * Calls to read and write methods on this channel will be forwarded to the underlying channel, and calls open channel
 * methods on {@link ReadWrite} will always return the same underlying channel.
 */
public interface DataChannel extends ByteChannel, ReadableDataChannel, WritableDataChannel, ReadWrite {

    /**
     * Wraps a {@link ByteChannel} in a {@link DataChannel}. If the channel is already a {@link DataChannel}, it is
     * returned as-is. Otherwise, a new {@link DataChannel} is created that delegates to the given channel.
     * <p>
     * The returned object is not guaranteed to be thread-safe.
     *
     * @param channel the channel to wrap
     * @return a {@link DataChannel} that delegates to the given channel
     */
    static @NotNull DataChannel wrap(@NotNull ByteChannel channel) {
        return channel instanceof DataChannel dch ? dch : new DataChannelDelegate(channel);
    }

    /**
     * Returns a new isolated channel that reads from and writes to this channel.
     *
     * @return the channel
     * @see IOUtil#isolate(ByteChannel)
     */
    @NotNull
    @Override
    default DataChannel channel() throws IOException {
        return IOUtil.isolate(this);
    }

    @Override
    @NotNull
    default ReadableDataChannel channelRead() throws IOException {
        return channel();
    }

    @Override
    @NotNull
    default WritableDataChannel channelWrite() throws IOException {
        return channel();
    }
}
