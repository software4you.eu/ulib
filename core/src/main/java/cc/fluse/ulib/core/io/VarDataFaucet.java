package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.io.channel.ByteBufferReadChannel;
import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import cc.fluse.ulib.core.util.Pool;
import org.jetbrains.annotations.NotNull;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * A data faucet that supports reading variable-length integers and arrays.
 *
 * @see VarNum
 */
public interface VarDataFaucet extends DataFaucet {

    /**
     * @see VarNum#readVarInt(Func)
     */
    default int readVarInt() throws IOException {
        try {
            return VarNum.readVarInt(this::read);
        } catch (IllegalArgumentException e) {
            throw new EOFException(e.getMessage());
        }
    }

    /**
     * @see VarNum#readVarLong(Func)
     */
    default long readVarLong() throws IOException {
        try {
            return VarNum.readVarLong(this::read);
        } catch (IllegalArgumentException e) {
            throw new EOFException(e.getMessage());
        }
    }

    /**
     * Reads a variable-length array from this channel. The array is encoded as a variable-length integer specifying the
     * length of the array, followed by the array contents.
     * <p>
     * The encoding format is: <pre> {@code <len: varint32> <data: uint8[]>}</pre>
     *
     * @return the array
     * @throws IOException if an I/O error occurs
     */
    default byte[] readVarArray() throws IOException {
        int len = readVarInt();
        var arr = new byte[len];
        readFully(arr);
        return arr;
    }

    /**
     * Reads a variable-length array from this channel. The array is encoded as a variable-length integer specifying the
     * length of the array, followed by the array contents. The array contents are passed block-wise to the given
     * consumer.
     * <p>
     * The encoding format is: <pre> {@code <len: varint32> <data: uint8[]>}</pre>
     *
     * @param consumer the consumer
     * @param <X>      the type of exception thrown by the consumer
     * @return the number of bytes read
     * @throws IOException if an I/O error occurs
     * @throws X           if the consumer throws an exception
     */
    default <X extends Exception> int readVarArray(@NotNull ParamTask<? super ByteBuffer, X> consumer) throws IOException, X {
        int len = readVarInt();

        try (var e = Pool.BYTE_BUFFER_POOL.obtain()) {
            var buf = e.get();

            int r = 0;
            while (r < len) {
                r += read(buf.clear().limit(Math.min(len - r, buf.capacity())));
                consumer.execute(buf.flip());
            }
            if (r != len)
                throw new UndefinedStateError("readVarArray: read " + r + " bytes, expected " + len + " bytes");

        }

        return len;
    }

    /**
     * Reads a variable-length array from this channel and provides it as a readable channel. The array is encoded as a
     * variable-length integer specifying the length of the array, followed by the array contents.
     * <p>
     * The encoding format is: <pre> {@code <len: varint32> <data: uint8[]>}</pre>
     *
     * @return the readable channel
     * @throws IOException if an I/O error occurs
     */
    default @NotNull ReadableDataChannel readVarChannel() throws IOException {
        return new ByteBufferReadChannel(ByteBuffer.wrap(readVarArray()));
    }

}
