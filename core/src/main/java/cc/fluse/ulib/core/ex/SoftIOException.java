package cc.fluse.ulib.core.ex;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class SoftIOException extends SoftException {
    public SoftIOException(@NotNull IOException cause) {
        super(cause);
    }

    @Override
    @NotNull
    public IOException getCause() {
        return (IOException) super.getCause();
    }
}
