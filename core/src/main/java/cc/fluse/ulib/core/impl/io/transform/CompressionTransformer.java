package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.ex.UndefinedStateError;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.transform.AbstractTransformer;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import static cc.fluse.ulib.core.util.Semantics.nofail;

public final class CompressionTransformer extends AbstractTransformer {

    private final Deflater compressor;
    private final Inflater decompressor;

    public CompressionTransformer(@NotNull Deflater compressor) {
        this.compressor = compressor;
        this.decompressor = null;
    }

    public CompressionTransformer(@NotNull Inflater decompressor) {
        this.decompressor = decompressor;
        this.compressor = null;
    }

    @Override
    protected void implWrite(@NotNull ByteBuffer src) throws InputFormatException {
        if (compressor != null) {
            // compression mode

            if (!compressor.needsInput()) writeCompressed(false);
            compressor.setInput(src.duplicate());
            writeCompressed(false);
            return;
        }
        UndefinedStateError.ensureNotNull(decompressor);
        // decompression mode

        if (!decompressor.needsInput()) writeDecompressed(false);
        decompressor.setInput(src.duplicate());
        writeDecompressed(false);
    }

    private void writeCompressed(boolean finish) {
        UndefinedStateError.ensureNotNull(compressor);
        nofail(IOException.class, () -> {
            var ch = IOUtil.synthesizeReadChannel(
                finish ? () -> !compressor.finished() : () -> !compressor.needsInput(), compressor::deflate);
            IOUtil.readAllBlockwise(ch, this::writeResult);
        });
    }

    private void writeDecompressed(boolean finish) throws InputFormatException {
        UndefinedStateError.ensureNotNull(decompressor);

        //noinspection resource
        var ch = IOUtil.synthesizeReadChannel(finish ? () -> !decompressor.finished()
            : () -> !decompressor.needsInput(), ParamFunc.as(decompressor::inflate));
        Expect.compute(() -> IOUtil.readAllBlockwise(ch, this::writeResult))
            .peekCaught(e -> {
                if (e instanceof DataFormatException dfe) throw new InputFormatException(dfe);
                if (e instanceof RuntimeException re) throw re;
                throw new UndefinedStateError(e);
            });
    }

    @Override
    protected void implComplete() throws IOTransformException {
        if (compressor != null) {
            // compression mode
            compressor.finish();
            writeCompressed(true);
            compressor.end();
            return;
        }
        UndefinedStateError.ensureNotNull(decompressor);

        // decompression mode
        writeDecompressed(true);
        decompressor.end();
    }
}
