package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.function.Function;

/**
 * A task that takes one parameter, returns a value and may throw a throwable object.
 *
 * @apiNote only pass this task object as function if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface ParamFunc<T, R, X extends Exception> extends Function<T, R> {

    @Deprecated
    static <T, R, X extends Exception> ParamFunc<T, R, X> as(@NotNull ParamFunc<T, R, ?> func) {
        return func::apply;
    }

    R execute(T t) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default R apply(T t) {
        return execute(t);
    }
}
