package cc.fluse.ulib.core.cli.ex;

import org.jetbrains.annotations.NotNull;

public class NoSuchOptionException extends OptionException {
    public NoSuchOptionException(@NotNull String optStr) {
        super(null, "No such Option '%s'".formatted(optStr));
    }
}
