package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.IOBouncer;
import org.jetbrains.annotations.NotNull;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class LimitInputStream extends FilterInputStream {

    /**
     * Constructs a new input stream allowing reading up to N bytes.
     *
     * @param source the source stream
     * @param limit  the maximum amount of bytes to read, &lt; 0 for no limit
     * @return the limited stream, or the source stream itself if limit &lt; 0
     */
    @NotNull
    public static InputStream limited(@NotNull InputStream source, long limit) {
        return limit < 0 ? source : new LimitInputStream(source, limit);
    }


    private final IOBouncer bouncer = Bouncer.io();
    private final long limit;
    private volatile long read;
    private long mark;

    /**
     * @param in    the source stream
     * @param limit the maximum bytes to pass through
     */
    public LimitInputStream(@NotNull InputStream in, long limit) {
        super(in);
        if ((this.limit = limit) < 0) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public synchronized int read() throws IOException {
        bouncer.pass();
        // limit reached?
        if (read >= limit) return -1;

        // read
        var r = super.read();
        if (r != -1) read++; // only inc if read successful
        return r;
    }

    @Override
    public synchronized int read(byte @NotNull [] b) throws IOException {
        bouncer.pass();
        return read(b, 0, b.length);
    }

    @Override
    public synchronized int read(byte @NotNull [] b, int off, int len) throws IOException {
        bouncer.pass();
        // limit reached?
        if (read >= limit) return -1;

        // bulk read
        var read = super.read(b, off, Math.min(len, (int) Math.min(limit - this.read, Integer.MAX_VALUE)));
        this.read += read;
        return read;
    }

    @Override
    public synchronized int available() throws IOException {
        bouncer.pass();
        return Math.toIntExact(Math.min(limit - read, super.available()));
    }

    @Override
    public synchronized void mark(int readlimit) {
        super.mark(readlimit);
        this.mark = this.read;
    }

    @Override
    public synchronized void reset() throws IOException {
        bouncer.pass();
        super.reset();
        this.read = this.mark;
    }

    @Override
    public long skip(long n) throws IOException {
        bouncer.pass();
        return super.skip(n);
    }

    @Override
    public void close() throws IOException {
        bouncer.block();
        super.close();
    }
}
