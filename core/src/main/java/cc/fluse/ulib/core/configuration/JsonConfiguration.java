package cc.fluse.ulib.core.configuration;

import cc.fluse.ulib.core.impl.configuration.json.JsonSerializer;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;

/**
 * Representation of a JSON-Type Configuration.
 */
public interface JsonConfiguration extends ReloadableConfiguration, DumpableConfiguration {

    /**
     * Creates a new empty JSON-Typed {@link Configuration}.
     *
     * @return the newly created configuration
     */
    @NotNull
    static JsonConfiguration newJson() {
        return JsonSerializer.getInstance().createNew();
    }

    /**
     * Loads a JSON-Typed {@link Configuration} from a reader.
     *
     * @param reader the JSON document
     * @return the loaded configuration
     */
    @NotNull
    static JsonConfiguration loadJson(@NotNull Reader reader) throws IOException {
        return JsonSerializer.getInstance().deserialize(reader);
    }

    /**
     * Loads a JSON-Typed {@link Configuration} from a stream.
     *
     * @param in the JSON document
     * @return the loaded configuration
     */
    @NotNull
    static JsonConfiguration loadJson(@NotNull InputStream in) throws IOException {
        return loadJson(new InputStreamReader(in));
    }

    /**
     * Loads a JSON-Typed {@link Configuration} from a certain path.
     *
     * @param path the path to the JSON document
     * @return the loaded configuration
     */
    @NotNull
    static JsonConfiguration loadJson(@NotNull Path path) throws IOException {
        try (var reader = Files.newBufferedReader(path)) {
            return loadJson(reader);
        }
    }

    @Override
    @NotNull JsonConfiguration getRoot();

    @Override
    @NotNull Optional<? extends JsonConfiguration> getSubsection(@NotNull KeyPath path);

    @Override
    @NotNull Collection<? extends JsonConfiguration> getSubsections(boolean deep);

    @Override
    @NotNull JsonConfiguration createSubsection(@NotNull KeyPath path);

    @Override
    @NotNull JsonConfiguration subAndCreate(@NotNull KeyPath path);
}
