package cc.fluse.ulib.core.ex;

import org.jetbrains.annotations.NotNull;

public class SoftInterruptedException extends SoftException {
    public SoftInterruptedException(@NotNull InterruptedException cause) {
        super(cause);
    }

    @Override
    @NotNull
    public InterruptedException getCause() {
        return (InterruptedException) super.getCause();
    }
}
