package cc.fluse.ulib.core.impl.inject;

import cc.fluse.ulib.core.function.BiParamTask;
import cc.fluse.ulib.core.impl.Internal;
import cc.fluse.ulib.core.impl.inject.InjectionConfiguration.Hooks;
import cc.fluse.ulib.core.impl.reflect.ClsRef;
import cc.fluse.ulib.core.inject.*;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import lombok.*;
import org.jetbrains.annotations.*;

import java.lang.instrument.UnmodifiableClassException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.*;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InjectionManager {
    public static final String HOOKING_KEY = "ulib.hooking", PROXY_KEY = "ulib.hook_proxy";

    @Getter
    private static final InjectionManager instance = new InjectionManager();

    static {
        System.getProperties().put(HOOKING_KEY, new Object[]{
                // [0] Hook runner
                (Function<Object[], ?>) instance::runHooks,

                // [1] Callback#isReturning()
                (Function<CallbackImpl<?>, Boolean>) CallbackImpl::isReturning,

                // [2] Callback#getReturnValue()
                (Function<Callback<?>, ?>) Callback::getReturnValue,

                // [3] caller class determination
                (Supplier<Class<?>>) ReflectUtil::getCallerClass
        });

        System.getProperties().put(PROXY_KEY, new Object[]{
                // [0] Proxy runner
                (Function<Object[], ?>) instance::runProxies,

                // [1] Callback#isReturning()
                (Function<CallbackImpl<?>, Boolean>) CallbackImpl::isReturning,

                // [2] Callback#hasReturnValue()
                (Function<Callback<?>, ?>) Callback::hasReturnValue,

                // [3] Callback#getReturnValue()
                (Function<Callback<?>, ?>) Callback::getReturnValue,

                // [4] caller class determination
                (Supplier<Class<?>>) ReflectUtil::getCallerClass
        });

        Internal.getInstrumentation().addTransformer(new ClassTransformer(instance), true);
    }

    // jvm class name -> conf
    private final Map<String, InjectionConfiguration> injections = new HashMap<>();
    private final Map<ClsRef, CompletableFuture<Void>> pendingTransform = new HashMap<>();
    private final Map<Thread, Map<String, Optional<TransformFail>>> transformResults = new HashMap<>();

    private Callback<?> runHooks(Object[] params) {
        return runHooks(
                ReflectUtil.getCallerClass(),
                (Class<?>) params[0],
                params[1],
                (boolean) params[2],
                params[3],
                (Class<?>) params[4],
                (String) params[5],
                (int) params[6],
                (Object[]) params[7]
        );
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private Callback<?> runHooks(Class<?> clazz, Class<?> returnType, Object returnValue, boolean hasReturnValue,
                                 Object self, Class<?> caller, String methodDescriptor, int at, Object[] params) {
        CallbackImpl<?> cb = new CallbackImpl(returnType, returnValue, hasReturnValue, self, caller);

        var calls = Optional.ofNullable(injections.get(ClsRef.getInternalName(clazz)))
                            .map(conf -> conf.getHooks().get(methodDescriptor))
                            .map(Hooks::getCallbacks)
                            .map(map -> map.get(at))
                            .orElse(Collections.emptySet());

        return processCalls((Set) calls, params, cb);
    }

    private Callback<?> runProxies(Object[] params) {
        return runProxies(
                ReflectUtil.getCallerClass(),
                (Class<?>) params[0],
                params[1],
                (boolean) params[2],
                params[3],
                params[4],
                (Class<?>) params[5],
                (String) params[6],
                (String) params[7],
                (int) params[8],
                (int) params[9],
                (Object[]) params[10]
        );
    }

    private Callback<?> runProxies(Class<?> clazz, Class<?> resultType, Object initialValue, boolean hasInitialValue,
                                   Object self, Object proxyInst, Class<?> caller,
                                   String methodSignature, String fullTargetSignature, int n, int at, Object[] params) {
        @SuppressWarnings({"unchecked", "rawtypes"})
        CallbackImpl<?> cb = new CallbackImpl(resultType, initialValue, hasInitialValue, self, proxyInst, caller);

        var calls = Optional.ofNullable(injections.get(ClsRef.getInternalName(clazz)))
                            .map(spec -> spec.getHooks().get(methodSignature))
                            .map(cont -> cont.getProxyCallbacks().get(at))
                            .map(proxyMap -> proxyMap.get(fullTargetSignature))
                            .map(callsMap -> {
                                // region collect & merge calls with defaults
                                // collect default calls
                                var defaultCalls = Optional.ofNullable(callsMap.get(0))
                                                           .orElse(Collections.emptySet());
                                var additionalCalls = Optional.ofNullable(callsMap.get(n))
                                                              .orElse(Collections.emptySet());

                                if (defaultCalls.isEmpty() && additionalCalls.isEmpty()) {
                                    return null;
                                }

                                Set<BiParamTask<? super Object[], ? super Callback<?>, ?>> merged = new LinkedHashSet<>(defaultCalls.size() + additionalCalls.size(), 1);
                                merged.addAll(defaultCalls);
                                merged.addAll(additionalCalls);
                                return merged;
                                // endregion
                            })
                            .orElse(Collections.emptySet());

        return processCalls(calls, params, cb);
    }

    @SneakyThrows
    private Callback<?> processCalls(Set<BiParamTask<? super Object[], ? super Callback<?>, ?>> calls, Object[] params, CallbackImpl<?> callback) {
        for (var call : calls) {
            try {
                //noinspection RedundantCast,unchecked
                ((BiParamTask<Object, Object, Exception>) call).execute(params.clone(), callback);
            } catch (InvocationTargetException e) {
                if (e.getCause() instanceof HookException he) {
                    throw he.getCause();
                }
                throw e.getCause();
            } catch (HookException he) {
                throw he.getCause();
            }
            if (callback.isCanceled()) {
                break; // cancel all future hook processing
            }
        }

        return callback;
    }

    boolean shouldProcess(String jvmCN) {
        return injections.containsKey(jvmCN);
    }

    Collection<String> getTargetMethods(String jvmCN) {
        return injections.get(jvmCN).getHooks().keySet();
    }

    boolean shouldProxy(String jvmCN, String methodSignature, HookPoint where, String targetSignature, int n) {
        var conf = injections.get(jvmCN);
        if (conf == null) {
            return false;
        }

        var cont = conf.getHooks().get(methodSignature);
        if (cont == null) {
            return false;
        }

        return Optional.ofNullable(cont.getProxyCallbacks().get(where.ordinal()))
                       .map(proxyMap -> proxyMap.get(targetSignature))
                       .map(cbMap -> cbMap.containsKey(0) || cbMap.containsKey(n))
                       .orElse(false);
    }

    void ensureProxySatisfaction(String jvmCN, String methodSignature, Map<HookPoint, Map<String, Collection<Integer>>> done)
    throws ConfigurationSatisfactionException {

        var cont = Optional.ofNullable(injections.get(jvmCN))
                           .map(conf -> conf.getHooks().get(methodSignature))
                           .orElseThrow(() -> new IllegalArgumentException("injection config of %s does not specify method %s to be hooked"
                                                                                   .formatted(jvmCN, methodSignature)));


        for (var en : cont.getProxyCallbacks().entrySet()) {
            var point = HookPoint.values()[en.getKey()];

            if (!done.containsKey(point)) {
                throw new ConfigurationSatisfactionException("Hook Point %s not found in `%s`"
                                                                     .formatted(point, methodSignature));
            }
            var doneTargets = done.get(point);

            var targets = en.getValue();

            for (var targetEn : targets.entrySet()) {
                var target = targetEn.getKey();

                if (!doneTargets.containsKey(target)) {
                    throw new ConfigurationSatisfactionException("Target `%s` for %s not found in %s"
                                                                         .formatted(target, point, methodSignature));
                }
                var doneNs = doneTargets.get(target);

                for (int n : targetEn.getValue().keySet()) {
                    if (n == 0) { // 0 means all
                        if (doneNs.isEmpty()) // throw if not a single injection happened
                        {
                            throw new ConfigurationSatisfactionException("No occurrence of target `%s`for %s found in %s"
                                                                                 .formatted(target, point, methodSignature));
                        }
                        continue;
                    }

                    if (!doneNs.contains(n)) {
                        throw new ConfigurationSatisfactionException("Nth occurrence (%d) of target `%s` for %s not found in %s"
                                                                             .formatted(n, target, point, methodSignature));
                    }
                }

            }

        }
    }

    void transformResult(@NotNull String jvmCN, @Nullable final Throwable thr) {

        if (transformResults.containsKey(Thread.currentThread())) {
            transformResults.get(Thread.currentThread())
                            .put(jvmCN, thr != null ? Optional.of(new TransformFail(jvmCN, thr)) : Optional.empty());
            return;
        }

        // incoming result outside manually triggered transform

        synchronized (this) {
            var it = pendingTransform.entrySet().iterator();
            while (it.hasNext()) {
                var en = it.next();
                var ref = en.getKey();

                if (!ref.getInternalName().equals(jvmCN)) {
                    continue;
                }
                it.remove();

                var fut = en.getValue();
                if (thr == null) {
                    fut.complete(null);
                } else {
                    fut.completeExceptionally(new TransformFail(jvmCN, thr));
                }
                break;
            }
        }

    }

    @Unmodifiable
    public synchronized Map<String, CompletableFuture<Void>> injectionsJoin(Map<ClsRef, InjectionConfiguration> instructions) {
        // TODO: transition from merge operation in hard-code to dynamic code?
        var op = new MergeOp(this.injections, instructions);
        op.merge();

        var toRedefine = op.needsRedefinition;
        if (!toRedefine.isEmpty()) {
            toRedefine.forEach(ref -> pendingTransform.put(ref, new CompletableFuture<>()));
        }

        var map = new HashMap<String, CompletableFuture<Void>>();
        instructions.keySet().forEach(ref ->
                                              map.put(ref.getInternalName(), Optional.ofNullable(pendingTransform.get(ref)).orElseGet(() -> CompletableFuture.completedFuture(null))));
        return Collections.unmodifiableMap(map);
    }

    public synchronized boolean hasPending() {
        return !pendingTransform.isEmpty();
    }

    @Unmodifiable
    public synchronized Map<String, CompletableFuture<Void>> getPending(@NotNull Predicate<@NotNull ClsRef> filter) {
        //noinspection unchecked
        return Map.ofEntries(pendingTransform.entrySet().stream()
                                             .filter(en -> filter.test(en.getKey()))
                                             .map(en -> Map.entry(en.getKey().getInternalName(), en.getValue()))
                                             .toArray(Map.Entry[]::new));
    }

    /**
     * Performs the transformation on all selected pending objects. In each case, success and failure, the pending
     * transform will be removed from the internal queue.
     *
     * @param filter the filter to select which transform to perform
     * @return pairs of the jvm class name representation and the respective results
     */
    @Unmodifiable
    public synchronized Map<String, Optional<? extends Exception>> transformPending(@NotNull Predicate<@NotNull ClsRef> filter)
    throws UnmodifiableClassException {
        if (!hasPending()) {
            return Collections.emptyMap();
        }
        var toTransform = pendingTransform.keySet().stream()
                                          .filter(filter).toList();

        // collect the classes to be redefined
        //noinspection rawtypes
        var toRedefine = new ArrayList<>(toTransform.stream()
                                                    .filter(ref -> ref.cls() != null)
                                                    .map(ref -> (Class) ref.cls())
                                                    .toList());
        var jvmFetch = toTransform.stream()
                                  .filter(ref -> ref.cls() == null)
                                  .map(ref -> ref.getInternalName())
                                  .toList();

        // fetch classes directly from jvm if necessary
        // WARNING: READS THROUGH ALL LOADED CLASSES INSIDE THE JVM!
        if (!jvmFetch.isEmpty()) {
            toRedefine.addAll(Arrays.stream(Internal.getInstrumentation().getAllLoadedClasses())
                                    .filter(cl -> jvmFetch.contains(ClsRef.getInternalName(cl)))
                                    .toList());
        }

        // redefine classes
        transformResults.put(Thread.currentThread(), new HashMap<>());
        Exception ex = null;
        for (Class<?> cl : toRedefine) {
            try {
                Internal.getInstrumentation().retransformClasses(cl);
            } catch (UnmodifiableClassException e) {
                if (ex != null) {
                    ex.addSuppressed(e);
                } else {
                    ex = e;
                }
            }
        }

        // process result
        final var results = transformResults.remove(Thread.currentThread());
        for (ClsRef ref : toTransform) {
            var result = results.get(ref.getInternalName());
            if (result == null) {
                var e = new IllegalStateException("Missing transform result: " + ref.getInternalName());
                if (ex != null) {
                    ex.addSuppressed(e);
                } else {
                    ex = e;
                }
                continue;
            }
            var fut = pendingTransform.remove(ref);

            if (result.isPresent()) {
                // transform failed
                fut.completeExceptionally(result.get());
                continue;
            }

            // transform success
            fut.complete(null);
        }

        if (ex != null) {
            if (ex instanceof UnmodifiableClassException uce) {
                throw uce;
            }
            throw (RuntimeException) ex;
        }
        return Collections.unmodifiableMap(results);
    }

    @RequiredArgsConstructor(access = AccessLevel.PRIVATE)
    private static final class MergeOp {
        private final Map<String, InjectionConfiguration> existing;
        private final Map<ClsRef, InjectionConfiguration> merging;
        private final HashSet<ClsRef> needsRedefinition = new HashSet<>();
        private ClsRef currentClass;

        private void markRedefinition() {
            needsRedefinition.add(currentClass);
        }

        private void merge() {
            merging.forEach((ref, mergingConf) -> {
                currentClass = ref;

                if (!existing.containsKey(ref.getInternalName())) {
                    existing.put(ref.getInternalName(), mergingConf);
                    markRedefinition();
                    return;
                }

                merge(existing.get(ref.getInternalName()), mergingConf);
            });

            currentClass = null;
        }

        @SuppressWarnings({"unchecked", "rawtypes"})
        private void merge(InjectionConfiguration existingConf, InjectionConfiguration mergingConf) {
            mergingConf.getHooks().forEach((descriptor, mergingCont) -> {
                var existingHooks = existingConf.getHooks();
                if (!existingHooks.containsKey(descriptor)) {
                    existingHooks.put(descriptor, mergingCont);
                    markRedefinition();
                    return;
                }

                // raw type for compiler compatibility
                merge((Hooks) existingHooks.get(descriptor), (Hooks) mergingCont);
            });
        }

        @SuppressWarnings({"rawtypes", "unchecked"})
        private <R> void merge(Hooks<R> existingCont, Hooks<R> mergingCont) {
            // raw type for compiler compatibility
            Map existingCallbacks = existingCont.getCallbacks();
            mergingCont.getCallbacks().forEach((at, mergingCallbacks) -> {
                if (!existingCallbacks.containsKey(at)) {
                    existingCallbacks.put(at, mergingCallbacks);
                    // no necessity to indicate redefinition bc regular hooks are always injected at HEAD & RETURN regardless of present callbacks
                    return;
                }

                ((Collection) existingCallbacks.get(at)).addAll(mergingCallbacks);

            });

            var existingProxyCallbacks = existingCont.getProxyCallbacks();
            mergingCont.getProxyCallbacks().forEach((at, mergingProxyMap) -> {
                if (!existingProxyCallbacks.containsKey(at)) {
                    existingProxyCallbacks.put(at, mergingProxyMap);
                    markRedefinition();
                    return;
                }

                var existingProxyMap = existingProxyCallbacks.get(at);
                mergingProxyMap.forEach((targetSignature, mergingCallMap) -> {
                    if (!existingProxyMap.containsKey(targetSignature)) {
                        existingProxyMap.put(targetSignature, mergingCallMap);
                        markRedefinition();
                        return;
                    }

                    var existingCallMap = existingProxyMap.get(targetSignature);
                    mergingCallMap.forEach((n, mergingCalls) -> {
                        if (!existingCallMap.containsKey(n)) {
                            existingCallMap.put(n, mergingCalls);
                            markRedefinition();
                            return;
                        }

                        existingCallMap.get(n).addAll(mergingCalls);

                    });

                });

            });
        }
    }

}
