package cc.fluse.ulib.core.impl.database.sql;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.database.sql.orm.Entity;
import cc.fluse.ulib.core.database.sql.orm.Serializer;
import cc.fluse.ulib.core.database.sql.query.Condition;
import cc.fluse.ulib.core.database.sql.query.Query;
import cc.fluse.ulib.core.database.sql.query.Where;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.database.sql.orm.EntityImpl;
import cc.fluse.ulib.core.impl.database.sql.query.QueryImpl;
import cc.fluse.ulib.core.impl.database.sql.query.QueryStartImpl;
import cc.fluse.ulib.core.impl.database.sql.query.SetQueryImpl;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.util.ArrayUtil;
import cc.fluse.ulib.core.util.Expect;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public abstract class AbstractSqlTable implements cc.fluse.ulib.core.database.sql.Table {
    public final AbstractSqlDatabase sql;
    @Getter
    protected final String name;
    protected final Map<String, Column<?>> columns;

    protected AbstractSqlTable(AbstractSqlDatabase sql, String name, Collection<Column<?>> columns) {
        this.sql = sql;
        this.name = name;
        this.columns = columns.stream().collect(Collectors.toMap(Column::getName, Function.identity()));
    }

    @Override
    public @NotNull Expect<Entity, ?> getEntity(@NotNull Object primaryKeyValue, @NotNull Map<String, Collection<Serializer<?, ?>>> serializers, boolean fetchLazy, boolean cache, boolean pushLazy) {
        var pk = getPrimaryKey().orElseThrow(() -> new UnsupportedOperationException("Table has no primary key"));
        // test if row with primary key exists
        try {
            if (!IOUtil.op(() -> select(pk.getName()).where(pk).isEqualToP(primaryKeyValue).query(), ResultSet::next, true))
                return Expect.empty();
        } catch (SQLException e) {
            return Expect.failed(e);
        }

        return Expect.compute(() -> new EntityImpl(this, q -> q.where(pk).isEqualToP(primaryKeyValue),
                serializers, fetchLazy, cache, pushLazy));
    }

    @Override
    public @NotNull Collection<Entity> getEntities(@NotNull Map<String, Function<? super Condition<Where>, Where>> keyValueSelectors,
                                                   @NotNull Map<String, Collection<Serializer<?, ?>>> serializers,
                                                   boolean fetchLazy, boolean cache, boolean pushLazy) {
        // TODO: support for tables without primary key

        var pk = getPrimaryKey().orElseThrow(() -> new UnsupportedOperationException("Table has no primary key"));
        var it = keyValueSelectors.entrySet().iterator();
        if (!it.hasNext()) throw new IllegalArgumentException("no selectors");

        // build where clause with given selectors
        var _1st = it.next();
        var w = _1st.getValue().apply(select(pk.getName()).where(_1st.getKey()));
        while (it.hasNext()) {
            var e = it.next();
            w = e.getValue().apply(w.and(e.getKey()));
        }

        // query!
        var li = new LinkedList<Entity>();
        try (var res = w.query()) {
            if (!res.next()) return Collections.emptyList();
            do {
                var pkVal = res.getObject(pk.getName());
                li.add(getEntity(pkVal, serializers, fetchLazy, cache, pushLazy).orElseRethrowRE());
            } while (res.next());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return Collections.unmodifiableCollection(li);
    }

    @Override
    public @NotNull Entity createEntity(@NotNull Map<String, Object> values, @NotNull Map<String, Collection<Serializer<?, ?>>> serializers, boolean fetchLazy, boolean hold, boolean pushLazy) {
        // TODO: support tables without primary key
        var pk = getPrimaryKey().orElse(null);
        if (pk == null) throw new UnsupportedOperationException("Table has no primary key");

        var pkValue = insert(values);
        if (pkValue instanceof Boolean b) throw new IllegalStateException("Boolean returned from insert");

        return getEntity(pkValue, serializers, fetchLazy, hold, pushLazy).orElseThrow();
    }

    @Override
    public @NotNull Optional<Column<?>> getPrimaryKey() {
        return columns.values().stream()
                .filter(c -> c.getIndex().map(i -> i == Column.Index.PRIMARY).orElse(false))
                .reduce((a, b) -> {throw new IllegalStateException("Multiple primary keys found");});
    }

    @Override
    public @NotNull Column<?>[] getColumns() {
        return columns.values().toArray(new Column[0]);
    }

    public Collection<Column<?>> columns() {
        return columns.values();
    }

    @Override
    @NotNull
    public Optional<Column<?>> getColumn(@NotNull String name) {
        return Optional.ofNullable(columns.get(name));
    }

    /**
     * Creates a table in the database by this table's definition.
     *
     * @param dataTypeOverwrites         a map of data type names to overwrite the data type with
     * @param columnTypeOverwrites       a map of column to data type name to overwrite the data type with of a column
     * @param propertiesOverwrites       a map of property name to value to overwrite the properties of the table.
     *                                   Possible properties are:
     *                                   <ul>
     *                                   <li>isIndexBeforeAutoIncrement: boolean, default: <code>true</code></li>
     *                                   <li>autoIncrementKeyword: String, default: "<code>auto_increment</code>"</li>
     *                                   </ul>
     * @param columnPropertiesOverwrites a map of column to properties to overwrite the properties of a column. Possible
     *                                   properties are:
     *                                   <ul>
     *                                   <li>isEnum: boolean, default: fallback to {@link Column#getDataType()} == {@link DataType#ENUM}</li>
     *                                   <li>isAutoIncrement: boolean, default: fallback to {@link Column#isAutoIncrement()}</li>
     *                                   </ul>
     * @return whether the table exists after this operation
     */
    // TODO: synchronize
    protected boolean create(Map<DataType, String> dataTypeOverwrites,
                             Map<Column<?>, String> columnTypeOverwrites,
                             Map<String, Object> propertiesOverwrites,
                             Map<Column<?>, Map<String, Object>> columnPropertiesOverwrites) throws SQLException {
        var columnsDefinition = columns.values().stream()
                .<String>map(col -> {
                    var sb = new StringBuilder("%s %s".formatted(sql.quote(col.getName(), true),
                            columnTypeOverwrites.getOrDefault(col, dataTypeOverwrites.getOrDefault(col.getDataType(), col.getDataType().name()))));

                    // size or enum values
                    if ((boolean) columnPropertiesOverwrites.getOrDefault(col, Collections.emptyMap()).getOrDefault("isEnum", col.getDataType() == DataType.ENUM)) {
                        var sj = new StringJoiner(", ", "(", ")");
                        sj.setEmptyValue("");
                        for (Object o : col.getAcceptable()) {
                            sj.add(sql.quote(o, false));
                        }
                        sb.append(sj);
                    } else if (col.getSize() >= 0) {
                        sb.append(String.format("(%d)", col.getSize()));
                    }


                    // not null
                    if (col.isNotNull()) sb.append(" not null");

                    // index and auto increment
                    var isAI = (boolean) columnPropertiesOverwrites.getOrDefault(col, Collections.emptyMap()).getOrDefault("isAutoIncrement", col.isAutoIncrement());
                    var aiKeyword = (String) propertiesOverwrites.getOrDefault("autoIncrementKeyword", "auto_increment");
                    if ((boolean) propertiesOverwrites.getOrDefault("isIndexBeforeAutoIncrement", true)) {
                        col.getIndex().ifPresent(in -> sb.append(" ").append(in.getSql()));
                        if (isAI) sb.append(" ").append(aiKeyword);
                    } else {
                        if (isAI) sb.append(" ").append(aiKeyword);
                        col.getIndex().ifPresent(in -> sb.append(" ").append(in.getSql()));
                    }

                    // default value
                    if (col.getDefaultValue() != null) {
                        sb.append(" default ");
                        var def = col.getDefaultValue();
                        sb.append(def instanceof Number || def instanceof Boolean ? def : sql.quote(def, false));
                    }

                    return sb.toString();
                })
                .collect(Collectors.joining(", "));


        try (var e = sql.getConnection();
             var st = e.get().prepareStatement("create table %s (%s);".formatted(sql.quote(name, true), columnsDefinition))) {
            if (st.executeUpdate() > 0) return true;
        }
        return exists();
    }

    @SneakyThrows
    @Override
    public boolean drop() {
        try (var e = sql.getConnection();
             var st = e.get().prepareStatement(String.format("drop table %s;", sql.quote(name, true)))) {
            if (st.executeUpdate() > 0) return true;
        }
        // intentionally not using `st.executeUpdate() > 0 || !exists()` to close statement before calling `exists()`
        return !exists();
    }

    @Override
    public @NotNull Query select(@NotNull String what, String... select) {
        return sel(false, ArrayUtil.concat(what, select));
    }

    @Override
    public @NotNull Query selectDistinct(@NotNull String what, @NotNull String @NotNull ... select) {
        return sel(true, ArrayUtil.concat(what, select));
    }

    /**
     * Starts a select query.
     *
     * @param distinct whether to select distinct values
     * @param what     the columns to select
     * @return the query
     */
    private QueryImpl sel(boolean distinct, String[] what) {
        return new QueryImpl(sql, this, String.format("%s %s from", distinct ? "select distinct" : "select",
                String.join(", ", what)));
    }

    @Override
    public @NotNull SetQueryImpl update() {
        return new SetQueryImpl(sql, this, "update");
    }

    @SneakyThrows
    @Override
    public boolean insert(@NotNull Object v, Object... vs) {
        try (var e = sql.getConnection();
             var st = insert0(List.of(ArrayUtil.concat(v, vs)), e.get()::prepareStatement)) {
            return st.executeUpdate() > 0;
        }
    }

    @SneakyThrows
    @Override
    public final @NotNull Object insert(@NotNull Map<String, Object> v) {
        Map<Column<?>, Object> map = v.entrySet().stream()
                .map(en -> {
                    var col = getColumn(en.getKey())
                            .orElseThrow(() -> new IllegalArgumentException("Column %s not found".formatted(en.getKey())));
                    return Tuple.of(col, en.getValue());
                })
                .collect(Collectors.toMap(Pair::getFirst, Pair::getSecond));

        if (getPrimaryKey().isEmpty()) {
            try (var e = sql.getConnection();
                 var st = insert0(map, e.get()::prepareStatement)) {
                return st.executeUpdate() > 0;
            }
        }

        try (var e = sql.getConnection();
             var st = insert0(map, str -> e.get().prepareStatement(str, Statement.RETURN_GENERATED_KEYS))) {
            st.executeUpdate();

            var rs = st.getGeneratedKeys();
            if (!rs.next()) {
                throw new IllegalStateException("No generated keys found");
            }

            return rs.getObject(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Generates an insert prepared statement with select columns.
     *
     * @param values       map with the values to insert (column -> value)
     * @param statementGen the function to generate the statement
     * @return the statement
     */
    protected <X extends Exception> PreparedStatement insert0(Map<Column<?>, Object> values,
                                                              ParamFunc<? super String, ? extends PreparedStatement, X> statementGen)
            throws SQLException, X {
        var columnsStr = new StringJoiner(", ", "(", ")");
        columnsStr.setEmptyValue("()");
        var valuesStr = new StringJoiner(", ", "(", ")");
        valuesStr.setEmptyValue("()");

        values.forEach((c, v) -> {
            columnsStr.add(sql.quote(c.getName(), true));
            valuesStr.add("?");
        });

        var st = statementGen.execute(insert_query_template(false).formatted(sql.quote(name, true), columnsStr, valuesStr));

        var it = values.values().iterator(); // TODO: check if order is the same as above
        int i = 0;
        while (it.hasNext()) {
            st.setObject(++i, it.next());
        }

        return st;
    }

    /**
     * Generates an insert prepared statement with all columns.
     *
     * @param values       the values to insert
     * @param statementGen the function to generate the statement
     * @return the statement
     */
    protected <X extends Exception> PreparedStatement insert0(List<?> values,
                                                              ParamFunc<? super String, ? extends PreparedStatement, X> statementGen)
            throws SQLException, X {
        var valuesStr = new StringJoiner(", ", "(", ")");
        valuesStr.setEmptyValue("()");
        for (int i = 0; i < values.size(); i++) {
            valuesStr.add("?");
        }

        var st = statementGen.execute(insert_query_template(true).formatted(sql.quote(name, true), valuesStr));

        var it = values.iterator();
        int i = 0;
        while (it.hasNext()) {
            st.setObject(++i, it.next());
        }

        return st;
    }

    protected String insert_query_template(boolean allValues) {
        return allValues ? "insert into %s values %s" : "insert into %s %s values %s";
    }

    @Override
    @NotNull
    public QueryStartImpl delete() {
        return new QueryStartImpl(sql, this, "delete from");
    }

    @SneakyThrows
    @Override
    public boolean truncate() {
        try (var e = sql.getConnection();
             var st = e.get().prepareStatement("truncate table %s".formatted(sql.quote(name, true)))) {
            return st.executeUpdate() > 0;
        }
    }
}
