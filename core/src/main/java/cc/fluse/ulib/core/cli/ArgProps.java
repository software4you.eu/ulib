package cc.fluse.ulib.core.cli;

import cc.fluse.ulib.core.util.Conditions;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public record ArgProps(@NotNull ArgNum num, @NotNull Collection<String> defaultArguments) {

    /**
     * Checks if a certain number of arguments is allowed.
     *
     * @param num the number of arguments
     * @return {@code true} if the amount of arguments is allowed, {@code false} otherwise
     */
    public boolean allowsArgs(int num) {
        return Conditions.bti(this.num.min(), num, this.num.max());
    }
}
