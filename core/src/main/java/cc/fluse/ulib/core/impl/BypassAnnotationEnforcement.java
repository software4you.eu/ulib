package cc.fluse.ulib.core.impl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to bypass the enforcement of {@link org.jetbrains.annotations.Nullable} and
 * {@link org.jetbrains.annotations.NotNull} annotations in the ulib exposed API.
 */
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.CLASS)
public @interface BypassAnnotationEnforcement {
}
