package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

/**
 * A task that takes one parameter, does not return a result and may throw a throwable object.
 *
 * @apiNote only pass this task object as consumer if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface ParamTask<T, X extends Exception> extends Consumer<T> {

    @Deprecated
    static <T, X extends Exception> ParamTask<T, X> as(@NotNull ParamTask<T, ?> func) {
        return func::accept;
    }

    void execute(T t) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default void accept(T t) {
        execute(t);
    }
}
