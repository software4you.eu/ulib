package cc.fluse.ulib.core.impl.database.sql.query;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.query.Where;
import org.jetbrains.annotations.NotNull;

final class WhereImpl extends QueryEndpointImpl implements Where {

    WhereImpl(ConditionImpl<Where> condition) {
        this(condition, "where");
    }

    WhereImpl(ConditionImpl<Where> condition, String operand) {
        super(condition.meta);
        meta.query.append(String.format(" %s %s%s %s",
                operand, condition.not ? "not " : "", meta.sql.quote(condition.source, true), condition.condition));
    }

    WhereImpl(Metadata meta, String condition) {
        super(meta);
        append("where", condition);
    }

    private void append(String operand, String condition) {
        meta.query.append(String.format(" %s %s", operand, condition));
    }


    @Override
    public @NotNull ConditionImpl<Where> and(@NotNull Column<?> column) {
        return and(column.getName());
    }

    @Override
    public @NotNull ConditionImpl<Where> and(@NotNull String column) {
        return new ConditionImpl<>(meta, column, c -> new WhereImpl(c, " and"));
    }

    @Override
    public @NotNull WhereImpl andRaw(@NotNull String condition) {
        append("and", condition);
        return this;
    }

    @Override
    public @NotNull ConditionImpl<Where> or(@NotNull Column<?> column) {
        return or(column.getName());
    }

    @Override
    public @NotNull ConditionImpl<Where> or(@NotNull String column) {
        return new ConditionImpl<>(meta, column, c -> new WhereImpl(c, " or"));
    }

    @Override
    public @NotNull WhereImpl orRaw(@NotNull String condition) {
        append("or", condition);
        return this;
    }
}
