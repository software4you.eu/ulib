package cc.fluse.ulib.core.io;

import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.CompletableFuture;

/**
 * Represents something that can be written to.
 *
 * @implSpec Specific characteristics for the methods have to be defined by the implementing class.
 */
public interface Writable {

    /**
     * Creates a new {@link Writable} object wrapping the given channel. Each opened stream or channel is backed by the
     * given channel, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source channel.
     *
     * @param channel the channel to wrap
     * @return a new {@link Writable} object wrapping the given channel
     */
    static @NotNull Writable of(@NotNull WritableByteChannel channel) {
        return () -> IOUtil.isolate(channel);
    }

    /**
     * Creates a new {@link Writable} object sourcing each opened stream or channel from the given channel supplier.
     *
     * @param channelSupplier the channel supplier
     * @return a new {@link Writable} object
     */
    static @NotNull Writable of(@NotNull Func<? extends @NotNull WritableByteChannel, IOException> channelSupplier) {
        return channelSupplier::execute;
    }

    /**
     * Creates a new {@link Writable} object wrapping the given stream. Each opened stream or channel is backed by the
     * given stream, thus they <b>do not</b> operate independently. The returned streams and channels are isolated,
     * meaning a close operation has no effect on the given source stream.
     *
     * @param stream the stream to wrap
     * @return a new {@link Writable} object wrapping the given stream
     */
    static @NotNull Writable of(@NotNull OutputStream stream) {
        return of(Channels.newChannel(stream));
    }

    /**
     * Opens a stream to write contents to the object. The stream is not closed automatically, closure has to be handled
     * by the caller.
     *
     * @return a stream writing the object's contents
     * @implSpec Specific characteristics for the stream have to be defined by the implementing class.
     */
    @NotNull
    default OutputStream streamWrite() throws IOException {
        return Channels.newOutputStream(channelWrite());
    }

    /**
     * Opens a stream to write contents to the object and passes it to the given writer function, which is executed
     * asynchronously. The writer function's result is returned as a future object. The stream is closed automatically.
     *
     * @param writer a function writing data to the object
     * @return a future object wrapping the writing function's result
     *
     * @implSpec Specific characteristics for the stream have to be defined by the implementing class.
     */
    @NotNull
    default CompletableFuture<Void> streamWrite(@NotNull ParamTask<? super @NotNull OutputStream, ?> writer) {
        try {
            return CompletableFuture.completedFuture(streamWrite()).thenAcceptAsync(ParamTask.as(out -> {
                try (out) {
                    writer.execute(out);
                }
            }));
        } catch (IOException e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    /**
     * Same as {@link #channel()}.
     *
     * @see #channel()
     */
    @NotNull
    default WritableByteChannel channelWrite() throws IOException {
        return channel();
    }

    /**
     * Opens a channel to write contents to the object. The channel is not closed automatically, closure has to be
     * handled by the caller.
     *
     * @return the channel
     * @implSpec Specific characteristics for the channel have to be defined by the implementing class. It is advised to
     * consider isolating the new channel if the object itself is backed by a channel and not an object that can be
     * opened (i.e. a file)
     */
    @NotNull WritableByteChannel channel() throws IOException;

    /**
     * Opens a channel to write contents to the object and passes it to the given writer function, which is executed
     * asynchronously. The writer function's result is returned as a future object. The channel is closed
     * automatically.
     *
     * @param writer a function writing data to the object
     * @return a future object wrapping the writing function's result
     *
     * @implSpec Specific characteristics for the channel have to be defined by the implementing class.
     */
    @NotNull
    default CompletableFuture<Void> channelWrite(@NotNull ParamTask<? super @NotNull WritableByteChannel, ?> writer) {
        try {
            return CompletableFuture.completedFuture(channelWrite()).thenAcceptAsync(ParamTask.as(ch -> {
                try (ch) {
                    writer.execute(ch);
                }
            }));
        } catch (IOException e) {
            return CompletableFuture.failedFuture(e);
        }
    }

    /**
     * Opens a channel to the object and writes the given bytes contained in the given buffer to it.
     *
     * @param buffer the buffer containing the bytes to write
     * @return a future object wrapping the writing function's result
     */
    @NotNull
    default CompletableFuture<Void> writeAll(@NotNull ByteBuffer buffer) {
        return channelWrite((ParamTask<WritableByteChannel, ?>) c -> {
            try (c) {
                c.write(buffer);
            }
        });
    }
}
