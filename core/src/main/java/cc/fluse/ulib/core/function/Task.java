package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.*;

import java.util.concurrent.Callable;

/**
 * A task that does not return a result and may throw a throwable object.
 *
 * @apiNote only pass this task object as runnable if you certainly know it won't throw an exception
 */
@FunctionalInterface
public interface Task<X extends Exception> extends Callable<Void>, Runnable {

    @Deprecated
    static <X extends Exception> Task<X> as(@NotNull Task<?> func) {
        return func::run;
    }

    void execute() throws X;

    @Override
    @Nullable
    @Contract("-> null")
    default Void call() throws Exception {
        execute();
        return null;
    }

    @SneakyThrows
    @Override
    @Deprecated
    default void run() {
        execute();
    }
}
