package cc.fluse.ulib.core.impl.tuple;

import cc.fluse.ulib.core.tuple.Tuple;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class TupleImpl<E> implements Tuple<E> {
    protected final E[] elements;

    @SafeVarargs
    public TupleImpl(E... elements) {
        this.elements = Arrays.copyOf(elements, elements.length);
    }

    protected final <T extends E> T getI(int i) {
        //noinspection unchecked
        return (T) elements[i];
    }

    @Override
    public int size() {
        return elements.length;
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return Spliterators.iterator(spliterator());
    }

    @Override
    public Spliterator<E> spliterator() {
        return Arrays.spliterator(elements);
    }

    @Override
    public @NotNull List<E> asList() {
        // not using List.of(E) to support null elements
        return Collections.unmodifiableList(Arrays.asList(elements));
    }

    @Override
    public @NotNull Set<E> asSet() {
        return Collections.unmodifiableSet(new LinkedHashSet<>(Arrays.asList(elements)));
    }
}
