package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.database.RemoteDatabase;

public interface MariaDBDatabase extends SqlDatabase, RemoteDatabase {
}
