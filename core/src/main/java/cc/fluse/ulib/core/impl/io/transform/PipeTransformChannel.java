package cc.fluse.ulib.core.impl.io.transform;

import cc.fluse.ulib.core.io.transform.TransformChannel;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

import static cc.fluse.ulib.core.util.Semantics.nofail;

public class PipeTransformChannel extends AbstractTransformChannel implements TransformChannel {

    @Override
    public int write(@NotNull ByteBuffer src) throws ClosedChannelException, InputFormatException {
        return super.push(src);
    }

    @Override
    public int read(@NotNull ByteBuffer dst) throws ClosedChannelException {
        return super.pull(dst);
    }

    @Override
    public boolean holdsData() throws ClosedChannelException {
        return super.holdsData();
    }

    @Override
    public void flush() throws IOException {
        process();
    }

    @Override
    public void close() {
        nofail(super::close);
    }
}
