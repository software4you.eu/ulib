package cc.fluse.ulib.core.util;

import cc.fluse.ulib.core.ex.SoftInterruptedException;
import cc.fluse.ulib.core.function.Func;
import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.impl.value.LazyValueImpl;
import cc.fluse.ulib.core.impl.value.NoSetLazyValue;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.tuple.Value;
import org.jetbrains.annotations.*;

import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Essentially a lazy value is just a cached value. A regular lazy value has the ability to autonomously obtain/cache
 * ("fetch") the value and to inform a third party ("push") about a value update.
 *
 * <p>
 * A <b>push-only</b> lazy value, is a lazy value that is not able to autonomously obtain a value (see
 * {@link #isAvailable()}).
 * <p>
 * A <b>fetch-only</b> lazy value, is a lazy value that will not perform any pushing action when an
 * {@link #set(Object) external value update} is received.
 *
 * @param <T> the value type
 */
@BypassAnnotationEnforcement
public interface LazyValue<T> extends Value<T> {

    /**
     * Constructs a lazy value with no initial value.
     *
     * @param fetch the fetch function
     * @param push  the push function
     * @param <T>   the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> of(@Nullable Func<? extends @Nullable T, ?> fetch, @Nullable ParamTask<? super @Nullable T, ?> push) {
        return new LazyValueImpl<>(null, fetch, push);
    }

    /**
     * Constructs a <b>fetch-only</b> lazy value with no initial value.
     *
     * @param fetchFunction the fetch function
     * @param <T>           the value type
     * @return the newly constructed object
     *
     * @see #immutable(Func)
     */
    @NotNull
    static <T> LazyValue<T> of(@NotNull Func<? extends @Nullable T, ?> fetchFunction) {
        return new LazyValueImpl<>(null, Objects.requireNonNull(fetchFunction), null);
    }

    /**
     * Constructs a <b>push-only</b> lazy value with no initial value.
     *
     * @param pushFunction the push function
     * @param <T>          the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> of(@NotNull ParamTask<? super @Nullable T, ?> pushFunction) {
        return new LazyValueImpl<>(null, null, Objects.requireNonNull(pushFunction));
    }

    /**
     * Constructs a lazy value.
     *
     * @param val   the initial value
     * @param fetch the fetch function
     * @param push  the push function
     * @param <T>   the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> of(@Nullable T val, @Nullable Func<? extends @Nullable T, ?> fetch, @Nullable ParamTask<? super @Nullable T, ?> push) {
        return new LazyValueImpl<>(Tuple.of(val), fetch, push);
    }

    /**
     * Constructs a <b>fetch-only</b> lazy value with an initial value.
     *
     * @param value         the initial value
     * @param fetchFunction the fetch function
     * @param <T>           the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> of(@Nullable T value, @NotNull Func<? extends @Nullable T, ?> fetchFunction) {
        return new LazyValueImpl<>(Tuple.of(value), Objects.requireNonNull(fetchFunction), null);
    }

    /**
     * Constructs a <b>push-only</b> lazy value with an initial value.
     *
     * @param value        the initial value
     * @param pushFunction the push function
     * @param <T>          the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> of(@Nullable T value, @NotNull ParamTask<? super @Nullable T, ?> pushFunction) {
        return new LazyValueImpl<>(Tuple.of(value), null, Objects.requireNonNull(pushFunction));
    }

    /**
     * Constructs a <b>fetch-only</b> lazy value that cannot receive external value updates, however it can still be
     * {@link #clear() cleared}.
     *
     * @param fetchFunction the fetch function
     * @param <T>           the value type
     * @return the newly constructed object
     */
    @NotNull
    static <T> LazyValue<T> immutable(@NotNull Func<T, ?> fetchFunction) {
        return new NoSetLazyValue<>(null, Objects.requireNonNull(fetchFunction), null);
    }


    /**
     * Obtains the value. Runs the fetch task in the current thread if no value is present. If another thread is already
     * executing the fetch task, this thread is waits for completion.
     *
     * @return the value
     *
     * @throws NoSuchElementException if no value is available as specified by {@link #isAvailable()}
     * @apiNote {@code null} is also a valid value
     */
    T get() throws NoSuchElementException, SoftInterruptedException;

    /**
     * Returns the value if it is available as specified by {@link #isAvailable()}.
     *
     * @return an expect object wrapping the value
     */
    default Expect<T, RuntimeException> getIfAvailable() {
        return isAvailable() ? Expect.compute(this::get) : Expect.empty();
    }

    /**
     * Returns the value if it is present as specified by {@link #isPresent()}.
     *
     * @return an expect object wrapping the value
     */
    default Expect<T, RuntimeException> getIfPresent() {
        return isPresent() ? Expect.compute(this::get) : Expect.empty();
    }


    /**
     * Deletes the underlying value.
     * <p>
     * Directly executing {@link #isPresent()} after this method will result in {@code false}.
     */
    void clear();

    /**
     * Determines if an underlying value is either present at the time of calling or can be autonomously obtained using
     * an underlying fetching function.
     *
     * @return {@code true} if a value is available, {@code false} otherwise
     */
    boolean isAvailable();

    /**
     * @return {@code true} if a value is currently present, {@code false} otherwise
     */
    boolean isPresent();

    /**
     * @return {@code true} if the underlying fetching function is currently running, {@code false} otherwise
     */
    boolean isRunning();

    /**
     * Sets the underlying value.
     * <p>
     * If a pushing function is present the value will be respectively pushed.
     *
     * @param t the value to set
     * @return the given value
     */
    @Nullable
    @Contract("!null -> !null")
    T set(@Nullable T t);
}
