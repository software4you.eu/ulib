package cc.fluse.ulib.core.impl.patch;

import cc.fluse.ulib.core.function.Task;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.function.Predicate;

/**
 * Enum of patches that can be applied. By default, all applicable patches are applied.
 */
public enum Patch {
    /**
     * The HotswapAgent forcibly defines any plugin class in any classloader, including delegation targets. This will
     * cause multiple definitions of the same class (it's even stated in the comment at the source code), which is
     * problematic. This patch disables that behavior for ulib delegation targets.
     *
     * @see <a href="https://github.com/HotswapProjects/HotswapAgent">HotswapAgent GitHub</a>
     * @see <a
     * href="https://github.com/HotswapProjects/HotswapAgent/blob/5064e775134b7d64c8c2a1110330ab0bf61646e9/hotswap-agent-core/src/main/java/org/hotswap/agent/util/classloader/ClassLoaderDefineClassPatcher.java#L93">Forced,
     * manual class definition source code</a>
     */
    HOTSWAPAGENT_PLUGIN_CLASSLOADER_PATCHER_PATCH(new HotswapAgentPluginClassloaderPatcherPatch());

    private final Task<?> task;

    Patch(Task<?> task) {
        this.task = task;
    }

    Patch() {
        this(() -> {});
    }

    public static void apply(Predicate<Patch> filter) {
        Arrays.stream(Patch.values())
                .filter(filter)
                .forEach(patch -> {
                    try {
                        patch.task.execute();
                    } catch (Exception e) {
                        var bout = new ByteArrayOutputStream();
                        e.printStackTrace(new PrintStream(bout));
                        System.err.printf("(ulib) Failed to apply patch: %s%nCaused by %s%n", patch, bout);
                    }
                });
    }
}
