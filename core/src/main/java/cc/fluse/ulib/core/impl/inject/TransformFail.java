package cc.fluse.ulib.core.impl.inject;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

class TransformFail extends Exception {

    private final String jvmCN;

    TransformFail(String jvmCN, @NotNull Throwable cause) {
        super("Class transform failed (%s): %s".formatted(jvmCN, cause.toString()), cause);
        this.jvmCN = jvmCN;
    }

    public String getJvmCN() {
        return Objects.requireNonNull(jvmCN);
    }
}
