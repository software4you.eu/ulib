package cc.fluse.ulib.core.configuration;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.BiFunction;

import static cc.fluse.ulib.core.configuration.KeyPath.parse;

/**
 * Same as {@link Section}, but with extended getters.
 */
public interface Configuration extends Section {

    /**
     * Uses a function to obtain two values from the supplied configuration instances with the same key and applies a mapping to them.
     *
     * @param c1     the first configuration
     * @param c2     the second configuration
     * @param key    the key
     * @param getter the function that obtains the values corresponding to the ke
     * @param mapper the mapping function
     * @return the mapping result
     */
    @NotNull
    static <C extends Configuration, T, R> Optional<R> map(@NotNull C c1, @NotNull C c2, @NotNull String key,
                                                           @NotNull BiFunction<? super C, String, Optional<T>> getter,
                                                           @NotNull BiFunction<T, T, R> mapper) {
        var val1 = getter.apply(c1, key);
        var val2 = getter.apply(c2, key);

        if (val1.isEmpty() || val2.isEmpty())
            return Optional.empty();

        return Optional.ofNullable(mapper.apply(val1.get(), val2.get()));
    }

    @NotNull
    default <T, R> Optional<R> map(@NotNull Configuration c, @NotNull String key,
                                   @NotNull BiFunction<? super Configuration, String, Optional<T>> getter,
                                   @NotNull BiFunction<T, T, R> mapper) {
        return Configuration.map(this, c, key, getter, mapper);
    }

    /**
     * Searches the configuration for a specific key and converts the value to the specified type.
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or empty optional if {@code path} not found or the value couldn't get converted
     */
    default @NotNull Optional<Object> get(@NotNull String path) {
        return get(parse(path));
    }

    /**
     * Reads a value and attempts to cast it to the given type.
     *
     * @param clazz the type class
     * @param path  the key path; nodes seperated by {@code .}
     * @param <T>   the type
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @throws NumberFormatException    if {@code <T>} is a number type but the read value cannot be parsed to it
     * @throws IllegalArgumentException if read value is not of type {@code <T>}
     * @see String#format(String, Object...)
     */
    default @NotNull <T> Optional<T> get(@NotNull Class<T> clazz, @NotNull String path) {
        return get(clazz, parse(path));
    }

    /**
     * Reads a list or an array and attempts to cast each element to the given type.
     * If not all elements can be cast, an empty optional is returned instead.
     *
     * @param clazz the type class
     * @param path  the key path; nodes seperated by {@code .}
     * @param <T>   the type
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @throws IllegalArgumentException if a entry is not of type {@code <T>}
     */
    @NotNull <T> Optional<List<T>> list(@NotNull Class<T> clazz, @NotNull KeyPath path);

    /**
     * Reads a list or an array and attempts to cast each element to the given type.
     * If not all elements can be cast, an empty optional is returned instead.
     *
     * @param clazz the type class
     * @param path  the key path; nodes seperated by {@code .}
     * @param <T>   the type
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @throws IllegalArgumentException if a entry is not of type {@code <T>}
     */
    default @NotNull <T> Optional<List<T>> list(@NotNull Class<T> clazz, @NotNull String path) {
        return list(clazz, parse(path));
    }

    /**
     * Sets a key to a specific value in the configuration.
     * <p>
     * A {@code null} value removes the key from the configuration.
     * <p>
     * Any previous associated value will be overwritten.
     *
     * @param path  the key path; nodes seperated by {@code .}
     * @param value the value
     * @throws IllegalStateException if the section cannot hold keyed values. This is the case if the sub was directly
     *                               loaded with not-keyed data.
     */
    default void set(@NotNull String path, @Nullable Object value) {
        set(parse(path), value);
    }

    /**
     * Reads a boolean.
     *
     * @param path the key path
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    Optional<Boolean> bool(@NotNull KeyPath path);

    /**
     * Reads a boolean.
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    default Optional<Boolean> bool(@NotNull String path) {
        return bool(parse(path));
    }

    /**
     * Reads a signed 32 bit integer (int).
     *
     * @param path the key path
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    Optional<Integer> int32(@NotNull KeyPath path);

    /**
     * Reads a signed 32 bit integer (int).
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    default Optional<Integer> int32(@NotNull String path) {
        return int32(parse(path));
    }

    /**
     * Reads a signed 64 bit integer (long).
     *
     * @param path the key path
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    Optional<Long> int64(@NotNull KeyPath path);

    /**
     * Reads a signed 64 bit integer (long).
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    default Optional<Long> int64(@NotNull String path) {
        return int64(parse(path));
    }

    /**
     * Reads a signed 32 bit decimal (float).
     *
     * @param path the key path
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    Optional<Float> dec32(@NotNull KeyPath path);

    /**
     * Reads a signed 32 bit decimal (float).
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    default Optional<Float> dec32(@NotNull String path) {
        return dec32(parse(path));
    }

    /**
     * Reads a signed 64 bit decimal (double).
     *
     * @param path the key path
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    Optional<Double> dec64(@NotNull KeyPath path);

    /**
     * Reads a signed 64 bit decimal (double).
     *
     * @param path the key path; nodes seperated by {@code .}
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     */
    @NotNull
    default Optional<Double> dec64(@NotNull String path) {
        return dec64(parse(path));
    }

    /**
     * Reads a String and processes it with {@link String#format(String, Object...)} if {@code replacements} are given.
     *
     * @param path         the key path
     * @param replacements the replacements
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @see String#format(String, Object...)
     */
    @NotNull
    default Optional<String> string(@NotNull KeyPath path, @NotNull Object... replacements) {
        return get(String.class, path)
                .or(() -> get(Number.class, path).map(Number::toString))
                .or(() -> get(Boolean.class, path).map(Object::toString))
                .map(str -> replacements.length > 0 ? str.formatted(replacements) : str);
    }

    /**
     * Reads a String and processes it with {@link String#format(String, Object...)} if {@code replacements} are given.
     *
     * @param path         the key path; nodes seperated by {@code .}
     * @param replacements the replacements
     * @return an optional wrapping the value, or an empty optional if there is no boolean at the path specified
     * @see String#format(String, Object...)
     */
    @NotNull
    default Optional<String> string(@NotNull String path, @NotNull Object... replacements) {
        return string(parse(path), replacements);
    }

    /**
     * Reads a String type List and processes each entry with {@link String#format(String, Object...)}
     * if {@code replacements} are given.
     *
     * @param path         the key path
     * @param replacements the replacements
     * @return the processed string list, or {@code def} if {@code path} does not exist
     * @see String#format(String, Object...)
     */
    @NotNull
    default Optional<List<String>> stringList(@NotNull KeyPath path, @NotNull Object... replacements) {
        return list(String.class, path)
                .map(list -> {
                    if (replacements.length == 0)
                        return list;
                    var res = new ArrayList<String>(list.size());
                    list.forEach(str -> {
                        res.add(str.formatted(replacements));
                    });
                    return res;
                });
    }

    /**
     * Reads a String type List and processes each entry with {@link String#format(String, Object...)}
     * if {@code replacements} are given.
     *
     * @param path         the key path; nodes seperated by {@code .}
     * @param replacements the replacements
     * @return the processed string list, or {@code def} if {@code path} does not exist
     * @see String#format(String, Object...)
     */
    @NotNull
    default Optional<List<String>> stringList(@NotNull String path, @NotNull Object... replacements) {
        return stringList(parse(path), replacements);
    }

    @Override
    @NotNull Configuration getRoot();

    @Override
    @NotNull Optional<? extends Configuration> getSubsection(@NotNull KeyPath path);

    @Override
    @NotNull Collection<? extends Configuration> getSubsections(boolean deep);

    @Override
    @NotNull Configuration createSubsection(@NotNull KeyPath path);

    @Override
    @NotNull Configuration subAndCreate(@NotNull KeyPath path);
}
