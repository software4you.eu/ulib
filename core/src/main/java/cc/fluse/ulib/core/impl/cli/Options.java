package cc.fluse.ulib.core.impl.cli;

import cc.fluse.ulib.core.cli.*;
import cc.fluse.ulib.core.cli.ex.*;
import cc.fluse.ulib.core.function.*;
import org.davidmoten.text.utils.WordWrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;


public class Options implements CliOptions {

    private final Set<Option> options = new LinkedHashSet<>();

    @Override
    public @NotNull Optional<CliOption> getOption(@NotNull String name) {
        //noinspection unchecked,rawtypes
        return (Optional) options.stream()
                                 .filter(opt -> opt.getName().equals(name))
                                 .findFirst();
    }

    @Override
    public @NotNull Optional<CliOption> getOption(char shortName) {
        //noinspection unchecked,rawtypes
        return (Optional) options.stream()
                                 .filter(opt -> {
                                     for (char name : opt.getShortNames()) {
                                         if (name == shortName) {
                                             return true;
                                         }
                                     }
                                     return false;
                                 })
                                 .findFirst();
    }

    @Override
    public @NotNull CliOption option(@NotNull String name, char @NotNull [] shortNames, @NotNull String description, @NotNull ArgProps argProps) {

        if (getOption(name).isPresent()) {
            throw new IllegalArgumentException("option name '%s' already known".formatted(name));
        }

        for (char sn : shortNames) {
            if (getOption(sn).isPresent()) {
                throw new IllegalArgumentException("option short name '%s' already known".formatted(sn));
            }
        }

        var opt = new Option(this, description, name, shortNames, argProps);
        options.add(opt);
        return opt;
    }

    @SuppressWarnings({"unchecked", "deprecation", "rawtypes"})
    public <X0 extends Exception, X1 extends Exception, X2 extends Exception, X3 extends Exception>
    void printHelp(@NotNull ParamTask<@NotNull String, X0> printer, int pfxSpacing, int nameWidth, int spacing, int descWidth,
                   @NotNull ParamFunc<@NotNull ArgProps, @Nullable String, X1> argStr,
                   @NotNull ParamFunc<@NotNull Set<@NotNull Set<@NotNull CliOption>>, @Nullable String, X2> requiresStr,
                   @NotNull ParamFunc<@NotNull Set<@NotNull CliOption>, @Nullable String, X3> incompatiblesStr)
    throws X0, X1, X2, X3 {

        var it = options.iterator();
        while (it.hasNext()) {
            var opt = it.next();

            var shortNames = new StringJoiner(", -", ", -", "");
            shortNames.setEmptyValue("");
            for (char c : opt.getShortNames()) {
                shortNames.add(String.valueOf(c));
            }


            var firstLine = new StringBuilder();
            // spacing
            if (pfxSpacing > 0) firstLine.append(("%" + pfxSpacing + "s").formatted(""));
            // long and short names
            firstLine.append(("%-" + nameWidth + "s").formatted("--" + opt.getName() + shortNames));


            // desc
            var descBuilder = new StringBuilder();
            descBuilder.append(opt.getDescription());

            // arg props
            Optional.ofNullable(argStr.apply(opt.getArgProps()))
                    .ifPresent(str -> descBuilder.append(System.lineSeparator()).append(str));

            // requires
            if (!opt.getRequires().isEmpty()) {
                Optional.ofNullable(requiresStr.apply((Set) opt.getRequires()))
                        .ifPresent(str -> descBuilder.append(System.lineSeparator()).append(str));
            }
            // incompatibles
            if (!opt.getIncompatible().isEmpty()) {
                Optional.ofNullable(incompatiblesStr.apply((Set) opt.getIncompatible()))
                        .ifPresent(str -> descBuilder.append(System.lineSeparator()).append(str));
            }

            var descLines = WordWrap.from(descBuilder.toString())
                                    .maxWidth(descWidth)
                                    .breakWords(false)
                                    .wrapToList();

            if (!descLines.isEmpty()) {
                // spacing between names & desc
                if (spacing > 0) firstLine.append(("%" + spacing + "s").formatted(""));
                // first desc line
                firstLine.append(descLines.get(0));
            }

            printer.execute(firstLine.toString());

            // print rest of desc lines
            for (String line : descLines.subList(1, descLines.size())) {
                printer.execute(("%" + (pfxSpacing + nameWidth + spacing) + "s%s").formatted("", line));
            }

            // print newline
            if (it.hasNext()) {
                printer.execute("");
            }
        }

    }

    @Override
    public @NotNull CliArgs parse(String @NotNull [] cliArgs) throws OptionException {
        var it = Spliterators.iterator(Arrays.spliterator(cliArgs));

        // parse options
        var opts = parseOptionWithArgs(it);

        // collect parameters
        var plainParams = new LinkedList<String>();
        it.forEachRemaining(plainParams::add);

        // validate option-set
        opts.forEach(BiParamTask.as((opt, args) -> {

            // check requirements
            if (!opt.getRequires().isEmpty()) {
                var anyOf = opt.getRequires();
                if (anyOf.stream().noneMatch(allOf -> allOf.stream().allMatch(opts::containsKey))) {
                    throw new InvalidOptionSetException(opt, null, "invalid option set, see help");
                }
            }

            // check incompatibilities
            for (Option unallowed : opt.getIncompatible()) {
                if (opts.containsKey(unallowed)) {
                    throw new InvalidOptionSetException(opt, unallowed, "may not be combined with '%s'".formatted(unallowed.getName()));
                }
            }

            // check option arguments
            var props = opt.getArgProps();
            if (args.isEmpty() && !props.allowsArgs(0)) {

                // check for default arg
                var def = props.defaultArguments();
                if (!def.isEmpty()) {
                    // put default
                    args.addAll(def);
                }
            }

            if (!props.allowsArgs(args.size())) {
                throw new IllegalOptionArgumentException(opt, "has an invalid amount of arguments (%d: %s)".formatted(args.size(), Arrays.toString(args.toArray())));
            }

        }));

        return new ParsedArgs(this, Collections.unmodifiableMap(opts), Collections.unmodifiableCollection(plainParams));
    }

    private LinkedHashMap<Option, Collection<String>> parseOptionWithArgs(Iterator<String> it) throws
                                                                                               NoSuchOptionException,
                                                                                               IllegalOptionArgumentException {
        var opts = new LinkedHashMap<Option, Collection<String>>();

        Option last = null;
        while (it.hasNext()) {
            var optString = it.next();

            // options end?
            if (optString.equals("--")) {
                break;
            }

            // single option
            if (optString.startsWith("--")) {
                var name = optString.substring(2);
                last = (Option) getOption(name)
                        .orElseThrow(() -> new NoSuchOptionException(name));
                opts.putIfAbsent(last, new LinkedList<>());
                continue;
            }

            // multiple options
            if (optString.startsWith("-")) {
                for (char shortName : optString.substring(1).toCharArray()) {
                    last = (Option) getOption(shortName)
                            .orElseThrow(() -> new NoSuchOptionException(String.valueOf(shortName)));
                    opts.putIfAbsent(last, new LinkedList<>());
                }
                continue;
            }

            // option argument
            if (last == null) {
                throw new IllegalOptionArgumentException("Option argument without prior option");
            }
            opts.get(last).add(optString);
        }

        return opts;
    }

}
