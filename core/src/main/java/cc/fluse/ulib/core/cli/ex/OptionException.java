package cc.fluse.ulib.core.cli.ex;

import cc.fluse.ulib.core.cli.CliOption;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class OptionException extends Exception {
    @Nullable
    private final CliOption option;

    public OptionException(@Nullable CliOption option, @NotNull String message) {
        super(message);
        this.option = option;
    }

    public OptionException(@Nullable CliOption option, @NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
        this.option = option;
    }

    @NotNull
    public Optional<CliOption> getOption() {
        return Optional.ofNullable(option);
    }
}
