package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.function.TriParamTask;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import cc.fluse.ulib.core.impl.tuple.BiMapImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * A map that maps keys to pairs of values.
 *
 * @param <K>  the type of keys
 * @param <V1> the type of the first values
 * @param <V2> the type of the second values
 */
@BypassAnnotationEnforcement
public interface BiMap<K, V1, V2> extends Map<K, Pair<V1, V2>> {

    /**
     * Returns a {@code BiMap} backed by the specified {@code map}.
     *
     * @param map the map backing the returned {@code BiMap}
     * @return the new map
     */
    static @NotNull <K, V1, V2> BiMap<K, V1, V2> wrap(@NotNull Map<K, Pair<V1, V2>> map) {
        return new BiMapImpl<>(map);
    }

    @SafeVarargs
    static @NotNull <K, V1, V2> BiMap<K, V1, V2> ofEntries(Triple<K, V1, V2>... entries) {
        var ents = Arrays.stream(entries)
                .map(t -> Map.entry(t.getFirst(), Tuple.of(t.getSecond(), t.getThird())));
        //noinspection unchecked
        return wrap(Map.ofEntries(ents.toArray(Map.Entry[]::new)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5,
                                                    K k6, V1 v1_6, V2 v2_6) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5), k6, Tuple.of(v1_6, v2_6)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5,
                                                    K k6, V1 v1_6, V2 v2_6,
                                                    K k7, V1 v1_7, V2 v2_7) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5), k6, Tuple.of(v1_6, v2_6), k7, Tuple.of(v1_7, v2_7)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5,
                                                    K k6, V1 v1_6, V2 v2_6,
                                                    K k7, V1 v1_7, V2 v2_7,
                                                    K k8, V1 v1_8, V2 v2_8) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5), k6, Tuple.of(v1_6, v2_6), k7, Tuple.of(v1_7, v2_7), k8, Tuple.of(v1_8, v2_8)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5,
                                                    K k6, V1 v1_6, V2 v2_6,
                                                    K k7, V1 v1_7, V2 v2_7,
                                                    K k8, V1 v1_8, V2 v2_8,
                                                    K k9, V1 v1_9, V2 v2_9) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5), k6, Tuple.of(v1_6, v2_6), k7, Tuple.of(v1_7, v2_7), k8, Tuple.of(v1_8, v2_8), k9, Tuple.of(v1_9, v2_9)));
    }

    static @NotNull <K, V1, V2> BiMap<K, V1, V2> of(K k1, V1 v1_1, V2 v2_1,
                                                    K k2, V1 v1_2, V2 v2_2,
                                                    K k3, V1 v1_3, V2 v2_3,
                                                    K k4, V1 v1_4, V2 v2_4,
                                                    K k5, V1 v1_5, V2 v2_5,
                                                    K k6, V1 v1_6, V2 v2_6,
                                                    K k7, V1 v1_7, V2 v2_7,
                                                    K k8, V1 v1_8, V2 v2_8,
                                                    K k9, V1 v1_9, V2 v2_9,
                                                    K k10, V1 v1_10, V2 v2_10) {
        return wrap(Map.of(k1, Tuple.of(v1_1, v2_1), k2, Tuple.of(v1_2, v2_2), k3, Tuple.of(v1_3, v2_3), k4, Tuple.of(v1_4, v2_4), k5, Tuple.of(v1_5, v2_5), k6, Tuple.of(v1_6, v2_6), k7, Tuple.of(v1_7, v2_7), k8, Tuple.of(v1_8, v2_8), k9, Tuple.of(v1_9, v2_9), k10, Tuple.of(v1_10, v2_10)));
    }

    /**
     * Returns the first value to which the specified key is mapped, or {@code null} if this map contains no mapping for
     * the key.
     *
     * @param key the key whose associated value is to be returned
     * @return the first value to which the specified key is mapped, or {@code null} if this map contains no mapping for
     * the key
     */
    default V1 getFirst(@NotNull Object key) {
        return Optional.ofNullable(get(key)).map(Pair::getFirst).orElse(null);
    }

    /**
     * Returns the second value to which the specified key is mapped, or {@code null} if this map contains no mapping
     * for the key.
     *
     * @param key the key whose associated value is to be returned
     * @return the second value to which the specified key is mapped, or {@code null} if this map contains no mapping
     * for the key
     */
    default V2 getSecond(@NotNull Object key) {
        return Optional.ofNullable(get(key)).map(Pair::getSecond).orElse(null);
    }

    /**
     * Associates the specified pair of values with the specified key in this map. If the map previously contained a
     * mapping for the key, the old pair of values is replaced by the specified pair of values.
     *
     * @param key    the key with which the specified pair of values is to be associated
     * @param value1 the first value to be associated with the specified key
     * @param value2 the second value to be associated with the specified key
     * @return the previous pair of values associated with {@code key}, or {@code null} if there was no mapping for
     * {@code key}.
     */
    default Pair<V1, V2> put(@NotNull K key, @Nullable V1 value1, @Nullable V2 value2) {
        return put(key, Tuple.of(value1, value2));
    }

    default Pair<V1, V2> putFirst(@NotNull K key, @Nullable V1 value) {
        return put(key, value, getSecond(key));
    }

    default Pair<V1, V2> putSecond(@NotNull K key, @Nullable V2 value) {
        return put(key, getFirst(key), value);
    }

    /**
     * Copies all of the mappings from the specified map to this map.
     *
     * @param m the map to be stored in this map
     */
    default void putAll(@NotNull BiMap<? extends K, ? extends V1, ? extends V2> m) {
        m.forEach((k, v1, v2) -> put(k, v1, v2));
    }

    /**
     * Performs the given action for each entry in this map until all entries have been processed or the action throws
     * an exception. An exception won't stop the iteration, but will be thrown after the iteration has finished, other
     * exceptions will be {@link Exception#addSuppressed(Throwable) suppressed}.
     *
     * @param action the action to be performed for each entry
     * @param <X>    the type of the exception thrown by the action
     * @throws X if the action throws an exception
     */
    default <X extends Exception> void forEach(TriParamTask<? super K, ? super V1, ? super V2, X> action) throws X {
        Exception ex = null;
        for (var en : entrySet()) {
            try {
                action.execute(en.getKey(), en.getValue().getFirst(), en.getValue().getSecond());
            } catch (Exception e) {
                if (ex == null) ex = e;
                else ex.addSuppressed(e);
            }
        }
        if (ex == null) return;
        if (ex instanceof RuntimeException re) throw re;
        throw (X) ex; // just hope the provided task will only throw RuntimeException or {X}
    }

    /**
     * Checks if the specified value is present at least once in this map. The value can be either a first or a second
     * value.
     *
     * @param value value whose presence in this map is to be tested
     * @return {@code true} if the specified value is present at least once in this map, {@code false} otherwise
     */
    @Override
    default boolean containsValue(Object value) {
        return values().stream().anyMatch(pair -> pair.getFirst().equals(value) || pair.getSecond().equals(value));
    }

    /**
     * Checks if the specified value is present as a first value in this map at least once.
     *
     * @param value value whose presence in this map is to be tested
     * @return {@code true} if the specified value is present as a first value in this map at least once, {@code false}
     */
    default boolean containsValue1(V1 value) {
        return values().stream().anyMatch(pair -> pair.getFirst().equals(value));
    }

    /**
     * Checks if the specified value is present as a second value in this map at least once.
     *
     * @param value value whose presence in this map is to be tested
     * @return {@code true} if the specified value is present as a second value in this map at least once, {@code false}
     */
    default boolean containsValue2(V2 value) {
        return values().stream().anyMatch(pair -> pair.getSecond().equals(value));
    }

    default Stream<Triple<K, V1, V2>> stream() {
        return entrySet().stream().map(en -> Tuple.of(en.getKey(), en.getValue().getFirst(), en.getValue().getSecond()));
    }
}
