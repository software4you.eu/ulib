package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.util.Pool.PoolElement;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.util.Collection;
import java.util.Optional;

/**
 * Representation of sql type databases.
 */
public interface SqlDatabase extends Database {

    /**
     * Retrieves a connection from the database.
     *
     * @return the connection
     * @throws IllegalStateException if no connection was established yet
     */
    @NotNull
    PoolElement<Connection> getConnection() throws IllegalStateException;

    /**
     * Returns this database's tables (the wrapper knows of).
     *
     * @return the tables
     */
    @NotNull
    Collection<Table> getTables();

    /**
     * Searches for a table within the database.
     *
     * @param name the table name
     * @return the table instance, or {@code null} if not found
     */
    @NotNull
    Optional<Table> getTable(@NotNull String name);

    /**
     * Adds a new table to this wrapper.
     *
     * @param name    the name
     * @param column  the 1st column
     * @param columns other columns
     * @return the table
     * @see ColumnBuilder
     */
    @NotNull
    Table addTable(@NotNull String name, @NotNull Column<?> column, @NotNull Column<?>... columns);

    /**
     * Adds a new table to this wrapper.
     *
     * @param name     the name
     * @param builder  the 1st column
     * @param builders other columns
     * @return the table
     * @see ColumnBuilder
     */
    @NotNull
    Table addTable(@NotNull String name, @NotNull ColumnBuilder<?> builder, @NotNull ColumnBuilder<?>... builders);
}
