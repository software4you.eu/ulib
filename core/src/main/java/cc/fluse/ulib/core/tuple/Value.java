package cc.fluse.ulib.core.tuple;

import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;

@BypassAnnotationEnforcement
public interface Value<T> extends Tuple<Object> {
    T getFirst();
}
