package cc.fluse.ulib.core.impl.io;

import cc.fluse.ulib.core.function.BiParamFunc;
import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.BypassAnnotationEnforcement;
import org.jetbrains.annotations.NotNull;

import java.io.EOFException;
import java.util.BitSet;

public final class VarWord {

    /**
     * Reads a variable length word, consisting out of blocks subsequently followed by an indicator bit, indicating if
     * there is a next block in the current word.
     * <p>
     * The read bits will be stored in the buffer.
     *
     * @param blockLen the block bit-length (excluding the indicator bit)
     * @param nbits    the maximum bits to write to the buffer
     * @param supplier the bit supplier
     * @throws IllegalStateException if the bit maximum is exceeded
     */
    @NotNull
    public static <X extends Exception> BitSet readVarWord(int blockLen, int nbits, @NotNull BitSupplier<X> supplier)
    throws X {
        if (blockLen < 1 || nbits < 1) {
            throw new IndexOutOfBoundsException();
        }

        var buffer = new BitSet();
        int cursor = 0;

        try {
            do {
                if (cursor > nbits) {
                    throw new IllegalArgumentException("Malformed VarWord: Bit maximum of %d exceeded (%d)".formatted(nbits, cursor));
                }

                // read and push bits for the current block
                for (int i = 0; i < blockLen; i++) {
                    // check if the bit in word is set
                    if (supplier.isNextBitSet()) {
                        buffer.set(cursor);
                    }
                    cursor++;
                }

            } while (supplier.isNextBitSet() /* check indicator bit for next block*/);
        } catch (EOFException e) {
            throw new IllegalArgumentException("EOF after %d bits".formatted(cursor));
        }

        return buffer;
    }

    /**
     * Writes a variable length word to a consumer.
     *
     * @param blockLen the block bit-length (excluding the indicator bit)
     * @param buffer   the buffer to read the bits from
     * @param consumer the bit consumer function
     * @return the amount of bits written tto the consumer (will always be a multiple of [blockLen + 1])
     */
    public static <X extends Exception> int writeVarWord(int blockLen, @NotNull BitSet buffer, @NotNull BitConsumer<X> consumer)
    throws X {
        if (blockLen < 1) {
            throw new IndexOutOfBoundsException();
        }

        int bitsWritten = 0;
        int cursor = 0;
        final int bufferLen = buffer.length();

        while (true) {
            // push bits for the current block
            for (int i = 0; i < blockLen; i++) {
                // bitsWritten is equal to index at this point (before increment)
                consumer.putNextBit(buffer.get(cursor++));
                bitsWritten++;
            }

            // write indicator bit
            bitsWritten++;
            if (cursor < bufferLen) {
                // there is a next block
                consumer.putNextBit(true);
                continue;
            }
            // no next block
            consumer.putNextBit(false);
            break;
        }

        consumer.flush();
        return bitsWritten;
    }

    /**
     * Generates a bitset up to n bits from a bit index checking function.
     *
     * @param nbits        the maximum bits (inclusive)
     * @param bitIndexFunc a function to determine if the n-th bit is set
     * @return the generated bitset.
     */
    @NotNull
    public static <X extends Exception> BitSet encode(int nbits, @NotNull ParamFunc<Integer, Boolean, X> bitIndexFunc)
    throws X {
        var set = new BitSet(nbits);
        for (int i = 0; i < nbits; i++) {
            if (bitIndexFunc.execute(i)) {
                set.set(i);
            }
        }
        return set;
    }

    @BypassAnnotationEnforcement
    public static <T, X extends Exception> T decode(@NotNull BitSet buffer, T neutral,
                                                    BiParamFunc<T, Integer, T, X> bitIndexAccumulator) throws X {

        T t = neutral;
        var bufferLen = buffer.length();

        for (int i = 0; i < bufferLen; i++) {
            if (buffer.get(i)) {
                t = bitIndexAccumulator.execute(t, i);
            }
        }

        return t;
    }


}
