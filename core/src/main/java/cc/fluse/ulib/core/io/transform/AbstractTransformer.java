package cc.fluse.ulib.core.io.transform;

import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.Bouncer.StateBouncer;
import cc.fluse.ulib.core.io.channel.PipeBufferChannel;
import cc.fluse.ulib.core.io.transform.ex.IOTransformException;
import cc.fluse.ulib.core.io.transform.ex.InputFormatException;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;

import static cc.fluse.ulib.core.util.Semantics.nofail;

/**
 * An implementation base of {@link ByteTransformer}.
 */
public abstract class AbstractTransformer implements ByteTransformer {

    private final ChBouncer bouncer = Bouncer.ch();
    private final StateBouncer completeBouncer = Bouncer.is("completed");
    private final PipeBufferChannel resultBuffer = new PipeBufferChannel();

    protected AbstractTransformer() {
    }

    protected final int writeResult(@NotNull ByteBuffer src) {
        return nofail(ClosedChannelException.class, () -> resultBuffer.write(src));
    }

    @Override
    public final int write(@NotNull ByteBuffer src) throws InputFormatException, ClosedChannelException {
        bouncer.pass();
        completeBouncer.pass();
        int pos = src.position();
        implWrite(src);
        return src.position() - pos;
    }

    @Override
    public final int read(@NotNull ByteBuffer dst) throws ClosedChannelException {
        bouncer.pass();
        return resultBuffer.read(dst);
    }

    @Override
    public final void close() {
        if (isClosed()) return;
        try {
            implClose();
        } finally {
            resultBuffer.close();
            bouncer.block();
        }
    }

    @Override
    public final void complete() throws IOTransformException, ClosedChannelException {
        bouncer.pass();
        if (isCompleted()) return;
        implComplete();
        completeBouncer.block();
    }

    @Override
    public final boolean isCompleted() {
        return !completeBouncer.canPass();
    }

    /**
     * @implSpec Overriding must be in form of <code>{@code return super.isClosed() || ...;}</code>
     */
    @Override
    @MustBeInvokedByOverriders
    public boolean isOpen() {
        return bouncer.canPass();
    }

    /**
     * @implSpec Overriding must be in form of <code>{@code return super.holdsData() || ...;}</code>
     */
    @Override
    @MustBeInvokedByOverriders
    public boolean holdsData() throws ClosedChannelException {
        bouncer.pass();
        return resultBuffer.holds();
    }

    /**
     * @implNote This method is only invoked while the channel is open.
     * @implSpec transforms the given bytes and writes the result with {@link #writeResult(ByteBuffer)}. If an exception
     * is thrown, no bytes must be written.
     */
    protected abstract void implWrite(@NotNull ByteBuffer src) throws InputFormatException;

    /**
     * @throws IOTransformException if completing fails
     * @implNote This method is only invoked while the channel is open. If an exception is thrown, the transformer is
     * <b>not</b> marked as completed and the exception is propagated to the caller.
     * @implSpec Attempts to complete the transformer, may throw an {@link IOTransformException} if completing fails.
     * Resulting bytes must be written with {@link #writeResult(ByteBuffer)}.
     */
    protected abstract void implComplete() throws IOTransformException;

    /**
     * @implNote This method is only invoked while the channel is open. Default implementation does nothing.
     * @implSpec Closes the transformer and discards any buffered data. This method must not fail.
     */
    protected void implClose() {
        // nop
    }
}
