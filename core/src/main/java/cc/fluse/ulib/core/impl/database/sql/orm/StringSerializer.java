package cc.fluse.ulib.core.impl.database.sql.orm;

import cc.fluse.ulib.core.function.ParamFunc;

public class StringSerializer<T> extends SimpleSerializer<T, String> {
    public StringSerializer(Class<T> javaType,
                            ParamFunc<? super T, ? extends String, ? extends SerializerException> serializer,
                            ParamFunc<? super String, ? extends T, ? extends SerializerException> deserializer) {
        super(javaType, String.class, serializer, deserializer);
    }
}
