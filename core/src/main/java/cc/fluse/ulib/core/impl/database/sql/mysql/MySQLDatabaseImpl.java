package cc.fluse.ulib.core.impl.database.sql.mysql;

import cc.fluse.ulib.core.database.sql.Column;
import cc.fluse.ulib.core.database.sql.MySQLDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlDatabase.DBImpl;
import cc.fluse.ulib.core.impl.database.sql.AbstractSqlTable;

import java.sql.Connection;
import java.util.Collection;
import java.util.Properties;

@DBImpl(names = "mysql",
        driverCoords = "{{maven.mysql}}",
        protocol = "jdbc:mysql://",
        supportsConnectionPooling = true)
public final class MySQLDatabaseImpl extends AbstractSqlDatabase implements MySQLDatabase {
    public MySQLDatabaseImpl(Connection connection) {
        super(connection);
    }

    public MySQLDatabaseImpl(String url, Properties info, int maxPoolSize) {
        super(url, info, maxPoolSize);
    }

    @Override
    protected AbstractSqlTable newTable(String name, Collection<Column<?>> columns) {
        return new MySQLTable(this, name, columns);
    }
}
