package cc.fluse.ulib.core.impl.io.datachannel;

import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.ReadableDataChannel;
import org.jetbrains.annotations.MustBeInvokedByOverriders;
import org.jetbrains.annotations.NotNull;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.Channels;

/**
 * Abstract base class for {@link ReadableDataChannel}s.
 *
 * @implSpec Implementations must override {@link #isOpen()} and {@link #close()}.
 */
public abstract class AbstractReadableDataChannel implements ReadableDataChannel {

    protected final ChBouncer bouncer = Bouncer.ch();
    protected final DataInputStream din;

    protected AbstractReadableDataChannel() {
        this.din = new DataInputStream(Channels.newInputStream(this));
    }

    @Override
    @MustBeInvokedByOverriders
    public boolean isOpen() {
        return bouncer.canPass();
    }

    @Override
    @MustBeInvokedByOverriders
    public void close() throws IOException {
        bouncer.close();
    }

    @Override
    public @NotNull InputStream streamRead() throws IOException {
        bouncer.pass();
        return IOUtil.isolate(din);
    }

    @Override
    public @NotNull ReadableDataChannel channelRead() throws IOException {
        bouncer.pass();
        return ReadableDataChannel.super.channelRead();
    }

    @Override
    public @NotNull ReadableDataChannel channel() throws IOException {
        bouncer.pass();
        return ReadableDataChannel.super.channel();
    }

    // delegate DataInput



    @Override
    public void readFully(@NotNull byte[] b) throws IOException {
        bouncer.pass();
        din.readFully(b);
    }

    @Override
    public void readFully(@NotNull byte[] b, int off, int len) throws IOException {
        bouncer.pass();
        din.readFully(b, off, len);
    }

    @Override
    public int skipBytes(int n) throws IOException {
        bouncer.pass();
        return din.skipBytes(n);
    }

    @Override
    public boolean readBoolean() throws IOException {
        bouncer.pass();
        return din.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        bouncer.pass();
        return din.readByte();
    }

    @Override
    public int readUnsignedByte() throws IOException {
        bouncer.pass();
        return din.readUnsignedByte();
    }

    @Override
    public short readShort() throws IOException {
        bouncer.pass();
        return din.readShort();
    }

    @Override
    public int readUnsignedShort() throws IOException {
        bouncer.pass();
        return din.readUnsignedShort();
    }

    @Override
    public char readChar() throws IOException {
        bouncer.pass();
        return din.readChar();
    }

    @Override
    public int readInt() throws IOException {
        bouncer.pass();
        return din.readInt();
    }

    @Override
    public long readLong() throws IOException {
        bouncer.pass();
        return din.readLong();
    }

    @Override
    public float readFloat() throws IOException {
        bouncer.pass();
        return din.readFloat();
    }

    @Override
    public double readDouble() throws IOException {
        bouncer.pass();
        return din.readDouble();
    }

    @Override
    public String readLine() throws IOException {
        bouncer.pass();
        return din.readLine();
    }

    @NotNull
    @Override
    public String readUTF() throws IOException {
        bouncer.pass();
        return din.readUTF();
    }
}
