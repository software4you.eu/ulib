package cc.fluse.ulib.core.io.channel;

import cc.fluse.ulib.core.io.Bouncer;
import cc.fluse.ulib.core.io.Bouncer.ChBouncer;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.Readable;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.InvalidMarkException;

/**
 * An input stream backed by a byte buffer. This class is the read-only version of {@link PipeBufferChannel}.
 * <p>
 * This class is thread-safe.
 *
 * @see PipeBufferChannel
 */
public class ByteBufferReadChannel extends InputStream implements ReadableDataChannel, Readable {

    private final ByteBuffer buffer;
    private final ChBouncer bouncer = Bouncer.ch();

    /**
     * @param buffer the backing buffer
     */
    public ByteBufferReadChannel(@NotNull ByteBuffer buffer) {
        this.buffer = buffer.duplicate();
    }

    @Override
    public synchronized boolean isOpen() {
        return bouncer.canPass();
    }

    /**
     * Closes this channel and releases any system resources associated with it that can be released. Invoking this
     * method more than once has no effect.
     */
    @Override
    public synchronized void close() {
        if (!isOpen()) return;
        bouncer.block();
        if (buffer.isReadOnly()) return;
        buffer.clear();
    }

    @Override
    public synchronized int read() throws IOException {
        bouncer.pass();
        return buffer.hasRemaining() ? Byte.toUnsignedInt(buffer.get()) : -1;
    }

    @Override
    public synchronized int read(@NotNull ByteBuffer dst) throws IOException {
        bouncer.pass();
        if (!buffer.hasRemaining()) return -1;

        int i = 0;
        while (buffer.hasRemaining() && dst.hasRemaining()) {
            dst.put(buffer.get());
        }
        return i;
    }

    @Override
    public synchronized int available() throws IOException {
        return buffer.remaining();
    }

    @Override
    public synchronized void mark(int readlimit) {
        buffer.mark();
    }

    @Override
    public synchronized void reset() throws IOException {
        bouncer.pass();
        try {
            buffer.reset();
        } catch (InvalidMarkException e) {
            buffer.rewind();
        }
    }

    @Override
    public boolean markSupported() {
        return true;
    }

    @Override
    public @NotNull InputStream streamRead() {
        return IOUtil.isolate((InputStream) this);
    }

    @Override
    public @NotNull ReadableDataChannel channel() {
        return IOUtil.isolate((ReadableDataChannel) this);
    }
}
