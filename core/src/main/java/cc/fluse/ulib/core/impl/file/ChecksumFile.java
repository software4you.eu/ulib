package cc.fluse.ulib.core.impl.file;

import cc.fluse.ulib.core.function.*;
import cc.fluse.ulib.core.util.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.nio.file.*;
import java.security.MessageDigest;
import java.util.concurrent.CompletableFuture;

public class ChecksumFile extends CachedFileImpl {

    private final Func<MessageDigest, ?> digestSupplier;

    private final ByteBuffer expectedDigest;

    private final CachedFileImpl checksumFile;
    private final LazyValue<ByteBuffer> checksum;
    private final Object $sumLock = new Object[0];

    public ChecksumFile(@Nullable String pfx, @NotNull Path path, @NotNull ParamTask<@NotNull OutputStream, ?> creationTask,
                        Func<MessageDigest, ?> digestSupplier, @Nullable ByteBuffer expectedDigest) {
        super(pfx, path.resolve(path.getFileName()), creationTask);
        this.digestSupplier = digestSupplier;

        this.expectedDigest = expectedDigest;

        this.checksumFile = new CachedFileImpl(pfx, path.resolve(path.getFileName().toString() + ".sum"),
                                               this::writeSum);
        this.checksum = LazyValue.of(() -> {
            if (expectedDigest != null) return expectedDigest;
            if (!checksumFile.readReady()) throw new IllegalStateException();
            synchronized ($sumLock) {
                return checksumFile.readAll().join();
            }
        });
    }

    private void writeSum(OutputStream out) {
        synchronized ($sumLock) {
            checksum.clear();
            Expect.compute(() -> {
                try (var ch = Files.newByteChannel(checksumFile.getLocation(), StandardOpenOption.WRITE)) {
                    ch.write(computeDigest(true));
                }
            }).rethrowRE();
        }
    }

    private ByteBuffer computeDigest(boolean hex) {
        return Expect.compute(() -> {
            try (var in = Files.newInputStream(super.path)) {
                var digest = HashUtil.computeHash(in, digestSupplier.execute());
                return ByteBuffer.wrap(hex ? Conversions.toHex(digest, false).getBytes() : digest);
            }
        }).orElseThrow();
    }

    // redo sum on write

    @Override
    public @NotNull OutputStream streamWrite() {
        return new FilterOutputStream(super.streamWrite()) {
            @Override
            public void close() throws IOException {
                try {
                    super.close();
                } finally {
                    checksumFile.populate().join();
                }
            }
        };
    }

    @Override
    public @NotNull CompletableFuture<Void> streamWrite(@NotNull ParamTask<? super @NotNull OutputStream, ?> writer) {
        return super.streamWrite(writer).thenRunAsync(() -> checksumFile.populate().join());
    }

    @Override
    public @NotNull WritableByteChannel channelWrite() {
        return new WritableByteChannel() {
            private final WritableByteChannel ch = ChecksumFile.super.channelWrite();

            @Override
            public int write(ByteBuffer src) throws IOException {
                return ch.write(src);
            }

            @Override
            public boolean isOpen() {
                return ch.isOpen();
            }

            @Override
            public void close() throws IOException {
                try {
                    ch.close();
                } finally {
                    checksumFile.populate().join();
                }
            }
        };
    }

    @Override
    public @NotNull CompletableFuture<Void> channelWrite(@NotNull ParamTask<? super @NotNull WritableByteChannel, ?> writer) {
        return super.channelWrite(writer).thenRunAsync(() -> checksumFile.populate().join());
    }

    @Override
    public synchronized @NotNull CompletableFuture<Void> populate() {
        return super.populate().thenRunAsync((Task<?>) () -> {
            ByteBuffer actual;
            if (expectedDigest != null && (actual = computeDigest(false)).mismatch(expectedDigest) != -1) {
                throw new IOException("Digest mismatch, expected '%s' but got '%s'".formatted(
                        Conversions.toHex(expectedDigest.array(), false),
                        Conversions.toHex(actual.array(), false)));
            }
            checksumFile.populate().join();
        });
    }

    // purge sum file and parent dir as well on regular purge

    @Override
    public @NotNull CompletableFuture<Void> purge() {
        return super.purge() // purge bin file
                    // purge sum file & dir
                    .thenRunAsync((Task<?>) () -> {
                        synchronized ($sumLock) {
                            checksum.clear();
                            checksumFile.purge().join();
                            Files.deleteIfExists(super.path.getParent());
                        }
                    });
    }

    @Override
    protected boolean readReady() {
        return super.readReady() // file exist?
               // compare hashes
               && checksum.get().mismatch(computeDigest(expectedDigest == null)) == -1;
    }
}
