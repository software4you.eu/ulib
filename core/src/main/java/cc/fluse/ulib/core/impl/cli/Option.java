package cc.fluse.ulib.core.impl.cli;

import cc.fluse.ulib.core.cli.ArgProps;
import cc.fluse.ulib.core.cli.CliOption;
import cc.fluse.ulib.core.util.Conversions;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;


@Getter
@RequiredArgsConstructor
public class Option implements CliOption {
    private final Options parent;

    private final String description;
    private final String name;
    private final char[] shortNames;

    private final ArgProps argProps;

    // option sets that *this* option requires one of
    private final Set<Set<Option>> requires = new LinkedHashSet<>();

    // Options that *this* options does not allow with
    private final Set<Option> incompatible = new LinkedHashSet<>();

    @Override
    public char @NotNull [] getShortNames() {
        return shortNames.clone();
    }

    @Override
    public void requires(@NotNull CliOption @NotNull [] @NotNull [] anyOf) {
        var sets = Arrays.stream(anyOf)
                         .map(allOf -> {
                             var set = Conversions.safecast(Option.class, Set.of(allOf))
                                                  .orElseThrow(IllegalArgumentException::new);
                             if (set.stream().anyMatch(oth -> this == oth)) {
                                 throw new IllegalArgumentException("same object");
                             }

                             // ensure compatibility
                             if (set.stream().anyMatch(o -> o.incompatible.contains(this) || this.incompatible.contains(o))) {
                                 throw new IllegalArgumentException("incompatible");
                             }
                             return set;
                         })
                         .collect(Collectors.toSet());

        this.requires.clear();
        this.requires.addAll(sets);
    }

    @Override
    public void incompatible(@NotNull CliOption @NotNull ... others) {
        for (CliOption other : others) {
            if (this == other) {
                throw new IllegalArgumentException("same object");
            }
            if (!(other instanceof Option oth)) {
                throw new IllegalArgumentException();
            }

            // ensure compatibility
            if (requires.stream().anyMatch(set -> set.contains(oth))
                || oth.requires.stream().anyMatch(set -> set.contains(this))) {
                throw new IllegalArgumentException("incompatible");
            }

            incompatible.add(oth);
            oth.incompatible.add(this);
        }
    }
}
