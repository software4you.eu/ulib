package cc.fluse.ulib.core.impl.configuration;

import cc.fluse.ulib.core.configuration.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.function.Function;

public class DelegationConf implements Configuration {

    protected final Configuration config;

    public DelegationConf(@NotNull Configuration config) {
        this.config = config;
    }

    @Override
    public @NotNull Configuration getRoot() {
        return config.getRoot();
    }

    @Override
    public @NotNull <T> Optional<List<T>> list(@NotNull Class<T> clazz, @NotNull KeyPath path) {
        return config.list(clazz, path);
    }

    @Override
    public @NotNull Optional<Boolean> bool(@NotNull KeyPath path) {
        return config.bool(path);
    }

    @Override
    public @NotNull Optional<Integer> int32(@NotNull KeyPath path) {
        return config.int32(path);
    }

    @Override
    public @NotNull Optional<Long> int64(@NotNull KeyPath path) {
        return config.int64(path);
    }

    @Override
    public @NotNull Optional<Float> dec32(@NotNull KeyPath path) {
        return config.dec32(path);
    }

    @Override
    public @NotNull Optional<Double> dec64(@NotNull KeyPath path) {
        return config.dec64(path);
    }

    @Override
    public boolean isRoot() {
        return config.isRoot();
    }

    @Override
    public void converge(@NotNull Section base, boolean strict, boolean deep) {
        config.converge(base, strict, deep);
    }

    @Override
    public void purge(boolean deep) {
        config.purge(deep);
    }

    @Override
    public @NotNull Optional<Object> get(@NotNull KeyPath path) {
        return config.get(path);
    }

    @Override
    public @NotNull <T> Optional<T> get(@NotNull Class<T> clazz, @NotNull KeyPath path) {
        return config.get(clazz, path);
    }

    @Override
    public @NotNull Collection<KeyPath> getKeys(boolean deep) {
        return config.getKeys(deep);
    }

    @Override
    public @NotNull Map<KeyPath, Object> getValues(boolean deep) {
        return config.getValues(deep);
    }

    @Override
    public @NotNull Map<String, Object> graphUnpack(@Nullable Function<@NotNull Object, @Nullable Object> mapper) {
        return config.graphUnpack(mapper);
    }

    @Override
    public boolean isEmpty() {
        return config.isEmpty();
    }

    @Override
    public void set(@NotNull KeyPath path, @Nullable Object value) {
        config.set(path, value);
    }

    @Override
    public boolean isSet(@NotNull KeyPath path) {
        return config.isSet(path);
    }

    @Override
    public boolean contains(@NotNull KeyPath path) {
        return config.contains(path);
    }

    @Override
    public @NotNull Optional<? extends Configuration> getSubsection(@NotNull KeyPath path) {
        return config.getSubsection(path);
    }

    @Override
    public @NotNull Configuration createSubsection(@NotNull KeyPath path) {
        return config.createSubsection(path);
    }

    @Override
    public @NotNull Collection<? extends Configuration> getSubsections(boolean deep) {
        return config.getSubsections(deep);
    }

    @Override
    public boolean isSubsection(@NotNull KeyPath path) {
        return config.isSubsection(path);
    }

    @Override
    public @NotNull Configuration subAndCreate(@NotNull KeyPath path) {
        return config.subAndCreate(path);
    }

    @Override
    public void clear() {
        config.clear();
    }
}
