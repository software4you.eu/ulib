package cc.fluse.ulib.core.http;

import cc.fluse.ulib.core.function.ParamFunc;
import cc.fluse.ulib.core.impl.http.RResource;
import cc.fluse.ulib.core.io.Readable;
import cc.fluse.ulib.core.util.Expect;
import org.jetbrains.annotations.NotNull;

import java.io.InputStream;
import java.net.URL;

public interface RemoteResource extends Readable {
    @NotNull
    static RemoteResource of(@NotNull URL url, @NotNull ParamFunc<? super @NotNull URL, ? extends @NotNull InputStream, ?> executor) {
        return new RResource(url, executor);
    }

    @NotNull
    static RemoteResource of(@NotNull URL url) {
        return new RResource(url);
    }

    @NotNull
    static RemoteResource of(@NotNull String url, @NotNull ParamFunc<? super @NotNull URL, ? extends @NotNull InputStream, ?> executor) {
        return of(Expect.compute(URL::new, url).orElseThrow(), executor);
    }

    @NotNull
    static RemoteResource of(@NotNull String url) {
        return of(Expect.compute(URL::new, url).orElseThrow());
    }

    /**
     * @return the location of this resource
     */
    @NotNull
    URL getRemoteLocation();
}
