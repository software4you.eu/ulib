package cc.fluse.ulib.core.function;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

/**
 * A task that takes one parameter, returns a value and may throw a throwable object.
 *
 * @apiNote only pass this task object as function if you know for certain it won't throw an exception
 */
@FunctionalInterface
public interface TriParamFunc<T, U, V, R, X extends Exception> extends TriFunction<T, U, V, R> {

    @Deprecated
    static <T, U, V, R, X extends Exception> TriParamFunc<T, U, V, R, X> as(@NotNull TriParamFunc<T, U, V, R, ?> func) {
        return func::apply;
    }

    R execute(T t, U u, V v) throws X;

    @SneakyThrows
    @Override
    @Deprecated
    default R apply(T t, U u, V v) {
        return execute(t, u, v);
    }
}
