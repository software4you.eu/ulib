package cc.fluse.ulib.core.database.sql;

import cc.fluse.ulib.core.database.RemoteDatabase;

/**
 * Represents a PostgreSQL database.
 */
public interface PostgreSQLDatabase extends SqlDatabase, RemoteDatabase {}
