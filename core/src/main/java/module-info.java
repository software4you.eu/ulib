module ulib.core {
    // static
    requires static lombok;
    requires static org.jetbrains.annotations;

    // java
    requires java.instrument;
    requires java.sql;
    requires java.net.http;

    // 3rd party
    requires org.javassist;
    requires org.yaml.snakeyaml;
    requires cliftonlabs.jsonsimple;
    requires maven.resolver.provider;
    requires com.github.davidmoten.wordwrap;

    // api exports
    exports cc.fluse.ulib.core.tuple;
    exports cc.fluse.ulib.core.cli;
    exports cc.fluse.ulib.core.cli.ex;
    exports cc.fluse.ulib.core.common;
    exports cc.fluse.ulib.core.configuration;
    exports cc.fluse.ulib.core.configuration.serialization;
    exports cc.fluse.ulib.core.database;
    exports cc.fluse.ulib.core.database.exception;
    exports cc.fluse.ulib.core.database.sql;
    exports cc.fluse.ulib.core.database.sql.orm;
    exports cc.fluse.ulib.core.database.sql.query;
    exports cc.fluse.ulib.core.dependencies;
    exports cc.fluse.ulib.core.ex;
    exports cc.fluse.ulib.core.function;
    exports cc.fluse.ulib.core.http;
    exports cc.fluse.ulib.core.http.jsonrpc;
    exports cc.fluse.ulib.core.inject;
    exports cc.fluse.ulib.core.io;
    exports cc.fluse.ulib.core.io.channel;
    exports cc.fluse.ulib.core.io.transform;
    exports cc.fluse.ulib.core.io.transform.ex;
    exports cc.fluse.ulib.core.reflect;
    exports cc.fluse.ulib.core.util;
    exports cc.fluse.ulib.core.file;

    // init export
    exports cc.fluse.ulib.core.impl.init to ulib.loader;

    // impl exports
    exports cc.fluse.ulib.core.impl to ulib.loader, ulib.minecraft, ulib.spigot, ulib.bungeecord, ulib.velocity;
    exports cc.fluse.ulib.core.impl.configuration to ulib.spigot;
}