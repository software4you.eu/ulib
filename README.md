# uLib

![Maven metadata URL](https://img.shields.io/maven-metadata/v?label=snapshot&metadataUrl=https%3A%2F%2Frepo.fluse.cc%2Fcc%2Ffluse%2Fulib-snapshot%2Fulib-loader%2Fmaven-metadata.xml) ![Maven metadata URL](https://img.shields.io/maven-metadata/v?label=release&metadataUrl=https%3A%2F%2Frepo.fluse.cc%2Fcc%2Ffluse%2Fulib%2Fulib-loader%2Fmaven-metadata.xml)

ulib is a general purpose library.

Copyright (c) 2023 [fluse1367](https://gitlab.com/fluse1367)   
See "Included Software" (at the bottom) for copyright and license notice of included software.

Please also refer to the [documentation](./docs/Readme.md).

## Features

Brief overview of ulib's features.

### Core

- Code Injection Framework
- Dynamic Library Injection
- Maven Resolver
- Reflection Utilities
- JsonRPC v2 API
- Yaml/Json API
- SQL API
- Misc

### Minecraft

- Mojang Launchermeta API
- Mojang/Bukkit Mappings API
- Proxy-Server Bridge
- User Cache
- Misc

### Spigot

- GUI/Inventory Menus API
- Custom Enchantments
- Item API
- Misc

## Things to Know

- The minecraft variants of library depend on recent spigot/bungeecord/velocity versions. That means it may not work as
  expected or may not work at all on older server versions. You will not receive any support, when using another server
  version than the one this library is built for. <br><br>
  If you want to use older server versions, consider a cross-version compatibility tool, like
  [ViaBackwards](https://www.spigotmc.org/resources/viabackwards.27448),
  [ViaRewind](https://www.spigotmc.org/resources/viarewind.52109) or
  [ProtocolSupport](https://www.spigotmc.org/resources/protocolsupport.7201).


- Minimum Java version is 17.


- When launching uLib for the first time (or if the respective caching folder was removed), it will download a few of
  dependencies/libraries.

## Disclaimer

Note the copyright and [license of this project](./LICENSE). Use this library at your own risk! The contributors of this
project do not take any responsibility/liability in any way.

# Included Software

The following 3rd-party software is included within this project:

- NBTEditor
  ([click](https://github.com/BananaPuncher714/NBTEditor/blob/ff303d06e16eed119079bae92b8ef5700ec35d87/src/main/java/io/github/bananapuncher714/nbteditor/NBTEditor.java))
  Copyright (c) 2018 [BananaPuncher714](https://github.com/BananaPuncher714), licensed under
  the [MIT license](https://github.com/BananaPuncher714/NBTEditor/blob/ff303d06e16eed119079bae92b8ef5700ec35d87/LICENSE)
- Apache Commons Lang v3 (Copyright (c) 2022 [The Apache Software Foundation](https://www.apache.org/), licensed under
  the [Apache License 2.0](https://github.com/apache/commons-lang/blob/9f1ac974e1b52c58b1950acdaaf3e0d5881409df/LICENSE.txt)):
  - SystemUtils
    Class ([click](https://github.com/apache/commons-lang/blob/9f1ac974e1b52c58b1950acdaaf3e0d5881409df/src/main/java/org/apache/commons/lang3/SystemUtils.java))
  - JavaVersion
    Class ([click](https://github.com/apache/commons-lang/blob/9f1ac974e1b52c58b1950acdaaf3e0d5881409df/src/main/java/org/apache/commons/lang3/JavaVersion.java))

---