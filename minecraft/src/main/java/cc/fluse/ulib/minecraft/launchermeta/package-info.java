/**
 * Utilities to obtain/read data from mojang's launchermeta api.
 *
 * @see <a href="https://launchermeta.mojang.com/mc/game/version_manifest.json" target="_blank">Official Version Manifest</a>
 */
package cc.fluse.ulib.minecraft.launchermeta;