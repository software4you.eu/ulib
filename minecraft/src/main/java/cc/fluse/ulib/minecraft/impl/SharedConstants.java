package cc.fluse.ulib.minecraft.impl;

import cc.fluse.ulib.core.util.SingletonInstance;
import cc.fluse.ulib.minecraft.plugin.PluginBase;
import cc.fluse.ulib.minecraft.util.Protocol;

public final class SharedConstants {
    // current plugin (substitute) base
    public static final SingletonInstance<PluginBase<?, ?>> BASE = new SingletonInstance<>();

    // current plain mc version
    public static final SingletonInstance<String> MC_VER = new SingletonInstance<>();

    // current protocol
    public static final SingletonInstance<Protocol> MC_PROTOCOL = new SingletonInstance<>();
}
