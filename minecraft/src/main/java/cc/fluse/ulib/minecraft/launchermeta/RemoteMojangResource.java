package cc.fluse.ulib.minecraft.launchermeta;

import cc.fluse.ulib.core.common.Sizable;
import cc.fluse.ulib.core.http.RemoteResource;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.URL;
import java.util.Optional;

/**
 * Represents a downloadable mojang-resource.
 */
public interface RemoteMojangResource extends Sizable, RemoteResource {
    /**
     * Returns the resource's id
     *
     * @return the id
     */
    @NotNull
    String getId();

    /**
     * Returns the SHA-1 file hash as hex string
     *
     * @return the file hash as hex string
     */
    @NotNull
    Optional<String> getSha1();


    /**
     * Returns the representing file url.
     *
     * @return the representing file url
     */
    @NotNull
    default URL getUrl() {
        return getRemoteLocation();
    }

    /**
     * Downloads the file.
     *
     * @param dest the destination
     */
    default void download(@NotNull File dest) throws IOException {
        download(new FileOutputStream(dest));
    }

    /**
     * Writes the contents of the file into the stream.
     *
     * @param out the stream to write to
     */
    default void download(@NotNull OutputStream out) throws IOException {
        try (var in = download()) {
            in.transferTo(out);
        }
    }

    /**
     * Downloads teh contents of the file.
     *
     * @return the content stream
     */
    @NotNull
    default InputStream download() throws IOException {
        return streamRead();
    }
}
