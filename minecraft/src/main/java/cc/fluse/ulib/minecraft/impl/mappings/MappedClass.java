package cc.fluse.ulib.minecraft.impl.mappings;

import cc.fluse.ulib.core.reflect.ReflectUtil;
import cc.fluse.ulib.core.util.Expect;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

class MappedClass extends Mapped<Class<?>> implements cc.fluse.ulib.minecraft.mappings.MappedClass {
    MappedClass(String sourceName, String mappedName) {
        super(sourceName, mappedName);
    }

    @SneakyThrows
    @Override
    public @NotNull Expect<Class<?>, ?> find() {
        return ReflectUtil.tryWithLoaders(l -> ReflectUtil.forName(mappedName(), false, l).orElseRethrow());
    }
}
