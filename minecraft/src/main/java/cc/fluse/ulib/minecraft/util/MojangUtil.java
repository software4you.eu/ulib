package cc.fluse.ulib.minecraft.util;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.http.HttpUtil;
import cc.fluse.ulib.core.tuple.Pair;
import cc.fluse.ulib.core.tuple.Tuple;
import cc.fluse.ulib.core.util.Conversions;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.*;

/**
 * Access to some portion mojang's rest api.
 */
public final class MojangUtil {

    private static final URI API = URI.create("https://api.mojang.com/");
    private static final URI SESSION = URI.create("https://sessionserver.mojang.com/");

    private static final URI PROFILE = API.resolve("users/profiles/minecraft/");
    private static final URI PROFILES = API.resolve("user/profiles/");


    /**
     * Fetches the UUID corresponding to the username.
     *
     * @param username the username to fetch the uuid for
     * @return the uuid
     */
    @NotNull
    public static Optional<UUID> fetchUUID(@NotNull String username) throws IOException {
        try {
            return JsonConfiguration.loadJson(HttpUtil.GET(PROFILE.resolve(username)).body())
                                    .string("id")
                                    .map(Conversions::hexToUUID);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Fetches all usernames a player had in the past and the current username.
     *
     * @param uuid the player's uuid
     * @return a list containing pairs of (username, optional: instant)
     */
    @NotNull
    public static Optional<List<Pair<String, Optional<Instant>>>> fetchNames(@NotNull UUID uuid) throws IOException {

        // strip dashes
        String uuidStr = uuid.toString().replace("-", "");

        try {
            return JsonConfiguration.loadJson(HttpUtil.GET(PROFILES.resolve(uuidStr + "/names")).body())
                                    .list(JsonConfiguration.class, "")
                                    .map(list -> list.stream()
                                                     .map(sub -> Tuple.of(
                                                             sub.string("name").orElseThrow(),
                                                             sub.int64("changedToAt").map(Instant::ofEpochMilli)))
                                                     .toList());


        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


}
