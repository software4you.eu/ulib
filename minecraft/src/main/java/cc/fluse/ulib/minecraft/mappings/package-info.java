/**
 * Utilities to obtain official NMS mappings and read them.
 *
 * @see <a href="https://www.minecraft.net/en-us/article/minecraft-snapshot-19w36a" target="_blank">Mappings Announcement</a>
 */
package cc.fluse.ulib.minecraft.mappings;