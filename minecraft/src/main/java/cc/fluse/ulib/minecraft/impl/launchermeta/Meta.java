package cc.fluse.ulib.minecraft.impl.launchermeta;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.file.CachedFile;
import cc.fluse.ulib.core.http.RemoteResource;
import cc.fluse.ulib.core.util.Expect;
import cc.fluse.ulib.core.util.LazyValue;
import cc.fluse.ulib.minecraft.launchermeta.VersionManifest;
import cc.fluse.ulib.minecraft.launchermeta.VersionsMeta;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.*;

public final class Meta implements VersionsMeta {

    public static LazyValue<Meta> INSTANCE = LazyValue.immutable(() -> new Meta(
            JsonConfiguration.loadJson(URI.create("https://launchermeta.mojang.com/mc/game/version_manifest.json").toURL().openStream())
    ));


    private final String release, snapshot;
    private final Map<String, LazyValue<Version>> versions;

    private Meta(JsonConfiguration json) {

        // versions loading
        Map<String, LazyValue<Version>> versions = new HashMap<>();
        json.list(JsonConfiguration.class, "versions")
                .orElseThrow()
                .forEach(sub -> {
                    var id = sub.string("id").orElseThrow();
                    var url = sub.string("url").orElseThrow();

                    versions.put(id, LazyValue.immutable(
                            () -> new Version(url, CachedFile.fromRemote(RemoteResource.of(url),
                                                                         CachedFile.DEFAULT_DIGEST_PROVIDER, null)
                                                             .streamRead(JsonConfiguration::loadJson)
                                                             .join()
                            )));
                });
        this.versions = Collections.unmodifiableMap(versions);

        var latest = json.getSubsection(KeyPath.of("latest")).orElseThrow();
        this.release = latest.string("release").orElseThrow();
        this.snapshot = latest.string("snapshot").orElseThrow();
    }

    @Override
    public @NotNull VersionManifest getRelease() {
        return get(release).orElseThrow();
    }

    @Override
    public @NotNull VersionManifest getSnapshot() {
        return get(snapshot).orElseThrow();
    }

    @Override
    public @NotNull Optional<VersionManifest> get(@NotNull String id) {
        if (versions.containsKey(id))
            return Optional.ofNullable(versions.get(id).get());
        return Optional.empty();
    }

    @SuppressWarnings("RedundantUnmodifiable")
    @Override
    public @NotNull Collection<VersionManifest> getVersions() {
        return Collections.unmodifiableCollection(versions.values().stream()
                                                          .map(LazyValue::getIfAvailable)
                                                          .filter(Expect::isPresent)
                                                          .map(Expect::getValue)
                                                          .toList());
    }
}
