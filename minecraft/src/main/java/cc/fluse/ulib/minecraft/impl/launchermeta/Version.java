package cc.fluse.ulib.minecraft.impl.launchermeta;

import cc.fluse.ulib.core.common.Keyable;
import cc.fluse.ulib.core.configuration.JsonConfiguration;
import cc.fluse.ulib.core.configuration.KeyPath;
import cc.fluse.ulib.core.util.LazyValue;
import cc.fluse.ulib.minecraft.launchermeta.*;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.net.URL;
import java.time.OffsetDateTime;
import java.util.*;

final class Version implements VersionManifest {
    private final String id;
    private final RemoteMojangResource assetIndex;
    private final Type type;
    private final URL url;
    private final OffsetDateTime time;
    private final OffsetDateTime releaseTime;
    private final Map<String, RemoteMojangResource> downloads;
    private final LazyValue<Collection<RemoteLibrary>> libraries;

    @SneakyThrows
    Version(String url, JsonConfiguration json) {
        this.id = json.string("id").orElseThrow();

        var ai = json.getSubsection(KeyPath.of("assetIndex")).orElseThrow();
        this.assetIndex = new MojangResource(ai.string("id").orElseThrow(), ai);

        this.type = Type.valueOf(json.string("type").orElseThrow().toUpperCase());
        this.url = new URL(url);

        this.time = OffsetDateTime.parse(json.string("time").orElseThrow());
        this.releaseTime = OffsetDateTime.parse(json.string("releaseTime").orElseThrow());

        Map<String, MojangResource> downloads = new HashMap<>();
        json.getSubsection(KeyPath.of("downloads")).orElseThrow()
            .getSubsections(false)
            .forEach(sub -> {
                @SuppressWarnings("unchecked")
                var key = ((Keyable<String>) sub).getKey();
                downloads.put(key, new MojangResource(key, sub));
            });
        this.downloads = Collections.unmodifiableMap(downloads);

        this.libraries = LazyValue.immutable(() -> {
            var libs = json.list(JsonConfiguration.class, "libraries")
                           .orElseThrow().stream().map(sub -> {
                        var mvnc = sub.string("name").orElseThrow();
                        var dwnlds = sub.getSubsection(KeyPath.of("downloads")).orElseThrow();
                        return new Library(mvnc, dwnlds);
                    })
                    .toList();
            //noinspection RedundantUnmodifiable
            return Collections.unmodifiableCollection(libs);
        });
    }

    @Override
    public @NotNull String getId() {
        return id;
    }

    @Override
    public @NotNull RemoteMojangResource getAssetIndex() {
        return assetIndex;
    }

    @Override
    public @NotNull Type getType() {
        return type;
    }

    @Override
    public @NotNull URL getUrl() {
        return url;
    }

    @Override
    public @NotNull OffsetDateTime getTime() {
        return time;
    }

    @Override
    public @NotNull OffsetDateTime getReleaseTime() {
        return releaseTime;
    }

    @Override
    public @NotNull Optional<RemoteMojangResource> getDownload(@NotNull String id) {
        return Optional.ofNullable(downloads.get(id));
    }

    @Override
    public @NotNull Map<String, RemoteMojangResource> getDownloads() {
        return downloads;
    }

    @Override
    public @NotNull Collection<RemoteLibrary> getLibraries() {
        return libraries.get();
    }
}
