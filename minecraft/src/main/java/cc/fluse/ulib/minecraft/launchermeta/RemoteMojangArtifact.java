package cc.fluse.ulib.minecraft.launchermeta;

import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

/**
 * Represents a library-artifact.
 */
public interface RemoteMojangArtifact extends RemoteMojangResource {
    /**
     * Returns the artifact's path.
     *
     * @return the path
     */
    @NotNull
    Path getPath();
}
