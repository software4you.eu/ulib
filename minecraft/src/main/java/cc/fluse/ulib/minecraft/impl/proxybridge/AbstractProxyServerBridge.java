package cc.fluse.ulib.minecraft.impl.proxybridge;

import cc.fluse.ulib.core.impl.Concurrent;
import cc.fluse.ulib.core.util.SingletonInstance;
import cc.fluse.ulib.minecraft.proxybridge.ProxyServerBridge;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.Future;

public abstract class AbstractProxyServerBridge extends ProxyServerBridge {
    public static final SingletonInstance<AbstractProxyServerBridge> INSTANCE = new SingletonInstance<>();

    private final HashMap<UUID, DataSupplier> answers = new HashMap<>();

    protected Future<byte[]> awaitData(UUID id, long timeout) {
        if (timeout == 0) {
            throw new IllegalArgumentException("Illegal timeout: 0");
        }
        DataSupplier supplier = new DataSupplier(timeout);
        answers.put(id, supplier);
        return Concurrent.run(supplier::get);
    }

    protected void putData(UUID id, byte[] data) {
        if (!answers.containsKey(id))
            return;
        answers.get(id).supply(data);
    }
}
