/**
 * Classes for caching player UUIDs and usernames.
 */
package cc.fluse.ulib.minecraft.usercache;