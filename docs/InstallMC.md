[<- Back to Overview](Readme.md)

# Installation in a Minecraft Environment

Installing uLib in one of the supported minecraft software ecosystems is fairly easy:
You just copy the loader file into the respective plugins or mods folder. Usually no special setup is necessary.

Currently supported are: Spigot, BungeeCord, Velocity and Fabric