# uLib Documentation

---
Please also refer to the main [README](../README.md) and [LICENSE](../LICENSE) file.

## Overview

1. [Introduction](About.md)
2. Installation
   1. [In a custom environment](DevRef.md#alternatives)
   2. [In a minecraft environment](InstallMC.md)
   3. [Troubleshooting](Troubleshooting.md)
3. Configuration
   1. [config.yml](../core/src/main/resources/META-INF/coreconfig.yml)
   2. [System Properties](System-Properties.md)
4. [Developer/In-Depth Reference](DevRef.md)