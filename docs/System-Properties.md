[<- Back to Overview](Readme.md)

## uLib System Properties

---

You can specify certain settings in uLib with the java system properties.

For instance, you can set a property using the `-D` argument in the java command. As it says in the `java -help`
command:

```shell
...@...:~$ java -help
    ...
    -D<name>=<value>
                  set a system property
    ...
```

Setting a system property (or multiple) would look like this:

```shell
java -Dsystem.property.key=my_value ...
```

The following settings are changeable with the system properties:

| Property                                      | Default     | Description                                                                                                                                                                           |
|:----------------------------------------------|-------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ulib.directory.main`                         | `.ulib`     | The main data-directory.                                                                                                                                                              |
| `ulib.directory.cache`                        | `cache`     | Directory where cached files will be placed. By default it will be placed inside the data-directory.                                                                                  |
| `ulib.directory.libraries`                    | `libraries` | Directory where library files will be placed. By default it will be placed inside the data-directory.                                                                                 |
| `ulib.forcesync`                              | `false`     | Switch to disable multi thread work off.                                                                                                                                              |
| `ulib.unsafe_operations`                      | `deny`      | Switch to allow [unsafe operations](DevRef.md#Unsafe-Operations). Possible values: `allow`, `deny`. Allow at your own risk!                                                           |
| `ulib.buffers.defaultcapacity`                | `1024`      | Default buffer capacity in bytes.                                                                                                                                                     |
| `ulib.buffers.allowdirect`                    | `true`      | Whether to allow java direct buffers. See "_Direct vs. non-direct buffers_" in the [javadocs](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/nio/ByteBuffer.html). |
| `ulib.maven-resolver.allow-checksum-mismatch` | `false`     | Whether to allow mismatching maven artifact checksums.                                                                                                                                |
| `ulib.install.module_layer`                   | `parent`    | Specifies the loader behavior. Possible values: `boot`, `comply`, `parent`.                                                                                                           |
| `ulib.install.tweak`                          | -           | (Unsafe) Enables tweaking of classes for initialization. Values are a comma seperated list of JNI targets.                                                                            |
| `ulib.install.env_overwrite`                  | -           | (Unsafe) Overwrites the environment type. Possible values: `STANDALONE`, `SPIGOT`, `BUNGEECORD`, `VELOCITY`, `TEST`                                                                   |

The properties have to be set before starting java/ulib, changing them during runtime takes no effect.