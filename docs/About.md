[<- Back to Overview](Readme.md)

# About uLib

ULib is a general purpose java library, so it serves multiple features.
It comes in multiple modules, including a set of minecraft related ones.

So, you can use ulib in non-minecraft environment as well as in a minecraft related one.

---

ULib is a bit different than other libraries, as a powerful bytecode transformation, or simplified a code injection
framework sits at its heart: You might have noticed that you don't place the module files directly in your environment,
instead you use the loader. The loader does some quirky things to "inject" the necessary ulib components into the java
runtime and ensures everything works just fine.

The code injection framework is heavily inspired by [SpongePowered Mixin](https://github.com/SpongePowered/Mixin), which
you might have heard from or even used it if you're a modder.

Special about ulib is that it can be used without any additional setup, just drop it in the plugins folder or include it
in your application's classpath - done. If you're a bit familiar with Java bytecode transformation, you might wonder how
the injection framework works without special setup. This is easy to explain: ULib simply self-initializes a javaagent
(read more about it [here](DevRef.md#about-the-javaagent)).

---

A personal note: This project accompanies me since I started learning Java, it went through several iterations even long
before I learnt about git and its awesomeness. At some point I decided to open source it, so that anyone can benefit
from it. I know it's just a piece of software, but it means a lot to me as I connect many memories with it. ~ fluse