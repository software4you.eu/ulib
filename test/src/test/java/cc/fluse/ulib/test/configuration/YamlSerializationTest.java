package cc.fluse.ulib.test.configuration;

import cc.fluse.ulib.core.configuration.YamlConfiguration;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

public class YamlSerializationTest {


    @Test
    public void testFreshDocument() throws IOException {
        testSerialization(YamlConfiguration.newYaml());
    }

    @Test
    public void testEmptyDocument() throws Exception {
        var doc = YamlConfiguration.loadYaml(new StringReader(""));
        testSerialization(doc);
    }

    private void testSerialization(YamlConfiguration yaml) throws IOException {
        var serializableObject = new SerializableObject(42, "Hello World!");


        // serialize to string
        yaml.set("obj", serializableObject);
        String serialized = yaml.dumpString();


        // deserialize to object
        yaml.clear();
        yaml.reload(new StringReader(serialized));
        var obj = yaml.get("obj").orElseThrow();

        Assert.assertEquals(serializableObject, obj);
    }


}
