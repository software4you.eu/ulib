package cc.fluse.ulib.test.configuration;

import cc.fluse.ulib.core.configuration.JsonConfiguration;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;

public class JsonSerializationTest {

    @Test
    public void testSerialization() throws IOException {
        var serializableObject = new SerializableObject(42, "Hello World!");

        // serialize to string
        var json = JsonConfiguration.newJson();
        json.set("obj", serializableObject);
        var serialized = json.dumpString();

        // deserialize to object
        json.clear();
        json.reload(new StringReader(serialized));
        var obj = json.get("obj").orElseThrow();

        Assert.assertEquals(serializableObject, obj);
    }
}
