package cc.fluse.ulib.test.io;

import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.DataChannel;
import cc.fluse.ulib.core.io.channel.PipeBufferChannel;
import cc.fluse.ulib.core.io.channel.SeekableDataChannel;
import org.junit.Assert;
import org.junit.Test;

import java.io.DataInput;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.channels.ByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.util.Arrays;

public class SeekableChannelAdapterTest {

    private static final int TEST_SIZE = 1024 * 1024;
    private static final int[] TEST_SEEK = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 1_000, 10_000, 100_000, 1_000_000};

    private ByteChannel createData(int size) throws IOException {
        var sampleSize = size >= 0 ? size : TEST_SIZE;


        var ch = DataChannel.wrap(new PipeBufferChannel());

        for (int i = 0; i < TEST_SIZE; i++) {
            ch.writeInt(i);
        }

        return ch;
    }

    private void testForwards(SeekableByteChannel ch) throws IOException {
        for (int i : TEST_SEEK) {
            ch.position(i * 4L);
            Assert.assertEquals(i, ((DataInput) ch).readInt());
        }
    }

    private void testBackwards(SeekableByteChannel ch) throws IOException {
        for (int i = TEST_SEEK.length - 1; i >= 0; i--) {
            ch.position(i * 4L);
            Assert.assertEquals(i, ((DataInput) ch).readInt());
        }
    }

    @Test
    public void testInMemoryForwards() throws IOException {
        testForwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), TEST_SIZE * 4, false)));
    }

    @Test
    public void testInMemoryBackwards() throws IOException {
        testBackwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), TEST_SIZE * 4, false)));
    }

    @Test
    public void testFileForwards() throws IOException {
        testForwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), 0, true)));
    }

    @Test
    public void testFileBackwards() throws IOException {
        testBackwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), 0, true)));
    }

    @Test
    public void testHybridForwards() throws IOException {
        testForwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), TEST_SIZE / 2, true)));
    }

    @Test
    public void testHybridBackwards() throws IOException {
        testBackwards(SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), TEST_SIZE / 2, true)));
    }

    @Test
    public void testMemoryLimit() throws IOException {
        try {
            IOUtil.toSeekableByteChannel(createData(0), 0, false);
        } catch (BufferOverflowException e) {
            // expected
            return;
        }
        // not expected
        throw new AssertionError("BufferOverflowException expected, but not thrown");
    }

    @Test
    public void testMemoryLimit2() throws IOException {
        var ch = SeekableDataChannel.wrap(IOUtil.toSeekableByteChannel(createData(-1), TEST_SIZE / 2, false));

        // should be able to read up to TEST_SIZE / 2
        for (int i : Arrays.stream(TEST_SEEK).filter(i -> i < TEST_SIZE / 2).toArray()) {
            ch.position(i * 4L);
            Assert.assertEquals(i, ch.readInt());
        }

        // should not be able to read beyond TEST_SIZE / 2
        try {
            ch.position(TEST_SIZE / 2 * 4L);
            ch.readInt();
        } catch (BufferOverflowException e) {
            // expected
            return;
        }
        throw new AssertionError("BufferOverflowException expected, but not thrown");
    }

}
