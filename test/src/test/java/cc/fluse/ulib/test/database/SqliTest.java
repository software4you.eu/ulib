package cc.fluse.ulib.test.database;

import cc.fluse.ulib.core.database.Database;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.file.FSUtil;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.sql.SQLException;

public class SqliTest {

    private final Path path = Path.of("testdb.sqlite");

    @Before
    public void clean() {
        FSUtil.rm(path, 0);
    }

    private SqlDatabase conn() throws IOException {
        return Database.connect(Database.Protocol.SQLite, path);
    }

    @Test
    public void testDB() throws Exception {
        int insert_id;
        try (var db = conn()) {
            insert_id = Util.dbTest(db);
        }

        // it's important to close the database connection before we can open it again,
        // so we can ensure, dbTest2 fetches all data from the database, not from the cache
        try (var db = conn()) {
            Util.dbTest2(db, insert_id);
        }
    }

    @Test
    public void testEntity() throws IOException, SQLException, InterruptedException {
        try (var db = conn()) {
            Util.entityTest(db);
        }
    }

}
