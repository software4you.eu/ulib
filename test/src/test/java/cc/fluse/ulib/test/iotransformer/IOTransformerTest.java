package cc.fluse.ulib.test.iotransformer;

import cc.fluse.ulib.core.function.ParamTask;
import cc.fluse.ulib.core.io.IOUtil;
import cc.fluse.ulib.core.io.channel.RingBufferChannel;
import cc.fluse.ulib.core.io.transform.ByteTransformer;
import cc.fluse.ulib.core.io.transform.TransformChannel;
import cc.fluse.ulib.test.TestUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Collection;
import java.util.Random;

public class IOTransformerTest {
    private static final Random RANDOM = new Random();

    public static ByteBuffer genSample(int size, long seed) {
        synchronized (RANDOM) {
            // seed
            RANDOM.setSeed(seed);
            // gen sample
            var sample = new byte[size];
            RANDOM.nextBytes(sample);
            // pack and return
            return ByteBuffer.wrap(sample).asReadOnlyBuffer();
        }
    }

    public static void testPipe(int sampleSize,
                                Collection<? extends ByteTransformer> outgoingProcessors,
                                Collection<? extends ByteTransformer> incomingProcessors) throws IOException {
        var pipe = new RingBufferChannel(sampleSize * 2, false);
        var write = TransformChannel.create((WritableByteChannel) IOUtil.isolate(pipe));
        var read = TransformChannel.create((ReadableByteChannel) IOUtil.isolate(pipe));

        outgoingProcessors.forEach(ParamTask.as(write::addTransformer));
        incomingProcessors.forEach(ParamTask.as(read::addTransformer));

        // test!
        sendThrough(genSample(sampleSize, 1234L), write, read);
    }

    private static void sendThrough(ByteBuffer sample, WritableByteChannel write, ReadableByteChannel read)
    throws IOException {

        // send sample through the pipe
        try (write) {
            write.write(sample);
        }

        ByteBuffer receivedSample;
        try (read) {
            receivedSample = IOUtil.readAll(read);
        }

        TestUtils.assertBBufEquals(sample.rewind(), receivedSample.rewind());
    }


}
