package cc.fluse.ulib.test;

import cc.fluse.ulib.core.function.Task;
import cc.fluse.ulib.core.util.SystemUtils;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.HexFormat;
import java.util.List;

public class TestUtils {

    /**
     * Captures stdout while running a certain task.
     *
     * @param task the task to tun
     * @return the captured data
     */
    public static <X extends Exception> InputStream captureStdOut(Task<X> task) throws X, IOException {

        // capture stdout
        final var stdout = System.out;

        // wrap
        var buf = new PipedInputStream();
        try (var out = new PrintStream(new PipedOutputStream(buf)) {
            @Override
            public void write(int b) {
                super.write(b);
                stdout.write(b);
            }

            @Override
            public synchronized void flush() {
                super.flush();
                stdout.flush();
            }
        }) {
            System.setOut(out);

            // run
            task.execute();
        } finally {
            // revert to original stdout
            System.setOut(stdout);
        }

        return buf;
    }

    /**
     * Runs a task and asserts the output from stdout using {@link Assert#assertArrayEquals(Object[], Object[])}
     *
     * @param task  the task to run
     * @param lines the expected output lines
     */
    public static <X extends Exception> void assertStdOut(Task<X> task, String... lines) throws X, IOException {

        try (var buf = new BufferedReader(new InputStreamReader(captureStdOut(task)))) {
            var readLines = buf.lines().toArray(String[]::new);
            Assert.assertArrayEquals(lines, readLines);
        }

    }

    public static void assertBBufEquals(ByteBuffer expected, ByteBuffer actual) throws IOException {
        var mi = expected.mismatch(actual);
        if (mi == -1) return;

        var hex = HexFormat.of().withUpperCase();
        var expectedByte = expected.remaining() > mi ? "0x" + hex.toHexDigits(expected.get(mi)) : "<EOF>";
        var gotByte = actual.remaining() > mi ? "0x" + hex.toHexDigits(actual.get(mi)) : "<EOF>";

        throw new IOException("mismatch at %d, expected: %s, got %s".formatted(mi, expectedByte, gotByte));
    }

    public static Process exec(@Nullable Path dir, String... cmd) throws IOException {
        return new ProcessBuilder(cmd)
                .directory(dir == null ? null : dir.toFile())
                .start();
    }

    public static void onlyLinux_x86_64() {
        if (!SystemUtils.IS_OS_LINUX || !List.of("amd64", "x86_64").contains(SystemUtils.OS_ARCH)) {
            throw new IllegalStateException("This test will only work on an x86_64 linux system");
        }
    }
}
