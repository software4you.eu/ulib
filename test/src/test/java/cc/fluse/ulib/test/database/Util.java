package cc.fluse.ulib.test.database;

import cc.fluse.ulib.core.database.sql.ColumnBuilder;
import cc.fluse.ulib.core.database.sql.DataType;
import cc.fluse.ulib.core.database.sql.SqlDatabase;
import cc.fluse.ulib.core.database.sql.orm.Serializer;
import cc.fluse.ulib.core.reflect.ReflectUtil;
import org.junit.Assert;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

final class Util {

    static final byte[] dummyData = {1, 2, 3, 4};
    static final byte[] dummyData2 = {5, 6, 7, 8};

    static int dbTest(SqlDatabase db) throws SQLException {

        // #addTable will throw exception if table already exists
        var table = db.addTable("test",
                ColumnBuilder.of(DataType.INTEGER, "id").primary().autoIncrement(),
                ColumnBuilder.of(DataType.TINYBLOB, "data")
        );

        // test create table
        Assert.assertFalse(table.exists());
        table.create();
        Assert.assertTrue(table.exists());

        // add data
        var inserted = table.insert(Map.of("data", dummyData));
        Assert.assertTrue(inserted instanceof Number);
        var insert_id = ((Number) inserted).intValue();


        // fetch data
        byte[] data;
        try (var res = table.select("data")
                .where("id").isEqualToP(insert_id)
                .query()) {
            Assert.assertTrue(res.next());
            data = res.getBytes(1);
        }
        Assert.assertArrayEquals(data, dummyData);


        // test update
        table.update()
                .setP("data", dummyData2)
                .where("id").isEqualToP(insert_id)
                .update();

        try (var res = table.select("data")
                .where("id").isEqualToP(insert_id)
                .query()) {
            Assert.assertTrue(res.next());
            data = res.getBytes(1);
        }
        Assert.assertArrayEquals(data, dummyData2);

        // TODO: test with more data types

        return insert_id;
    }

    static void dbTest2(SqlDatabase db, int insert_id) throws SQLException {

        var table = db.getTable("test").orElseThrow();

        // test fetch data
        byte[] data;
        try (var res = table.select("data")
                .where("id").isEqualToP(insert_id)
                .query()) {
            Assert.assertTrue(res.next());
            data = res.getBytes(1);
        }
        Assert.assertArrayEquals(data, dummyData2);

        // test delete
        table.delete()
                .where("id").isEqualToP(insert_id)
                .update();
        // row should not exist anymore
        try (var res = table.select("data")
                .where("id").isEqualToP(insert_id)
                .query()) {
            Assert.assertFalse(res.next());
        }

        // test drop table
        Assert.assertTrue(table.exists());
        Assert.assertTrue(table.drop());
        Assert.assertFalse(table.exists());
    }

    static void entityTest(SqlDatabase db) throws SQLException, Serializer.SerializerException, InterruptedException {

        var table = db.addTable("entity_test",
                ColumnBuilder.of(DataType.INTEGER, "id").primary().autoIncrement(),
                ColumnBuilder.of(DataType.TINYBLOB, "data"),
                ColumnBuilder.of(DataType.VARCHAR, "str").notNull().size(255)
        );

        // create table
        Assert.assertFalse(table.exists());
        table.create();
        Assert.assertTrue(table.exists());

        // define MyString for serialization test
        record MyString(String str) {
            static final Serializer<MyString, String> SERIALIZER = Serializer.create(MyString.class, MyString::str, MyString::new);

            @Override
            public boolean equals(Object obj) {
                return ReflectUtil.autoEquals(this, obj);
            }

            @Override
            public int hashCode() {
                return str.hashCode();
            }
        }

        // create entity
        var entity = table.createEntity(
                Map.of("str", "some string"),
                Map.of("str", List.of(MyString.SERIALIZER)),
                true, true, true
        );

        // test entity defaults
        Assert.assertEquals(entity.getDirect("str"), "some string");
        Assert.assertNull(entity.getDirect("data"));

        // test entity update
        entity.setDirect("str", "some other string");
        entity.setDirect("data", dummyData);
        entity.waitFor();

        Assert.assertEquals(entity.getDirect("str"), "some other string");
        Assert.assertArrayEquals((byte[]) entity.getDirect("data"), dummyData);


        // test serialization
        var myStr = new MyString("my string");
        ex:
        {
            try {
                entity.setDirect("str", myStr);
            } catch (IllegalArgumentException e) {
                break ex;
            }
            Assert.fail("IllegalArgumentException expected");
        }
        entity.set("str", myStr);
        entity.waitFor();
        Assert.assertEquals(entity.get("str"), new MyString(myStr.str()));


        // finally, drop table
        Assert.assertTrue(table.exists());
        Assert.assertTrue(table.drop());
        Assert.assertFalse(table.exists());
    }

}
