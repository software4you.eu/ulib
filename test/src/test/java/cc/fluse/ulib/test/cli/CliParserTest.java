package cc.fluse.ulib.test.cli;

import cc.fluse.ulib.core.cli.ArgNum;
import cc.fluse.ulib.core.cli.CliOption;
import cc.fluse.ulib.core.cli.CliOptions;
import cc.fluse.ulib.core.cli.ex.IllegalOptionArgumentException;
import cc.fluse.ulib.core.cli.ex.InvalidOptionSetException;
import cc.fluse.ulib.core.cli.ex.NoSuchOptionException;
import cc.fluse.ulib.core.cli.ex.OptionException;
import cc.fluse.ulib.test.TestUtils;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CliParserTest {

    CliOptions options = CliOptions.create();
    CliOption a = options.option("testA", new char[]{'a'}, "Test Option A",
            ArgNum.NO_ARGS.withDefaults());
    CliOption b = options.option("testB", new char[]{'b'}, "Test Option B",
            ArgNum.REQ_ARG1.withDefaults("default arg for B"));
    CliOption c = options.option("testC", new char[]{'c'}, "Test Option C",
            ArgNum.REQ_ARG.withDefaults());

    {
        a.requires(b);
        c.incompatible(a, b);
    }


    @Test
    public void testOptions() throws OptionException {
        var res = options.parse(new String[]{"-a", "--testB", "--", "other stuff", "lol"});

        assertTrue(res.hasOption(a));
        assertTrue(res.hasOption('a'));
        assertTrue(res.hasOption("testA"));

        assertTrue(res.hasOption(b));
        assertTrue(res.hasOption('b'));
        assertTrue(res.hasOption("testB"));

        assertFalse(res.hasOption(c));
        assertFalse(res.hasOption('c'));
        assertFalse(res.hasOption("testC"));

        assertArrayEquals(res.getParameters().toArray(), new Object[]{"other stuff", "lol"});
        assertArrayEquals(res.getOptionArguments('b').orElseThrow().toArray(), new Object[]{"default arg for B"});

        res = options.parse(new String[]{"-c", ""});

        assertFalse(res.hasOption('a'));
        assertFalse(res.hasOption('b'));
        assertTrue(res.hasOption('c'));

        assertArrayEquals(res.getOptionArguments('c').orElseThrow().toArray(), new Object[]{""});

        res = options.parse(new String[]{"-ab"});
        assertTrue(res.hasOption(a));
        assertTrue(res.hasOption(b));
    }

    @Test
    public void testErrors() throws OptionException {
        assertThrows(NoSuchOptionException.class,
                     () -> options.parse(new String[]{"--doesnotexist"}));

        // test illegal args
        assertThrows(IllegalOptionArgumentException.class,
                     () -> options.parse(new String[]{"-ba", "unallowed arg"}));
        assertThrows(IllegalOptionArgumentException.class,
                     () -> options.parse(new String[]{"-c"}));
        options.parse(new String[]{"-b"});
        assertThrows(IllegalOptionArgumentException.class,
                () -> options.parse(new String[]{"-c", "arg0", "arg1"}));
        assertThrows(IllegalOptionArgumentException.class,
                () -> options.parse(new String[]{"invalid arg", "-ab"}));

        // test invalid sets
        assertThrows(InvalidOptionSetException.class,
                () -> options.parse(new String[]{"-bc", ""}));
        assertThrows(InvalidOptionSetException.class,
                () -> options.parse(new String[]{"-a"}));
    }

    @Test
    public void testHelp() throws IOException {
        TestUtils.assertStdOut(() -> options.printHelp(System.out::println),
                               """
                                    --testA, -a              Test Option A
                                                             [No arguments]
                                                             [Requires any of (all of {--testB})]
                                                             [Incompatible with {--testC}]
                                                              
                                    --testB, -b              Test Option B
                                                             [At least one argument, Default: {default arg for B}]
                                                             [Incompatible with {--testC}]
                                                              
                                    --testC, -c              Test Option C
                                                             [Exactly one argument]
                                                             [Incompatible with {--testA, --testB}]
                               """.split("\n"));
    }

}
