package cc.fluse.ulib.test.io;

import cc.fluse.ulib.core.io.VarNum;
import cc.fluse.ulib.minecraft.io.MinecraftIO;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class VarNumTest {

    private final int[] TEST_INTS = {Integer.MIN_VALUE, Integer.MIN_VALUE / 2, 0, 42, 349058, Integer.MAX_VALUE / 2, Integer.MAX_VALUE};
    private final byte[][] TEST_INTS_BYTES = new byte[TEST_INTS.length][];


    private final long[] TEST_LONGS = {Long.MIN_VALUE, Long.MIN_VALUE / 2, 0L, 42L, 349058L, Long.MAX_VALUE / 2, Long.MAX_VALUE};
    private final byte[][] TEST_LONGS_BYTES = new byte[TEST_LONGS.length][];

    private final int[] BLOCK_LENS = {7, 15, 31};

    public VarNumTest() throws IOException {

        // produce mcio byte sequences

        var bout = new ByteArrayOutputStream();
        var out = new DataOutputStream(bout);

        // ints
        for (int i = 0; i < TEST_INTS.length; i++) {
            MinecraftIO.writeVarInt(out, TEST_INTS[i]).$rethrow();
            TEST_INTS_BYTES[i] = bout.toByteArray();
            bout.reset();
        }

        // longs
        for (int i = 0; i < TEST_LONGS.length; i++) {
            MinecraftIO.writeVarLong(out, TEST_LONGS[i]).$rethrow();
            TEST_LONGS_BYTES[i] = bout.toByteArray();
            bout.reset();
        }

    }

    /**
     * Tests if the VarWord suite generates the same byte sequences.
     */
    @Test
    public void testMciInteroperabilityEncode() {

        var bout = new ByteArrayOutputStream();

        // ints
        for (int i = 0; i < TEST_INTS.length; i++) {
            VarNum.writeVarInt(7, TEST_INTS[i], bout::write);
            Assert.assertArrayEquals(TEST_INTS_BYTES[i], bout.toByteArray());
            bout.reset();
        }

        // longs
        for (int i = 0; i < TEST_LONGS.length; i++) {
            VarNum.writeVarLong(7, TEST_LONGS[i], bout::write);
            Assert.assertArrayEquals(TEST_LONGS_BYTES[i], bout.toByteArray());
            bout.reset();
        }
    }

    /**
     * Tests if the VarWord suite decodes the given byte sequences to the correct integer.
     */
    @Test
    public void testMciInteroperabilityDecode() {
        for (int i = 0; i < TEST_INTS_BYTES.length; i++) {
            var num = VarNum.readVarInt(7, new ByteArrayInputStream(TEST_INTS_BYTES[i])::read);
            Assert.assertEquals(TEST_INTS[i], num);
        }

        for (int i = 0; i < TEST_LONGS_BYTES.length; i++) {
            var num = VarNum.readVarLong(7, new ByteArrayInputStream(TEST_LONGS_BYTES[i])::read);
            Assert.assertEquals(TEST_LONGS[i], num);
        }
    }

    @Test
    public void testPiping() {
        var bout = new ByteArrayOutputStream();

        for (int blen : BLOCK_LENS) {

            // ints
            for (int num : TEST_INTS) {
                VarNum.writeVarInt(blen, num, bout::write);
                Assert.assertEquals(num, VarNum.readVarInt(blen, new ByteArrayInputStream(bout.toByteArray())::read));
                bout.reset();
            }

            // longs
            for (long num : TEST_LONGS) {
                VarNum.writeVarLong(blen, num, bout::write);
                Assert.assertEquals(num, VarNum.readVarLong(blen, new ByteArrayInputStream(bout.toByteArray())::read));
                bout.reset();
            }

        }

    }
}
