package cc.fluse.ulib.test.iotransformer;

import cc.fluse.ulib.core.io.transform.ByteTransformer;
import cc.fluse.ulib.core.io.transform.TransformChannel;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CipherTransformerTest {

    private final IvParameterSpec iv = new IvParameterSpec(new byte[16]);
    private final SecretKeySpec key;
    private final Cipher cipher;
    private final byte[] sample;

    public CipherTransformerTest() throws NoSuchPaddingException, NoSuchAlgorithmException {
        var key = new byte[16]; // 16 bytes = 128 bit
        var rand = new SecureRandom();
        rand.setSeed(1908L);
        rand.nextBytes(key);
        this.key = new SecretKeySpec(key, "AES");
        this.cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

        // generate sample
        this.sample = new byte[5128];
        ThreadLocalRandom.current().nextBytes(sample);
    }

    @Test
    public void testCipher() throws IOException, InvalidAlgorithmParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        // encrypt sample with pipeline
        var bout = new ByteArrayOutputStream();
        try (var out = TransformChannel.create(bout)) {
            // init cipher
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            out.addTransformer(ByteTransformer.of(cipher));

            // pipe through
            out.write(ByteBuffer.wrap(sample));
        }

        var encryptedSample = bout.toByteArray();

        // decrypt with cipher
        cipher.init(Cipher.DECRYPT_MODE, key, iv);
        var decryptedSample = cipher.doFinal(encryptedSample);

        Assert.assertArrayEquals(sample, decryptedSample);
    }

    @Test
    public void testDecipher() throws InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);


        // encrypt sample with cipher
        var encryptedSample = cipher.doFinal(sample);

        // decrypt encrypted sample with pipeline
        var bout = new ByteArrayOutputStream();
        try (var out = TransformChannel.create(bout)) {

            // put cipher into decrypt mode and add transformer
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            out.addTransformer(ByteTransformer.of(cipher));

            // pipe through
            out.write(ByteBuffer.wrap(encryptedSample));
        }

        Assert.assertArrayEquals(sample, bout.toByteArray());
    }

    @Test
    public void testCipherPipe() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IOException {
        // init ciphers
        var enCipher = cipher;
        enCipher.init(Cipher.ENCRYPT_MODE, key, iv);

        var deCipher = Cipher.getInstance(cipher.getAlgorithm());
        deCipher.init(Cipher.DECRYPT_MODE, key, iv);

        IOTransformerTest.testPipe(sample.length,
            List.of(ByteTransformer.of(enCipher)),
            List.of(ByteTransformer.of(deCipher))
        );
    }

    @Test
    public void testCipherStreamPipe() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, IOException {
        var enCipher = cipher;
        enCipher.init(Cipher.ENCRYPT_MODE, key, iv);

        var deCipher = Cipher.getInstance(cipher.getAlgorithm());
        deCipher.init(Cipher.DECRYPT_MODE, key, iv);

        IOTransformerTest.testPipe(sample.length,
            List.of(ByteTransformer.of(os -> new CipherOutputStream(os, enCipher))),
            List.of(ByteTransformer.of(os -> new CipherOutputStream(os, deCipher)))
        );
    }

}
