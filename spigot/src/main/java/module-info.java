module ulib.spigot {
    // static
    requires static org.jetbrains.annotations;
    requires static lombok;
    requires static org.bukkit;
    requires static com.google.common;
    requires static com.google.gson;
    requires static commons.lang;

    // java
    requires java.logging;

    // 3rd party
    requires xseries;

    // ulib
    requires transitive ulib.minecraft;

    // api exports
    exports cc.fluse.ulib.spigot.enchantment;
    exports cc.fluse.ulib.spigot.inventorymenu.builder;
    exports cc.fluse.ulib.spigot.inventorymenu.entry;
    exports cc.fluse.ulib.spigot.inventorymenu.handlers;
    exports cc.fluse.ulib.spigot.inventorymenu.menu;
    exports cc.fluse.ulib.spigot.inventorymenu;
    exports cc.fluse.ulib.spigot.item;
    exports cc.fluse.ulib.spigot.plugin;
    exports cc.fluse.ulib.spigot.util;

    // impl exports
    exports cc.fluse.ulib.spigot.impl to ulib.loader;
}