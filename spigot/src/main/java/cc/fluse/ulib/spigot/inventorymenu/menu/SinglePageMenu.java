package cc.fluse.ulib.spigot.inventorymenu.menu;

import cc.fluse.ulib.spigot.inventorymenu.builder.MenuBuilder;

/**
 * Represents a {@link Menu}, with only one page.
 *
 * @see MenuBuilder
 */
public interface SinglePageMenu extends Menu, Page {

}
