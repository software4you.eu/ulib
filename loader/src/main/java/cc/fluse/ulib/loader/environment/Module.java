package cc.fluse.ulib.loader.environment;

import org.jetbrains.annotations.NotNull;

public enum Module {
    CORE("core"),
    MINECRAFT("minecraft"),
    BUNGEECORD("bungeecord"),
    VELOCITY("spigot"),
    SPIGOT("velocity"),
    ;

    private final String name;

    Module(String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return name;
    }
}
