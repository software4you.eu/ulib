package cc.fluse.ulib.loader.minecraft;

import cc.fluse.ulib.loader.environment.Environment;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.install.InitAccess;
import cc.fluse.ulib.loader.install.Installer;
import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.plugin.annotation.DataDirectory;
import com.velocitypowered.api.proxy.ProxyServer;
import lombok.SneakyThrows;
import org.slf4j.Logger;

import java.nio.file.Path;

@Plugin(
        id = "ulib3",
        name = "uLib 3 loader",
        authors = "fluse1367",
        url = "https://fluse.cc",
        version = "{{project.version}}"
)
public class PluginVelocity {

    static {
        RuntimeImpl.environment(Environment.VELOCITY);
        Installer.installMe();
    }

    private final ProxyServer proxyServer;
    private final Logger logger;
    private final Path dataDir;
    @SuppressWarnings("FieldCanBeLocal") // prevent the substitute to be gc'd
    private Object pluginSubstitute;

    @Inject
    public PluginVelocity(ProxyServer proxyServer, Logger logger, @DataDirectory Path dataDir) {
        this.proxyServer = proxyServer;
        this.logger = logger;
        this.dataDir = dataDir;
    }

    @SneakyThrows
    @Subscribe
    public void onInit(ProxyInitializeEvent e) {
        this.pluginSubstitute = InitAccess.getInstance().construct("velocity", "impl.PluginSubst",
                                                                   this, proxyServer, logger, dataDir);
    }
}
