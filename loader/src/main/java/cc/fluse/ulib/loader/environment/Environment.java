package cc.fluse.ulib.loader.environment;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

import static cc.fluse.ulib.loader.environment.Module.CORE;
import static cc.fluse.ulib.loader.environment.Module.MINECRAFT;

public enum Environment {
    TEST(Module.values()),
    STANDALONE(CORE),
    SPIGOT(CORE, MINECRAFT, Module.SPIGOT),
    BUNGEECORD(CORE, MINECRAFT, Module.VELOCITY),
    VELOCITY(CORE, MINECRAFT, Module.VELOCITY),
    FABRIC(CORE, MINECRAFT),
    QUILT(CORE, MINECRAFT),
    ;


    private final Set<Module> defaults;

    Environment(Module... defaults) {
        this.defaults = Set.of(defaults);
    }

    @NotNull
    public Set<Module> getDefaults() {
        return defaults;
    }
}
