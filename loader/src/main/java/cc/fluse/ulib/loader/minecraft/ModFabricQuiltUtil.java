package cc.fluse.ulib.loader.minecraft;

import cc.fluse.ulib.core.inject.HookInjection;
import cc.fluse.ulib.core.inject.InjectUtil;
import cc.fluse.ulib.core.reflect.ReflectUtil;

import java.util.Optional;
import java.util.function.Function;

final class ModFabricQuiltUtil {
    static void inject(String pfx, Function<String, byte[]> getter) throws Exception {
        new HookInjection()
                .addHook(ReflectUtil.forName(pfx + ".loader.impl.launch.knot.MixinServiceKnot", true).orElseRethrowRE(),
                         "getClassBytes(Ljava/lang/String;Z)[B",
                         InjectUtil.defaultHookingSpec(), (p, cb) -> Optional.ofNullable(
                                getter.apply(((String) p[0]).replace('/', '.'))
                        ).ifPresent(cb::setReturnValue))
                .injectNowFlat();
    }
}
