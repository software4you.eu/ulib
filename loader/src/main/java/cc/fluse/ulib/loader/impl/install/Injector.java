package cc.fluse.ulib.loader.impl.install;

import lombok.SneakyThrows;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

// handles injection of ulib
public class Injector {

    private final Logger logger;
    private final Initializer initializer;

    private final Set<ClassLoader> published = new HashSet<>(); // contains all CL's that ulib is installed to
    private final Set<Class<? extends ClassLoader>> injected = new HashSet<>();
    private final Set<ClassLoader> privileged = new HashSet<>(); // contains all CL's that may request loading of ulib classes regardless of installation state
    private Object delegation;

    Injector(Initializer initializer, Logger logger) {
        this.initializer = initializer;
        this.logger = logger;
        logger.finest(() -> "Injector created");
    }

    @SneakyThrows
    void initDelegation() {
        logger.fine(() -> "Initializing delegation");
        this.delegation = initializer.coreClass("cc.fluse.ulib.core.inject.ClassLoaderDelegation")
                .getConstructor(ClassLoader.class)
                .newInstance(initializer.getLoader());
    }

    @SneakyThrows
    void additionally(String method, Class<?>[] paramTypes, Function<? super Object[], Optional<String>> classNameResolver) {
        logger.finer(() -> "Adding additionally: " + method + " " + Arrays.toString(paramTypes) + " " + classNameResolver);
        this.delegation.getClass()
                // additionally(String method, Class<?>[] paramTypes, Function<? super Object[], Optional<String>> classNameResolver)
                .getMethod("additionally", String.class, Class[].class, Function.class)
                .invoke(this.delegation, method, paramTypes, classNameResolver);
    }

    public void privileged(ClassLoader loader, boolean is) {
        if (is) {
            logger.finer(() -> "Adding privileged: " + loader);
            privileged.add(loader);
        } else {
            logger.finer(() -> "Removing privileged: " + loader);
            privileged.remove(loader);
        }
    }

    private boolean testLoadingRequest(Class<?> requester, String request) {
        var requestingLayer = requester.getModule().getLayer();
        return  // do not delegate inner requests
                (requestingLayer == null || requestingLayer != initializer.getLoader().getLayer())
                // only allow public api module
                && initializer.getLoader().findApiModule(request, ModuleLoader.FLAG_DENY_IMPL).isPresent();
    }

    @SneakyThrows
    public void installLoaders(ClassLoader target) {
        if (published.contains(target)) {
            return;
        }
        logger.info(() -> "Installing ulib to " + target);
        published.add(target);

        var cl = target.getClass();
        if (injected.contains(cl)) {
            return;
        }

        logger.finer(() -> "Injecting ulib class loader to " + cl);
        var clIU = initializer.coreClass("cc.fluse.ulib.core.inject.InjectUtil");
        try {
            clIU.getMethod("injectLoaderDelegation", delegation.getClass(), Predicate.class, BiPredicate.class, Class.class)
                .invoke(null,
                        delegation,
                        (Predicate<ClassLoader>) loader -> published.contains(loader) || privileged.contains(loader),
                        (BiPredicate<Class<?>, String>) this::testLoadingRequest,
                        cl);
        } catch (InvocationTargetException e) {
            throw e.getTargetException();
        }

        injected.add(cl);
    }

    @SneakyThrows
    public void addReadsTo(Module module) {
        logger.finer(() -> "Adding reads to " + module);
        initializer.coreClass("cc.fluse.ulib.core.impl.Internal")
                .getMethod("makeAccessibleTo", Module.class)
                .invoke(null, module);
    }
}
