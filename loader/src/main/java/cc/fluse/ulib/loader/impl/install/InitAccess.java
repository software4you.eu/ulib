package cc.fluse.ulib.loader.impl.install;

import cc.fluse.ulib.loader.environment.Module;
import cc.fluse.ulib.loader.environment.Runtime;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.Util;
import cc.fluse.ulib.loader.install.Installer;
import cc.fluse.ulib.loader.minecraft.*;
import lombok.SneakyThrows;
import lombok.Synchronized;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

import static cc.fluse.ulib.loader.impl.Logging.LOGGER;

public class InitAccess {

    private static final Collection<Class<?>> PERMITTED = Util.tryClasses(
            () -> Installer.class,
            () -> PluginVelocity.class,
            () -> PluginSpigot.class,
            () -> PluginBungeecord.class,
            () -> ModFabric.class,
            () -> ModQuilt.class
    ); // tryClasses bc PluginVelocity/PluginSpigot might fail to load

    private static final InitAccess inst = new InitAccess();

    public static InitAccess getInstance() {
        inst.ensureAccess();
        return inst;
    }

    public InitAccess() {
        LOGGER.finest(() -> "InitAccess created");
    }

    private Set<Module> additionalModules = new HashSet<>();
    private Object initializer, injector;
    private boolean init;

    private void ensureAccess() {
        var caller = StackWalker.getInstance(StackWalker.Option.RETAIN_CLASS_REFERENCE).walk(st -> st
                // skip first frame as it is the caller of this method which is always this class itself
                .skip(1)
                .findFirst().orElseThrow()
                .getDeclaringClass()
        );
        if (!PERMITTED.contains(caller)
            && caller.getClassLoader() != getClass().getClassLoader()) {
            throw new SecurityException();
        }
    }

    @Synchronized
    public Runtime getRuntime() {
        ensureAccess();
        if (!init) throw new IllegalStateException();
        return Objects.requireNonNull(RuntimeImpl.get());
    }

    @Synchronized
    public void addModule(@NotNull Module module) {
        ensureAccess();
        if (init) throw new IllegalStateException();
        LOGGER.fine(() -> "Adding module " + module.getName());
        additionalModules.add(module);
    }

    public void ensureInit() {
        ensureAccess();
        try {
            init();
        } catch (URISyntaxException | ReflectiveOperationException e) {
            Throwable cause = e instanceof InvocationTargetException ite ? ite.getCause() : e;
            throw new RuntimeException("Failure while initializing ulib", cause);
        }
    }

    public void install(ClassLoader cl, java.lang.Module publish) throws ReflectiveOperationException {
        ensureAccess();
        ensureInit();

        injector.getClass().getMethod("installLoaders", ClassLoader.class)
                .invoke(injector, cl);

        if (publish != null) {
            injector.getClass().getMethod("addReadsTo", java.lang.Module.class)
                    .invoke(injector, publish);
        }
    }

    public void privileged(ClassLoader loader, boolean is) {
        ensureAccess();
        ensureInit();

        try {
            injector.getClass().getMethod("privileged", ClassLoader.class, boolean.class)
                    .invoke(injector, loader, is);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new InternalError(e);
        }
    }

    public ClassLoader loader() {
        ensureAccess();
        ensureInit();
        try {
            return (ClassLoader) initializer.getClass()
                                            .getMethod("getLoader")
                                            .invoke(initializer);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new InternalError(e);
        }
    }

    public ModuleLayer layer() {
        ensureAccess();
        ensureInit();

        try {
            var loader = loader();
            return (ModuleLayer) loader.getClass().getMethod("getLayer").invoke(loader);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return null;
        }
    }

    public byte[] classBytes(String fqcn) {
        ensureAccess();
        ensureInit();

        try {
            // fetch class bytes
            // will fail on any non-public ulib class
            var in = Class.forName(fqcn, true, loader())
                          .getModule().getResourceAsStream(Util.classify(fqcn));

            // to byte array
            var bout = new ByteArrayOutputStream();
            Util.write(in, bout); // closes stream
            return bout.toByteArray();
        } catch (ClassNotFoundException | IOException e) {
            return null;
        }
    }

    @SneakyThrows
    public Object construct(String module, String classNameSub, Object... initArgs) {
        ensureAccess();
        ensureInit();

        return initializer.getClass()
                          .getMethod("construct", String.class, String.class, Object[].class)
                          .invoke(initializer, module, classNameSub, initArgs);
    }

    @Synchronized
    private void init() throws URISyntaxException, ReflectiveOperationException {
        if (init) {
            return;
        }
        init = true;
        var runtime = RuntimeImpl.get();
        runtime.addActiveModules(additionalModules);
        additionalModules = null;

        LOGGER.info(() -> "Initializing ulib v%s with %s environment, %d modules: {%s}"
                .formatted(runtime.getVersion(), runtime.getEnvironment(), runtime.getAvailableModules().size(), runtime.getAvailableModules().stream().map(Module::getName).collect(Collectors.joining(", "))));

        if (getClass().getModule().isNamed()) {
            LOGGER.fine(() -> "Loader is a named module, loading initializer directly");

            // loader already loaded as module
            var initializer = Initializer.provide(runtime);
            this.initializer = initializer;
            this.injector = initializer.getInjector();
            return;
        }

        LOGGER.fine(() -> "Loading initializer via module loader");

        // load self as module
        File me = new File(getClass().getProtectionDomain().getCodeSource().getLocation().toURI());

        var layer = ModuleLoader.loadLayer(null, List.of(me),
                        getClass().getClassLoader(), Collections.singletonList(ModuleLayer.boot()), true)
                .layer();
        LOGGER.finest(() -> "Loaded layer: " + layer);

        this.initializer = Class.forName("cc.fluse.ulib.loader.impl.install.Initializer", true, layer.findLoader("ulib.loader"))
                .getMethod("provide", Object.class)
                .invoke(null, runtime.serialize());

        this.injector = initializer.getClass().getMethod("getInjector").invoke(this.initializer);
    }

}
