package cc.fluse.ulib.loader.minecraft;

import cc.fluse.ulib.loader.environment.Environment;
import cc.fluse.ulib.loader.impl.RuntimeImpl;
import cc.fluse.ulib.loader.impl.install.InitAccess;
import cc.fluse.ulib.loader.install.Installer;
import net.fabricmc.loader.api.entrypoint.PreLaunchEntrypoint;
import net.fabricmc.loader.impl.entrypoint.EntrypointUtils;

public class ModFabric implements PreLaunchEntrypoint {

    static {
        RuntimeImpl.environment(Environment.FABRIC);
        Installer.installTo(ClassLoader.getSystemClassLoader());
        Installer.installMe();

        try {
            ModFabricQuiltUtil.inject("net.fabricmc", InitAccess.getInstance()::classBytes);
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    @Override
    public void onPreLaunch() {
        EntrypointUtils.invoke("ulibPreLaunch", PreLaunchEntrypoint.class, PreLaunchEntrypoint::onPreLaunch);
    }
}
