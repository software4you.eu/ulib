package cc.fluse.ulib.loader.launch;

import cc.fluse.ulib.core.cli.ArgNum;
import cc.fluse.ulib.core.cli.CliArgs;
import cc.fluse.ulib.core.cli.CliOptions;
import cc.fluse.ulib.core.cli.ex.OptionException;
import cc.fluse.ulib.core.io.IOUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import static cc.fluse.ulib.core.util.Semantics.nofail;

final class CLI {
    private static CliOptions options() {
        var options = CliOptions.create();

        var help = options.option("help", new char[]{'h'}, "Shows the help message",
            ArgNum.NO_ARGS.withDefaults());
        var launch = options.option("launch", new char[]{'l'}, "Launches a jar file",
            ArgNum.REQ_ARG.withDefaults());
        var main = options.option("main", new char[]{'m'}, "Stars a main class",
                                  ArgNum.REQ_ARG.withDefaults());

        help.incompatible(launch, main);
        launch.incompatible(help, main);
        main.incompatible(help, launch);

        return options;
    }

    static void parse(String[] args) {
        var options = options();

        CliArgs res;
        try {
            res = options.parse(args);
        } catch (OptionException e) {
            fail(e.getMessage(), null);
            return;
        }

        if (res.getOptions().isEmpty() || res.hasOption('h')) {
            options.printHelp(System.out::println);
            return;
        }

        if (res.hasOption('l')) {
            startJar(Path.of(res.getOptionArguments('l').orElseThrow().iterator().next()),
                    res.getParameters().toArray(new String[0]));
            return;
        }

        if (res.hasOption('m')) {
            startMain(res.getOptionArguments('m').orElseThrow().iterator().next(),
                    res.getParameters().toArray(new String[0]),
                    Bootstrap.class.getClassLoader());
            return;
        }

        throw new InternalError();
    }

    private static void fail(String s, Throwable t) {
        if (s != null) System.err.println(s);
        if (t != null) t.printStackTrace();
        System.exit(1);
    }

    private static void startJar(Path path, String[] args) {
        // open jar
        JarFile jar;
        try {
            jar = new JarFile(path.toFile());
        } catch (IOException e) {
            fail("Unable to open file '%s'".formatted(path), e);
            return;
        }

        // read manifest
        Manifest manifest;
        try {
            if ((manifest = jar.getManifest()) == null)
                throw new IOException("No manifest found");
        } catch (IOException e) {
            fail("Failed to read manifest", e);
            return;
        }

        // retrieve main class
        String fqcn;
        if ((fqcn = manifest.getMainAttributes().getValue(Attributes.Name.MAIN_CLASS)) == null) {
            fail("No main class found in manifest", null);
            return;
        }

        IOUtil.closeQuietly(jar);

        var url = nofail(MalformedURLException.class, () -> path.toUri().toURL());
        startMain(fqcn, args, new URLClassLoader(new URL[]{url}, Bootstrap.class.getClassLoader()));
    }

    private static void startMain(String fqcn, String[] args, ClassLoader loader) {
        // load class
        Class<?> mainCl;
        try {
            mainCl = Class.forName(fqcn, true, loader);
        } catch (NoClassDefFoundError | ClassNotFoundException e) {
            fail("Unable to load class '%s'".formatted(fqcn), e);
            return;
        }

        // lookup main
        Method mainMt;
        try {
            if (!Modifier.isStatic((mainMt = mainCl.getMethod("main", String[].class)).getModifiers()))
                throw new NoSuchMethodException();
        } catch (NoSuchMethodException e) {
            fail("Class '%s' has no main method".formatted(fqcn), null);
            return;
        }

        // invoke main
        try {
            mainMt.invoke(null, (Object) args);
        } catch (IllegalAccessException e) {
            throw new InternalError(e);
        } catch (InvocationTargetException e) {
            fail(null, e.getTargetException());
        }
    }
}
