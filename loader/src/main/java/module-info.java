module ulib.loader {
    // static
    requires static lombok;
    requires static org.jetbrains.annotations;

    // ulib
    requires static ulib.core; // static bc it's loaded later

    // 3rd party
    requires static org.slf4j;
    requires static com.google.guice;

    // minecraft; static bc loader won't get loaded as module when in minecraft context
    requires static bungeecord.api; // bungeecord
    requires static org.bukkit; // spigot
    requires static com.velocitypowered.api; // velocity
    requires static quilt.loader; // quilt+fabric

    // java
    requires java.instrument;
    requires java.logging;
    requires jdk.attach;

    // api exports
    exports cc.fluse.ulib.loader.install;
    exports cc.fluse.ulib.loader.environment;
    exports cc.fluse.ulib.loader.agent to java.instrument;

    // impl exports
    opens cc.fluse.ulib.loader.impl.install;
}